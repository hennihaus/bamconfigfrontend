// ***********************************************************
// This example support/component.ts is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

import '@angular/localize/init';
import './commands';
import { mount, MountConfig } from 'cypress/angular';
import { EnvironmentProviders, LOCALE_ID, Provider, Type } from '@angular/core';
import { provideHttpClient, withFetch } from '@angular/common/http';
import {
  provideRouter,
  TitleStrategy,
  withComponentInputBinding,
} from '@angular/router';
import { CustomTitleStrategy } from '../../src/app/core/services/custom-title-strategy';
import { environment } from '../../src/environments/environment';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import {
  ACTIVE_MQ_URL,
  ASYNC_TASK_THUMBNAIL_URL,
  BASE_API_URL,
  BASE_API_VERSION,
  BASE_TITLE,
  BASE_TITLE_PREFIX,
  GOOGLE_CLOUD_IAM_URL,
  PASSWORD_LENGTH,
  PASSWORD_SEQUENCE,
} from '../../src/tokens';

declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      mount: typeof customMount;

      dragOffItem(x?: number, y?: number): Chainable<JQuery>;

      dropOnItem(): Chainable<JQuery>;
    }
  }
}

const providers: (Provider | EnvironmentProviders)[] = [
  provideHttpClient(withFetch()),
  provideRouter([], withComponentInputBinding()),
  {
    provide: LOCALE_ID,
    useValue: 'de',
  },
  {
    provide: TitleStrategy,
    useClass: CustomTitleStrategy,
  },
  {
    provide: ACTIVE_MQ_URL,
    useValue: environment.activeMqUrl,
  },
  {
    provide: GOOGLE_CLOUD_IAM_URL,
    useValue: environment.googleCloudIamUrl,
  },
  {
    provide: BASE_TITLE,
    useValue: environment.baseTitle,
  },
  {
    provide: BASE_TITLE_PREFIX,
    useValue: environment.baseTitlePrefix,
  },
  {
    provide: BASE_API_URL,
    useValue: environment.baseApiUrl,
  },
  {
    provide: BASE_API_VERSION,
    useValue: environment.baseApiVersion,
  },
  {
    provide: PASSWORD_LENGTH,
    useValue: environment.passwordLength,
  },
  {
    provide: PASSWORD_SEQUENCE,
    useValue: environment.passwordSequence,
  },
  {
    provide: ASYNC_TASK_THUMBNAIL_URL,
    useValue: environment.asyncTaskThumbnailUrl,
  },
];

const customMount = <T>(
  component: string | Type<T>,
  config?: MountConfig<T>,
) => {
  registerLocaleData(localeDe);

  if (!config) {
    config = { providers };
  } else {
    config.providers = [...(config?.providers || []), providers];
  }

  return mount<T>(component, config);
};

Cypress.Commands.add('mount', customMount);
