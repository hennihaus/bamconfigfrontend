// ***********************************************
// This example namespace declaration will help
// with Intellisense and code completion in your
// IDE or Text Editor.
// ***********************************************
// declare namespace Cypress {
//   interface Chainable<Subject = any> {
//     customCommand(param: any): typeof customCommand;
//   }
// }
//
// function customCommand(param: any): void {
//   console.warn(param);
// }
//
// NOTE: You can use it like so:
// Cypress.Commands.add('customCommand', customCommand);
//
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add(
  'dragOffItem',
  { prevSubject: 'element' },
  (element, x = 100, y = 100) => {
    cy.wrap(element).trigger('mousedown', { button: 0 });
    cy.wrap(element).trigger('mousemove', { pageX: x, pageY: y });

    return cy.wrap(element);
  },
);

Cypress.Commands.add('dropOnItem', { prevSubject: 'element' }, (element) => {
  cy.wrap(element).trigger('mousemove', { position: 'center' });
  cy.wrap(element).trigger('mouseup', { button: 0 });

  return cy.wrap(element);
});
