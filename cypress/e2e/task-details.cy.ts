describe('TaskDetails', () => {
  it('navigates to task details page with task id', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, task).as('getTaskRequest');
      cy.visit(`/tasks/details/${task.uuid}`);

      // when
      cy.wait('@getTaskRequest');

      // then
      cy.get('[data-cy=task-details-headline]').should('be.visible');
      cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
      cy.get('[data-cy=task-details-responses-table]').should('be.visible');
      cy.get('[data-cy=tasks-menu-item]').should('have.class', 'active');
      cy.get('[data-cy=print-tasks-button]').should('be.visible');
    });
  });

  it('navigates to task list page when task details page is called without task id', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
      'getTasksRequest',
    );
    cy.visit('/tasks/details/');

    // when
    cy.wait('@getTasksRequest');

    // then
    cy.url().should('eq', 'http://localhost:4200/tasks/list');
    cy.get('[data-cy=task-list-item]').should('have.length', 3);
  });

  it('navigates to task edit page when edit link is clicked', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.visit(`/tasks/details/${task.uuid}`);
      cy.wait('@getTaskRequest');

      // when
      cy.get('[data-cy=task-details-edit-link]').click();

      // then
      cy.wait(['@getExampleTeamRequest', '@getTaskRequest']);
      cy.url().should('eq', `http://localhost:4200/tasks/edit/${task.uuid}`);
      cy.get('[data-cy=form-stepper-headline]').should('be.visible');
    });
  });
});
