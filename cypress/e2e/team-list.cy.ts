describe('TeamList', () => {
  it('shows first page when team list page is visited, filtered and reset', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        cy.fixture('pagination').then((pagination) => {
          // given
          cy.intercept('GET', '/v1/teams?limit=5', (req) =>
            req.reply({
              delay: 250, // milliseconds
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination: {
                  ...pagination,
                  prev: null,
                },
              },
            }),
          ).as('getTeamsRequestWithoutQueryParams');
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&username=${teams.items[0].username}&jmsQueue=${teams.items[0].jmsQueue}&studentFirstname=${teams.items[0].students[0].firstname}&studentLastname=${teams.items[0].students[0].lastname}&minRequests=0&maxRequests=1&type=REGULAR&hasPassed=true&banks=Deutsche%20Bank&banks=Sparkasse`,
            (req) =>
              req.reply({
                delay: 250, // milliseconds
                body: {
                  ...teams,
                  items: [...teams.items, ...teams.items].slice(0, 5),
                  pagination: {
                    ...pagination,
                    prev: null,
                  },
                },
              }),
          ).as('getTeamsRequestWithQueryParams');
          cy.intercept('GET', '/v1/banks', (req) =>
            req.reply({
              delay: 250, // milliseconds
              body: [
                { ...bank, isAsync: true, name: 'Sparkasse' },
                { ...bank, isAsync: true, name: 'Deutsche Bank' },
              ],
            }),
          ).as('getBanksRequest');

          // when first page is visited without query params
          cy.visit('/teams');
          // then
          cy.get('[data-cy=loading-overlay]').should('be.visible');
          cy.wait('@getTeamsRequestWithoutQueryParams');
          cy.wait('@getBanksRequest');
          cy.get('[data-cy=loading-overlay]').should('not.exist');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);

          // when first page is visited with query params on filter
          cy.get('[data-cy=team-list-filter-username-input]').type(
            teams.items[0].username,
          );
          cy.get('[data-cy=team-list-filter-jms-queue-input]').type(
            teams.items[0].jmsQueue,
          );
          cy.get('[data-cy=team-list-filter-passed-radio-button]').check();
          cy.get('[data-cy=team-list-filter-type-radio-button]').last().check();
          cy.get('[data-cy=team-list-filter-min-requests-input]').type('0');
          cy.get('[data-cy=team-list-filter-max-requests-input]').type('1');
          cy.get('[data-cy=team-list-filter-student-firstname-input]').type(
            teams.items[0].students[0].firstname,
          );
          cy.get('[data-cy=team-list-filter-student-lastname-input]').type(
            teams.items[0].students[0].lastname,
          );
          cy.get('[data-cy=team-list-filter-left-bank-checkbox]').each((el) =>
            cy.wrap(el).check(),
          );
          cy.get('[data-cy=team-list-filter-right-bank-checkbox]').each((el) =>
            cy.wrap(el).check(),
          );
          cy.get('[data-cy=team-list-filter-submit-button]').click();
          // then
          cy.get('[data-cy=loading-overlay]').should('be.visible');
          cy.wait('@getTeamsRequestWithQueryParams');
          cy.get('[data-cy=loading-overlay]').should('not.exist');
          cy.get('[data-cy=team-list-filter-submit-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=team-list-filter-reset-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=team-list-item]').should('have.length', 5);

          // when first page is visited without query params on reset
          cy.get('[data-cy=team-list-filter-reset-button]').click();
          // then
          cy.get('[data-cy=loading-overlay]').should('be.visible');
          cy.wait('@getTeamsRequestWithoutQueryParams');
          cy.get('[data-cy=loading-overlay]').should('not.exist');
          cy.get('[data-cy=team-list-filter-submit-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=team-list-filter-reset-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
        });
      });
    });
  });

  it('shows first page and filled out team list filters when team list page is visited with query parameters', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        cy.fixture('pagination').then((pagination) => {
          // given
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&username=${teams.items[0].username}&jmsQueue=${teams.items[0].jmsQueue}&studentFirstname=${teams.items[0].students[0].firstname}&studentLastname=${teams.items[0].students[0].lastname}&minRequests=0&maxRequests=1&type=REGULAR&hasPassed=true&banks=Deutsche%20Bank&banks=Sparkasse`,
            {
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination: {
                  ...pagination,
                  prev: null,
                },
              },
            },
          ).as('getTeamsRequest');
          cy.intercept('GET', '/v1/banks', {
            body: [
              { ...bank, isAsync: true, name: 'Sparkasse' },
              { ...bank, isAsync: true, name: 'Deutsche Bank' },
            ],
          }).as('getBanksRequest');

          // when
          cy.visit(
            `/teams?username=${teams.items[0].username}&jmsQueue=${teams.items[0].jmsQueue}&studentFirstname=${teams.items[0].students[0].firstname}&studentLastname=${teams.items[0].students[0].lastname}&minRequests=0&maxRequests=1&type=REGULAR&hasPassed=PASSED&banks=Deutsche%20Bank&banks=Sparkasse`,
          );

          // then
          cy.wait('@getTeamsRequest');
          cy.wait('@getBanksRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=team-list-filter-username-input]').should(
            'have.value',
            teams.items[0].username,
          );
          cy.get('[data-cy=team-list-filter-jms-queue-input]').should(
            'have.value',
            teams.items[0].jmsQueue,
          );
          cy.get('[data-cy=team-list-filter-passed-radio-button]').should(
            'be.checked',
          );
          cy.get('[data-cy=team-list-filter-not-passed-radio-button]').should(
            'not.be.checked',
          );
          cy.get('[data-cy=team-list-filter-both-passed-radio-button]').should(
            'not.be.checked',
          );
          cy.get('[data-cy=team-list-filter-type-radio-button]')
            .should('have.length', 2)
            .first()
            .should('not.be.checked');
          cy.get('[data-cy=team-list-filter-type-radio-button]')
            .should('have.length', 2)
            .last()
            .should('be.checked');
          cy.get('[data-cy=team-list-filter-both-types-radio-button]').should(
            'not.be.checked',
          );
          cy.get('[data-cy=team-list-filter-min-requests-input]').should(
            'have.value',
            '0',
          );
          cy.get('[data-cy=team-list-filter-max-requests-input]').should(
            'have.value',
            '1',
          );
          cy.get('[data-cy=team-list-filter-student-firstname-input]').should(
            'have.value',
            teams.items[0].students[0].firstname,
          );
          cy.get('[data-cy=team-list-filter-student-lastname-input]').should(
            'have.value',
            teams.items[0].students[0].lastname,
          );
          cy.get('[data-cy=team-list-filter-left-bank-checkbox]').each((el) =>
            cy.wrap(el).should('be.checked'),
          );
          cy.get('[data-cy=team-list-filter-right-bank-checkbox]').each((el) =>
            cy.wrap(el).should('be.checked'),
          );
        });
      });
    });
  });

  it('shows different pages when pagination items are clicked', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        cy.fixture('pagination').then((pagination) => {
          // given
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&username=${teams.items[0].username}`,
            {
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination: {
                  ...pagination,
                  prev: null,
                },
              },
            },
          ).as('getStartTeamsRequest');
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&cursor=${pagination.next}&username=${teams.items[0].username}`,
            {
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination,
              },
            },
          ).as('getNextTeamsRequest');
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&cursor=${pagination.last}&username=${teams.items[0].username}`,
            {
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination: {
                  ...pagination,
                  next: null,
                },
              },
            },
          ).as('getLastTeamsRequest');
          cy.intercept(
            'GET',
            `/v1/teams?limit=5&cursor=${pagination.prev}&username=${teams.items[0].username}`,
            {
              body: {
                ...teams,
                items: [...teams.items, ...teams.items].slice(0, 5),
                pagination,
              },
            },
          ).as('getPreviousTeamsRequest');
          cy.intercept('GET', '/v1/banks', {
            body: [{ ...bank, isAsync: true }],
          }).as('getBanksRequest');

          // when first page is visited
          cy.visit(`/teams?username=${teams.items[0].username}`);
          // then
          cy.wait('@getStartTeamsRequest');
          cy.wait('@getBanksRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-start-item]').should(
            'have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'not.exist',
          );
          cy.get('[data-cy=cursor-pagination-next-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-end-item]').should(
            'not.have.class',
            'active',
          );

          // when next page is clicked
          cy.get('[data-cy=cursor-pagination-next-item]').click();
          // then
          cy.wait('@getNextTeamsRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-start-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-next-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-end-item]').should(
            'not.have.class',
            'active',
          );

          // when end page is clicked
          cy.get('[data-cy=cursor-pagination-end-item]').click();
          // then
          cy.wait('@getLastTeamsRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-start-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-next-item]').should('not.exist');
          cy.get('[data-cy=cursor-pagination-end-item]').should(
            'have.class',
            'active',
          );

          // when previous page is clicked
          cy.get('[data-cy=cursor-pagination-previous-item]').click();
          // then
          cy.wait('@getPreviousTeamsRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-start-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-next-item]').should(
            'not.have.class',
            'active',
          );
          cy.get('[data-cy=cursor-pagination-end-item]').should(
            'not.have.class',
            'active',
          );
        });
      });
    });
  });

  it('shows no pagination when team list page is visited and no further pages are available', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        cy.fixture('pagination').then((pagination) => {
          // given
          cy.intercept('GET', '/v1/teams?limit=5', {
            body: {
              ...teams,
              items: [...teams.items, ...teams.items].slice(0, 5),
              pagination: {
                ...pagination,
                prev: null,
                next: null,
              },
            },
          }).as('getTeamsRequest');
          cy.intercept('GET', '/v1/banks', {
            body: [{ ...bank, isAsync: true }],
          }).as('getBanksRequest');

          // when
          cy.visit('/teams');

          // then
          cy.wait('@getTeamsRequest');
          cy.wait('@getBanksRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-menu]').should('not.exist');
        });
      });
    });
  });

  it('shows first page when team list page size is changed on a higher page with query parameters', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        cy.fixture('pagination').then((pagination) => {
          // given
          cy.intercept('GET', '/v1/teams?limit=5', {
            body: {
              ...teams,
              items: [...teams.items, ...teams.items].slice(0, 5),
              pagination: {
                ...pagination,
                prev: null,
              },
            },
          }).as('getStartTeamsRequest');
          cy.intercept('GET', `/v1/teams?limit=5&cursor=${pagination.next}`, {
            body: {
              ...teams,
              items: [...teams.items, ...teams.items].slice(0, 5),
              pagination,
            },
          }).as('getNextTeamsRequest');
          cy.intercept('GET', '/v1/teams?limit=9', {
            body: {
              ...teams,
              items: [...teams.items, ...teams.items, ...teams.items].slice(
                0,
                9,
              ),
              pagination: {
                ...pagination,
                prev: null,
              },
            },
          }).as('getResizeTeamsRequest');
          cy.intercept('GET', '/v1/banks', {
            body: [{ ...bank, isAsync: true }],
          }).as('getBanksRequest');

          // when next page is visited
          cy.visit('/teams');
          cy.get('[data-cy=cursor-pagination-next-item]').click();
          // then
          cy.wait('@getStartTeamsRequest');
          cy.wait('@getNextTeamsRequest');
          cy.wait('@getBanksRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 5);
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'be.visible',
          );

          // when page size is changed
          cy.viewport(1920, 1080);
          // then
          cy.wait('@getResizeTeamsRequest');
          cy.get('[data-cy=team-list-item]').should('have.length', 9);
          cy.get('[data-cy=cursor-pagination-previous-item]').should(
            'not.exist',
          );
        });
      });
    });
  });

  it('shows team details page when team item is clicked', () => {
    cy.fixture('teams').then((teams) => {
      // given
      cy.intercept('GET', '/v1/teams?limit=5', { body: teams }).as(
        'getTeamsRequest',
      );
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.visit('/teams');

      // when
      cy.wait('@getTeamsRequest');
      cy.wait('@getBanksRequest');
      cy.get('[data-cy=team-list-item-image]')
        .first()
        .invoke('attr', 'src')
        .then((thumbnailUrl) => cy.wrap(thumbnailUrl).as('thumbnailUrl'));
      cy.get('[data-cy=team-list-item]').first().click();

      // then
      cy.get<string | undefined>('@thumbnailUrl')
        .then((thumbnailUrl) => thumbnailUrl?.replace(/\//g, '%2F'))
        .then((thumbnailUrl) => {
          cy.url().should(
            'eq',
            `http://localhost:4200/teams/details/${thumbnailUrl}/${teams.items[0].uuid}`,
          );
        });
    });
  });

  it('navigates to team create page when new team button is clicked', () => {
    // given
    cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
      'getTeamsRequest',
    );
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/teams');

    // when
    cy.wait('@getTeamsRequest');
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=new-team-button]').click();

    // then
    cy.wait('@getBanksRequest');
    cy.url().should('eq', 'http://localhost:4200/teams/create');
  });
});
