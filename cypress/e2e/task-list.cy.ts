describe('TaskList', () => {
  beforeEach(() => {
    cy.task('resetChromeRemoteInterface');
  });

  it('shows first page when task list page is visited without list and page number', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks, ...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks');

      // when
      cy.wait('@getTasksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when task list page is visited without page number', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks, ...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks/list');

      // when
      cy.wait('@getTasksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when task list page is visited with a too small page number', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks, ...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks/list/0');

      // when
      cy.wait('@getTasksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/tasks/list/0');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when task list page is visited with a negative page number', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks, ...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks/list/-1');

      // when
      cy.wait('@getTasksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/tasks/list/-1');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows last page when task list page is visited with a too high page number', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks, ...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks/list/4');

      // when
      cy.wait('@getTasksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/tasks/list/4');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 2) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows different pages when pagination items are clicked with two pages', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: [...tasks, ...tasks],
      }).as('getTasksRequest');
      cy.visit('/tasks/list/1');
      cy.wait('@getTasksRequest');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 2);

      // when second page is clicked
      cy.get('[data-cy=frontend-pagination-item]').eq(1).click();
      // then one task is shown
      cy.url().should('eq', 'http://localhost:4200/tasks/list/2');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(0)
        .should('not.have.class', 'active');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(1)
        .should('have.class', 'active');
      cy.get('[data-cy=task-list-item]').should('have.length', 1);

      // when first page is clicked
      cy.get('[data-cy=frontend-pagination-item]').eq(0).click();
      // then five tasks are shown
      cy.get('[data-cy=task-list-item]').should('have.length', 5);
    });
  });

  it('shows task details page when task item is clicked', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: tasks,
      }).as('getTasksRequest');
      cy.intercept('GET', `/v1/tasks/${tasks[0].uuid}`, { body: tasks[0] }).as(
        'getTaskRequest',
      );
      cy.visit('/tasks/list/1');

      // when
      cy.wait('@getTasksRequest');
      cy.get('[data-cy=task-list-item]').first().click();

      // then
      cy.wait('@getTaskRequest');
      cy.url().should(
        'eq',
        `http://localhost:4200/tasks/details/${tasks[0].uuid}`,
      );
      cy.get('[data-cy=task-details-headline]').should('be.visible');
    });
  });

  it('prints tasks on page load and navigates to task list page on afterprint', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: tasks,
      }).as('getTasksRequest');
      cy.visit('/tasks/list(print:print/tasks)');
      cy.window().then((win) => {
        cy.stub(win, 'print').as('print');
      });
      cy.task('activatePrintMediaQuery');

      // when page is loaded
      cy.wait('@getTasksRequest');
      // then it shows print content and no app content
      cy.get('@print').should('be.calledOnce');
      cy.get('[data-cy=header]').should('not.be.visible');
      cy.get('[data-cy=main]').should('not.be.visible');
      cy.get('[data-cy=task-list-item]').should('not.be.visible');
      cy.get('[data-cy=task-list-print-item]').should(
        'have.length',
        tasks.length,
      );
      // when print view is closed
      cy.window().trigger('afterprint');
      // then it shows app content
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=header]').should('be.visible');
      cy.get('[data-cy=main]').should('be.visible');
      cy.get('[data-cy=task-list-item]').should('have.length', tasks.length);
      cy.get('[data-cy=task-list-print-item]').should('not.exist');
    });
  });

  it('prints all tasks when print button is clicked and returns to task list page on afterprint', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', {
        body: tasks,
      }).as('getTasksRequest');
      cy.visit('/tasks');
      cy.wait('@getTasksRequest');
      cy.window().then((win) => {
        cy.stub(win, 'print').as('print');
      });
      cy.task('activatePrintMediaQuery');

      // when print button is clicked
      cy.get('[data-cy=print-tasks-button]').click();
      // then it shows print content and no app content
      cy.url().should(
        'eq',
        'http://localhost:4200/tasks/list(print:print/tasks)',
      );
      cy.wait('@getTasksRequest');
      cy.get('@print').should('be.calledOnce');
      cy.get('[data-cy=header]').should('not.be.visible');
      cy.get('[data-cy=main]').should('not.be.visible');
      cy.get('[data-cy=task-list-item]').should('not.be.visible');
      cy.get('[data-cy=task-list-print-item]').should(
        'have.length',
        tasks.length,
      );
      cy.get('[data-cy=task-list-print-item]').each((el, index) => {
        cy.wrap(el)
          .find('[data-cy=task-details-integration-step]')
          .contains(index + 1);
      });
      // when print view is closed
      cy.window().trigger('afterprint');
      // then it shows app content
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=header]').should('be.visible');
      cy.get('[data-cy=main]').should('be.visible');
      cy.get('[data-cy=task-list-item]').should('have.length', tasks.length);
      cy.get('[data-cy=task-list-print-item]').should('not.exist');
    });
  });

  it('prints first and second task when print button is clicked and no async bank is active', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.fixture('bank').then((bank) => {
        cy.intercept('GET', '/v1/tasks', {
          body: [
            tasks[0],
            tasks[1],
            {
              ...tasks[2],
              banks: [
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
              ],
            },
          ],
        }).as('getTasksRequest');
      });
      cy.visit('/tasks');
      cy.wait('@getTasksRequest');
      cy.window().then((win) => {
        cy.stub(win, 'print').as('print');
      });
      cy.task('activatePrintMediaQuery');

      // when
      cy.get('[data-cy=print-tasks-button]').click();

      // then
      cy.url().should(
        'eq',
        'http://localhost:4200/tasks/list(print:print/tasks)',
      );
      cy.wait('@getTasksRequest');
      cy.get('@print').should('be.calledOnce');
      cy.get('[data-cy=task-list-print-item]').should('have.length', 2);
      cy.get('[data-cy=task-list-print-item]').each((el, index) => {
        cy.wrap(el)
          .find('[data-cy=task-details-integration-step]')
          .contains(index + 1);
      });
    });
  });

  it('prints error message when print button is clicked and no tasks are provided', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { body: [] }).as('getTasksRequest');
    cy.visit('/tasks');
    cy.wait('@getTasksRequest');
    cy.window().then((win) => {
      cy.stub(win, 'print').as('print');
    });
    cy.task('activatePrintMediaQuery');

    // when
    cy.get('[data-cy=print-tasks-button]').click();

    // then
    cy.url().should(
      'eq',
      'http://localhost:4200/tasks/list(print:print/tasks)',
    );
    cy.wait('@getTasksRequest');
    cy.get('@print').should('be.calledOnce');
    cy.get('[data-cy=task-list-print-item]').should('not.exist');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
  });

  it('prints error message when print button is clicked with internal server error', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { body: {}, statusCode: 500 }).as(
      'getTasksRequest',
    );
    cy.visit('/tasks');
    cy.wait('@getTasksRequest');
    cy.window().then((win) => {
      cy.stub(win, 'print').as('print');
    });
    cy.task('activatePrintMediaQuery');

    // when
    cy.get('[data-cy=print-tasks-button]').click();

    // then
    cy.url().should(
      'eq',
      'http://localhost:4200/tasks/list(print:print/tasks)',
    );
    cy.wait('@getTasksRequest');
    cy.get('@print').should('be.calledOnce');
    cy.get('[data-cy=task-list-print-item]').should('not.exist');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
  });
});
