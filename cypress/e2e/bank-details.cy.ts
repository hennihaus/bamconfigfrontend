describe('BankDetails', () => {
  it('navigates to bank details page with bank id', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, { body: bank }).as(
        'getBankRequest',
      );
      cy.visit(`/banks/details/${bank.uuid}`);

      // when
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // then
      cy.get('[data-cy=bank-details-headline]').should('be.visible');
      cy.get('[data-cy=bank-details-edit-link]').should('be.visible');
      cy.get('[data-cy=banks-menu-item]').should('have.class', 'active');
      cy.get('[data-cy=distribute-teams-dropdown]').should('be.visible');
    });
  });

  it('navigates to bank list page when bank details page is called without bank id', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/banks/details');

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.url().should('eq', 'http://localhost:4200/banks/list');
    cy.get('[data-cy=bank-list-item]').should('have.length', 3);
  });

  it('navigates to bank edit page when edit link is clicked', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, { body: bank }).as(
        'getBankRequest',
      );
      cy.visit(`/banks/details/${bank.uuid}`);

      // when
      cy.wait(['@getBankRequest', '@getBanksRequest']);
      cy.get('[data-cy=bank-details-edit-link]').click();

      // then
      cy.wait('@getBankRequest');
      cy.url().should('eq', `http://localhost:4200/banks/edit/${bank.uuid}`);
      cy.get('[data-cy=bank-edit-headline]').should('be.visible');
    });
  });
});
