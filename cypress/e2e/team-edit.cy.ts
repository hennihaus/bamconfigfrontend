describe('TeamEdit', () => {
  it('navigates to team details page without thumbnailUrl when team form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, team).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `/v1/teams/${team.uuid}`, team).as(
        'updateTeamRequest',
      );
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait(['@updateTeamRequest', '@getTeamRequest']);
      cy.url().should('eq', `http://localhost:4200/teams/details/${team.uuid}`);
    });
  });

  it('navigates to team details page with thumbnailUrl when team form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, {
        body: team,
      }).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `/v1/teams/${team.uuid}`, {
        body: team,
      }).as('updateTeamRequest');
      cy.visit(
        `/teams/edit/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait(['@updateTeamRequest', '@getTeamRequest']);
      cy.url().should(
        'eq',
        `http://localhost:4200/teams/details/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );
    });
  });

  it('stays on team edit page when team form submit responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, {
        body: team,
      }).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `/v1/teams/${team.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateTeamRequest');
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait('@updateTeamRequest');
      cy.url().should('eq', `http://localhost:4200/teams/edit/${team.uuid}`);
    });
  });

  it('stays on team edit page when team edit page has unsaved changes and confirm dialog is rejected with thumbnailUrl', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.visit(
        `/teams/edit/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=teams-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should(
        'eq',
        `http://localhost:4200/teams/edit/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );
      cy.get('[data-cy=team-form-username-input]').should(
        'have.value',
        'NewTeamUsername',
      );
    });
  });

  it('stays on team edit page when team edit page has unsaved changes and confirm dialog is rejected without thumbnailUrl', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=teams-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/teams/edit/${team.uuid}`);
      cy.get('[data-cy=team-form-username-input]').should(
        'have.value',
        'NewTeamUsername',
      );
    });
  });

  it('navigates to team list page when team edit page has unsaved changes and confirm dialog is accepted', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
        'getTeamsRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=teams-menu-item]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@getTeamsRequest');
      cy.url().should('eq', 'http://localhost:4200/teams/list');
      cy.get('[data-cy=team-list-item]').should('have.length', 3);
    });
  });

  it('navigates to team list page without confirmation dialog when team edit page has no unsaved changes on page load', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
        'getTeamsRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=teams-menu-item]').click();

      // then
      cy.wait('@getTeamsRequest');
      cy.url().should('eq', 'http://localhost:4200/teams/list');
      cy.get('[data-cy=team-list-item]').should('have.length', 3);
    });
  });

  it('navigates to team list page without confirmation dialog when team edit page has no unsaved changes on page edit', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
        'getTeamsRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.visit(`/teams/edit/${team.uuid}`);
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=teams-menu-item]').click();

      // then
      cy.wait('@getTeamsRequest');
      cy.url().should('eq', 'http://localhost:4200/teams/list');
      cy.get('[data-cy=team-list-item]').should('have.length', 3);
    });
  });

  it('navigates to team list page when team edit page is called without team id', () => {
    // given
    cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
      'getTeamsRequest',
    );
    cy.visit('/teams/edit');

    // when
    cy.wait('@getTeamsRequest');

    // then
    cy.url().should('eq', 'http://localhost:4200/teams/list');
    cy.get('[data-cy=team-list-item]').should('have.length', 3);
  });
});
