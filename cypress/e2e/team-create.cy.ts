describe('TeamCreate', () => {
  it('navigates to team details page when team form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', '/v1/banks', { fixture: 'banks', delay: 250 }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams/*/unique/username/*', {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', '/v1/teams/*/unique/jmsQueue/*', {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', '/v1/teams/*/unique/password/*', {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', '/v1/teams/*', team).as('updateTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}`, team).as('getTeamRequest');
      cy.visit('/teams/create');
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=team-form-jms-queue-input]').type(team.jmsQueue);
      cy.get('[data-cy=team-form-password-input]').type(team.password);
      cy.get('[data-cy=team-form-students-firstname-input]').type(
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').type(
        team.students[0].lastname,
      );
      cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
      cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.wait(['@updateTeamRequest', '@getTeamRequest']);
      cy.url().should('eq', `http://localhost:4200/teams/details/${team.uuid}`);
    });
  });

  it('stays on team create page when team form submit responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/*/unique/username/*`, {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/jmsQueue/*`, {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/password/*`, {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `v1/teams/*`, {
        statusCode: 500,
        body: {},
      }).as('updateTeamRequest');
      cy.visit('/teams/create');
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=team-form-jms-queue-input]').type(team.jmsQueue);
      cy.get('[data-cy=team-form-password-input]').type(team.password);
      cy.get('[data-cy=team-form-students-firstname-input]').type(
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').type(
        team.students[0].lastname,
      );
      cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
      cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
        '@updateTeamRequest',
      ]);
      cy.url().should('eq', 'http://localhost:4200/teams/create');
    });
  });

  it('stays on team create page when team create page has unsaved changes and confirm dialog is rejected', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/teams/create');
    cy.wait('@getBanksRequest');

    // when
    cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
    cy.get('[data-cy=teams-menu-item]').click();
    cy.on('window:confirm', () => false);

    // then
    cy.url().should('eq', 'http://localhost:4200/teams/create');
    cy.get('[data-cy=team-form-selected-statistic]').should('be.visible');
    cy.get('[data-cy=team-form-unselected-statistic]').should('not.exist');
  });

  it('navigates to team list page when team create page has unsaved changes and confirm dialog is accepted', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
      'getTeamsRequest',
    );
    cy.visit('/teams/create');
    cy.wait('@getBanksRequest');

    // when
    cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
    cy.get('[data-cy=teams-menu-item]').click();
    cy.on('window:confirm', () => true);

    // then
    cy.wait('@getTeamsRequest');
    cy.url().should('eq', 'http://localhost:4200/teams/list');
    cy.get('[data-cy=team-list-item]').should('have.length', 3);
  });

  it('navigates to team list page without confirmation dialog when team create page has no unsaved changes on page load', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
      'getTeamsRequest',
    );
    cy.visit('/teams/create');
    cy.wait('@getBanksRequest');

    // when
    cy.get('[data-cy=teams-menu-item]').click();

    // then
    cy.wait('@getTeamsRequest');
    cy.url().should('eq', 'http://localhost:4200/teams/list');
    cy.get('[data-cy=team-list-item]').should('have.length', 3);
  });

  it('navigates to team list page without confirmation dialog when team create page has no unsaved changes on page edit', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=5', { fixture: 'teams' }).as(
        'getTeamsRequest',
      );
      cy.visit('/teams/create');
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-form-students-firstname-input]').type(
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').type(
        team.students[0].lastname,
      );
      cy.get('[data-cy=team-form-students-firstname-input]').clear();
      cy.get('[data-cy=team-form-students-lastname-input]').clear();
      cy.get('[data-cy=teams-menu-item]').click();

      // then
      cy.wait('@getTeamsRequest');
      cy.url().should('eq', 'http://localhost:4200/teams/list');
      cy.get('[data-cy=team-list-item]').should('have.length', 3);
    });
  });
});
