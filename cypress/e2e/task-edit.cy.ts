describe('TaskEdit', () => {
  it('navigates to task details page when task form is submitted', () => {
    cy.fixture('task').then((task) => {
      // given
      const now = new Date(2024, 9, 21, 7, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, task).as('getTaskRequest');
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.intercept('PATCH', `/v1/tasks/${task.uuid}`, task).as(
        'updateTaskRequest',
      );
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-submit-button]').click();

      // then
      cy.wait(['@updateTaskRequest', '@getTaskRequest']);
      cy.url().should('eq', `http://localhost:4200/tasks/details/${task.uuid}`);
    });
  });

  it('stays on task edit page when task form submit responds with internal server error', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.intercept('PATCH', `/v1/tasks/${task.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateTaskRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .clear();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .type('New parameter description');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').first().clear();
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .type('New response description');
      cy.get('[data-cy=form-stepper-submit-button]').click();

      // then
      cy.wait('@updateTaskRequest');
      cy.url().should('eq', `http://localhost:4200/tasks/edit/${task.uuid}`);
    });
  });

  it('stays on task edit page when task edit page has unsaved changes on first step and confirm dialog is rejected', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/tasks/edit/${task.uuid}`);
      cy.get('[data-cy=task-form-title-input]').should(
        'have.value',
        'New task title',
      );
    });
  });

  it('stays on task edit page when task edit page has unsaved changes on second step and confirm dialog is rejected', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .clear();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .type('New parameter description');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/tasks/edit/${task.uuid}`);
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .should('have.value', 'New parameter description');
    });
  });

  it('stays on task edit page when task edit page has unsaved changes on third step and confirm dialog is rejected', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').first().clear();
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .type('New response description');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/tasks/edit/${task.uuid}`);
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .should('have.value', 'New response description');
    });
  });

  it('navigates to task list page when task edit page has unsaved changes on first step and confirm dialog is accepted', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
        'getTasksRequest',
      );
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
    });
  });

  it('navigates to task list page when task edit page has unsaved changes on second step and confirm dialog is accepted', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
        'getTasksRequest',
      );
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .clear();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .type('New parameter description');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
    });
  });

  it('navigates to task list page when task edit page has unsaved changes on third step and confirm dialog is accepted', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
        'getTasksRequest',
      );
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').first().clear();
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .type('New response description');
      cy.get('[data-cy=tasks-menu-item]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
    });
  });

  it('navigates to task list page without confirmation dialog when task edit page has no unsaved changes on page load', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
        'getTasksRequest',
      );
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=tasks-menu-item]').click();

      // then
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
    });
  });

  it('navigates to task list page without confirmation dialog when task edit page has no unsaved changes on page edit', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
        'getTasksRequest',
      );
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type(task.title);
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .clear();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .type(task.parameters[0].description);
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').first().clear();
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .type(task.responses[0].description);

      cy.get('[data-cy=tasks-menu-item]').click();

      // then
      cy.wait('@getTasksRequest');
      cy.url().should('eq', 'http://localhost:4200/tasks/list');
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
    });
  });

  it('navigates to bank edit page when amount in euros edit button is clicked ', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${task.banks[0].uuid}`, {
        fixture: 'bank',
      }).as('getBankRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-amount-in-euros-example-field]')
        .find('[data-cy=task-form-parameters-bank-edit-button]')
        .click();

      // then
      cy.wait(['@getBanksRequest', '@getBankRequest']);
      cy.url().should(
        'eq',
        `http://localhost:4200/banks/edit/${task.banks[0].uuid}`,
      );
      cy.get('[data-cy=bank-edit-headline]').should('be.visible');
    });
  });

  it('navigates to bank edit page when term in months edit button is clicked ', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${task.banks[0].uuid}`, {
        fixture: 'bank',
      }).as('getBankRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.visit(`/tasks/edit/${task.uuid}`);
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-term-in-months-example-field]')
        .find('[data-cy=task-form-parameters-bank-edit-button]')
        .click();

      // then
      cy.wait(['@getBanksRequest', '@getBankRequest']);
      cy.url().should(
        'eq',
        `http://localhost:4200/banks/edit/${task.banks[0].uuid}`,
      );
      cy.get('[data-cy=bank-edit-headline]').should('be.visible');
    });
  });

  it('navigates to example team edit page when username edit button is clicked ', () => {
    cy.fixture('task').then((task) => {
      cy.fixture('team').then((team) => {
        // given
        cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
          'getBanksRequest',
        );
        cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
          'getTeamRequest',
        );
        cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
          'getTaskRequest',
        );
        cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
          fixture: 'teams',
        }).as('getExampleTeamRequest');
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.visit(`/tasks/edit/${task.uuid}`);
        cy.wait([
          '@getTaskRequest',
          '@getExampleTeamRequest',
          '@isTitleUniqueRequest',
        ]);

        // when
        cy.get('[data-cy=form-stepper-next-button]').click();
        cy.get('[data-cy=task-form-parameters-username-edit-button]').click();

        // then
        cy.wait(['@getBanksRequest', '@getTeamRequest']);
        cy.url().should('eq', `http://localhost:4200/teams/edit/${team.uuid}`);
        cy.get('[data-cy=team-edit-headline]').should('be.visible');
      });
    });
  });

  it('navigates to example team edit page when password edit button is clicked ', () => {
    cy.fixture('task').then((task) => {
      cy.fixture('team').then((team) => {
        // given
        cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
          'getBanksRequest',
        );
        cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
          'getTeamRequest',
        );
        cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
          'getTaskRequest',
        );
        cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
          fixture: 'teams',
        }).as('getExampleTeamRequest');
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.visit(`/tasks/edit/${task.uuid}`);
        cy.wait([
          '@getTaskRequest',
          '@getExampleTeamRequest',
          '@isTitleUniqueRequest',
        ]);

        // when
        cy.get('[data-cy=form-stepper-next-button]').click();
        cy.get('[data-cy=task-form-parameters-password-edit-button]').click();

        // then
        cy.wait(['@getBanksRequest', '@getTeamRequest']);
        cy.url().should('eq', `http://localhost:4200/teams/edit/${team.uuid}`);
        cy.get('[data-cy=team-edit-headline]').should('be.visible');
      });
    });
  });

  it('navigates to task list page when task edit page is called without task id', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
      'getTasksRequest',
    );
    cy.visit('/tasks/edit');

    // when
    cy.wait('@getTasksRequest');

    // then
    cy.url().should('eq', 'http://localhost:4200/tasks/list');
    cy.get('[data-cy=task-list-item]').should('have.length', 3);
  });
});
