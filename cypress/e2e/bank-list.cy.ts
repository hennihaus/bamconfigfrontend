describe('BankList', () => {
  it('shows first page when bank list page is visited without list and page number', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks');

      // when
      cy.wait('@getBanksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/banks/list');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when bank list page is visited without page number', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks/list');

      // when
      cy.wait('@getBanksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/banks/list');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when bank list page is visited with a too small page number', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks/list/0');

      // when
      cy.wait('@getBanksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/banks/list/0');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows first page when bank list page is visited with a negative page number', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks/list/-1');

      // when
      cy.wait('@getBanksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/banks/list/-1');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 0) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows last page when bank list page is visited with a too high page number', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks/list/4');

      // when
      cy.wait('@getBanksRequest');

      // then
      cy.url().should('eq', 'http://localhost:4200/banks/list/4');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').each((item, index) => {
        if (index === 2) {
          cy.wrap(item).should('have.class', 'active');
        } else {
          cy.wrap(item).should('not.have.class', 'active');
        }
      });
    });
  });

  it('shows different pages when pagination items are clicked with two pages', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks],
      }).as('getBanksRequest');
      cy.visit('/banks/list/1');
      cy.wait('@getBanksRequest');
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 2);

      // when second page is clicked
      cy.get('[data-cy=frontend-pagination-item]').eq(1).click();
      // then one bank is shown
      cy.url().should('eq', 'http://localhost:4200/banks/list/2');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(0)
        .should('not.have.class', 'active');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(1)
        .should('have.class', 'active');
      cy.get('[data-cy=bank-list-item]').should('have.length', 1);

      // when first page is clicked
      cy.get('[data-cy=frontend-pagination-item]').eq(0).click();
      // then five banks are shown
      cy.get('[data-cy=bank-list-item]').should('have.length', 5);
    });
  });

  it('shows bank details page when bank item is clicked', () => {
    // given
    cy.fixture('banks').then((banks) => {
      cy.intercept('GET', '/v1/banks', { body: banks }).as('getBanksRequest');
      cy.intercept('GET', `/v1/banks/${banks[0].uuid}`, { body: banks[0] }).as(
        'getBankRequest',
      );
      cy.visit('/banks/list/1');

      // when
      cy.wait('@getBanksRequest');
      cy.get('[data-cy=bank-list-item]').first().click();

      // then
      cy.wait('@getBankRequest');
      cy.url().should(
        'eq',
        `http://localhost:4200/banks/details/${banks[0].uuid}`,
      );
      cy.get('[data-cy=bank-details-headline]').should('be.visible');
    });
  });
});
