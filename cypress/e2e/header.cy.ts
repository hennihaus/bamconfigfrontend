describe('Header', () => {
  it('shows home page on root path', () => {
    // given
    cy.visit('/');

    // then
    cy.url().should('eq', Cypress.config().baseUrl);
    cy.get('[data-cy=home-title]').should('be.visible');
  });

  it('shows home page on unknown path', () => {
    // given
    cy.visit('/unknown-path');

    // then
    cy.url().should('eq', Cypress.config().baseUrl);
    cy.get('[data-cy=home-title]').should('be.visible');
  });

  it('marks start item as active on root path', () => {
    // given
    cy.visit('/');

    // then
    cy.get('[data-cy=start-menu-item]').should('have.class', 'active');
    cy.get('[data-cy=tasks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=banks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=teams-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=print-tasks-button]').should('not.exist');
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
    cy.get('[data-cy=new-team-button]').should('not.exist');
  });

  it('shows home page when home navigation item is clicked', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/banks');

    // when
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=start-menu-item]').click();

    // then
    cy.get('[data-cy=start-menu-item]').should('have.class', 'active');
    cy.get('[data-cy=tasks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=banks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=teams-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=print-tasks-button]').should('not.exist');
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
    cy.get('[data-cy=new-team-button]').should('not.exist');
  });

  it('shows bank list page when bank navigation item is clicked', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/');

    // when
    cy.get('[data-cy=banks-menu-item]').click();

    // then
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=start-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=tasks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=banks-menu-item]').should('have.class', 'active');
    cy.get('[data-cy=teams-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=print-tasks-button]').should('not.exist');
    cy.get('[data-cy=distribute-teams-dropdown]').should('be.visible');
    cy.get('[data-cy=new-team-button]').should('not.exist');
  });

  it('shows team list page when team navigation item is clicked', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.intercept('GET', '/v1/teams?limit=*', { fixture: 'teams' }).as(
      'getTeamsRequest',
    );
    cy.visit('/');

    // when
    cy.get('[data-cy=teams-menu-item]').click();

    // then
    cy.wait('@getTeamsRequest');
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=start-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=tasks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=banks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=teams-menu-item]').should('have.class', 'active');
    cy.get('[data-cy=print-tasks-button]').should('not.exist');
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
    cy.get('[data-cy=new-team-button]').should('be.visible');
  });

  it('shows task list page when task navigation item is clicked', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { fixture: 'tasks' }).as(
      'getTasksRequest',
    );
    cy.visit('/');

    // when
    cy.get('[data-cy=tasks-menu-item]').click();

    // then
    cy.wait('@getTasksRequest');
    cy.get('[data-cy=start-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=tasks-menu-item]').should('have.class', 'active');
    cy.get('[data-cy=banks-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=teams-menu-item]').should('not.have.class', 'active');
    cy.get('[data-cy=print-tasks-button]').should('be.visible');
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
    cy.get('[data-cy=new-team-button]').should('not.exist');
  });
});
