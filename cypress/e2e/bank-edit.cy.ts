describe('BankEdit', () => {
  it('navigates to bank details page when bank form is submitted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        ...bank,
        isActive: true,
      }).as('getBankRequest');
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, bank).as(
        'updateBankRequest',
      );
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type(
        'https://example.com/thubmnail.jpg',
      );
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.wait(['@updateBankRequest', '@getBankRequest']);
      cy.url().should('eq', `http://localhost:4200/banks/details/${bank.uuid}`);
    });
  });

  it('stays on bank edit page when bank form submit responds with internal server error', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: bank,
      }).as('getBankRequest');
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateBankRequest');
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('1');
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.wait('@updateBankRequest');
      cy.url().should('eq', `http://localhost:4200/banks/edit/${bank.uuid}`);
    });
  });

  it('stays on bank edit page when bank edit page has unsaved changes and confirm dialog is rejected', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {
          ...bank,
          isActive: true,
        },
      }).as('getBankRequest');
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('1');
      cy.get('[data-cy=banks-menu-item]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/banks/edit/${bank.uuid}`);
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').should(
        'have.value',
        '1',
      );
    });
  });

  it('navigates to bank list page when bank edit page has unsaved changes and confirm dialog is accepted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {
          ...bank,
          isActive: true,
        },
      }).as('getBankRequest');
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('1');
      cy.get('[data-cy=banks-menu-item]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@getBanksRequest');
      cy.url().should('eq', 'http://localhost:4200/banks/list');
      cy.get('[data-cy=bank-list-item]').should('have.length', 3);
    });
  });

  it('navigates to bank list page without confirmation dialog when bank edit page has no unsaved changes on page load', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {
          ...bank,
          isActive: true,
        },
      }).as('getBankRequest');
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=banks-menu-item]').click();

      // then
      cy.wait('@getBanksRequest');
      cy.url().should('eq', 'http://localhost:4200/banks/list');
      cy.get('[data-cy=bank-list-item]').should('have.length', 3);
    });
  });

  it('navigates to bank list page without confirmation dialog when bank edit page has no unsaved changes on page edit', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {
          ...bank,
          isActive: true,
        },
      }).as('getBankRequest');
      cy.visit(`/banks/edit/${bank.uuid}`);
      cy.wait(['@getBankRequest', '@getBanksRequest']);

      // when
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type(
        bank.creditConfiguration.minAmountInEuros,
      );
      cy.get('[data-cy=banks-menu-item]').click();

      // then
      cy.wait('@getBanksRequest');
      cy.url().should('eq', 'http://localhost:4200/banks/list');
      cy.get('[data-cy=bank-list-item]').should('have.length', 3);
    });
  });

  it('navigates to bank list page when bank edit page is called without bank id', () => {
    // given
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.visit('/banks/edit');

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.url().should('eq', 'http://localhost:4200/banks/list');
    cy.get('[data-cy=bank-list-item]').should('have.length', 3);
  });
});
