describe('TeamDetails', () => {
  it('navigates to team details page with thumbnail in url', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `v1/teams/${team.uuid}`, team).as('getTeamRequest');
      cy.visit(
        `/teams/details/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );

      // when
      cy.wait('@getTeamRequest');

      // then
      cy.get('[data-cy=team-details-image]').should(
        'have.attr',
        'src',
        'https://semantic-ui.com/images/avatar2/large/elyse.png',
      );
      cy.get('[data-cy=teams-menu-item]').should('have.class', 'active');
      cy.get('[data-cy=new-team-button]').should('be.visible');
    });
  });

  it('navigates to team details page without thumbnail in url', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `v1/teams/${team.uuid}`, team).as('getTeamRequest');
      cy.visit(`/teams/details/${team.uuid}`);

      // when
      cy.wait('@getTeamRequest');

      // then
      cy.get('[data-cy=team-details-image]').should('be.visible');
      cy.get('[data-cy=teams-menu-item]').should('have.class', 'active');
      cy.get('[data-cy=new-team-button]').should('be.visible');
    });
  });

  it('navigates to team list page when team is deleted', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `v1/teams/${team.uuid}`, {
        body: team,
      }).as('getTeamRequest');
      cy.intercept('DELETE', `v1/teams/${team.uuid}`, {
        statusCode: 204,
        body: '',
        delay: 250, // milliseconds
      }).as('deleteTeamRequest');
      cy.intercept('GET', 'v1/teams?limit=5', { fixture: 'teams' }).as(
        'getTeamsRequest',
      );
      cy.visit(`teams/details/${team.uuid}`);
      cy.wait('@getTeamRequest');

      // when
      cy.get('[data-cy=team-details-delete-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@deleteTeamRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.wait('@getTeamsRequest');
      cy.url().should('eq', 'http://localhost:4200/teams/list');
    });
  });

  it('stays on team details page when team deletion is rejected', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `v1/teams/${team.uuid}`, {
        body: team,
      }).as('getTeamRequest');
      cy.visit(`teams/details/${team.uuid}`);
      cy.wait('@getTeamRequest');

      // when
      cy.get('[data-cy=team-details-delete-button]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.url().should('eq', `http://localhost:4200/teams/details/${team.uuid}`);
    });
  });

  it('navigates to team edit page when edit link is clicked with thumbnailUrl', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/username/${team.username}`,
        { body: { isUnique: true } },
      ).as('isUsernameUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/jmsQueue/${team.jmsQueue}`,
        { body: { isUnique: true } },
      ).as('isJmsQueueUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/password/${team.password}`,
        { body: { isUnique: true } },
      ).as('isPasswordUniqueRequest');
      cy.visit(
        `/teams/details/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );

      // when
      cy.wait('@getTeamRequest');
      cy.get('[data-cy=team-details-edit-link]').click();

      // then
      cy.wait([
        '@getTeamRequest',
        '@getBanksRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.url().should(
        'eq',
        `http://localhost:4200/teams/edit/https:%2F%2Fsemantic-ui.com%2Fimages%2Favatar2%2Flarge%2Felyse.png/${team.uuid}`,
      );
      cy.get('[data-cy=team-edit-headline]').should('be.visible');
    });
  });

  it('navigates to team edit page when edit link is clicked without thumbnailUrl', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `/v1/teams/${team.uuid}`, { body: team }).as(
        'getTeamRequest',
      );
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/username/${team.username}`,
        { body: { isUnique: true } },
      ).as('isUsernameUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/jmsQueue/${team.jmsQueue}`,
        { body: { isUnique: true } },
      ).as('isJmsQueueUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/password/${team.password}`,
        { body: { isUnique: true } },
      ).as('isPasswordUniqueRequest');
      cy.visit(`/teams/details/${team.uuid}`);

      // when
      cy.wait('@getTeamRequest');
      cy.get('[data-cy=team-details-edit-link]').click();

      // then
      cy.wait([
        '@getTeamRequest',
        '@getBanksRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.url().should('eq', `http://localhost:4200/teams/edit/${team.uuid}`);
      cy.get('[data-cy=team-edit-headline]').should('be.visible');
    });
  });
});
