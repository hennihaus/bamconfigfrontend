import { InjectionToken } from '@angular/core';

export const BASE_API_URL = new InjectionToken<string>('baseApiUrl');
export const BASE_API_VERSION = new InjectionToken<string>('baseApiVersion');
export const BASE_TITLE = new InjectionToken<string>('baseTitle');
export const BASE_TITLE_PREFIX = new InjectionToken<string>('baseTitlePrefix');
export const ACTIVE_MQ_URL = new InjectionToken<string>('activeMqUrl');
export const GOOGLE_CLOUD_IAM_URL = new InjectionToken<string>(
  'googleCloudIamUrl',
);
export const PASSWORD_LENGTH = new InjectionToken<number>('passwordLength');
export const PASSWORD_SEQUENCE = new InjectionToken<string>('passwordSequence');
export const ASYNC_TASK_THUMBNAIL_URL = new InjectionToken<string>(
  'asyncTaskThumbnailUrl',
);
