import { Component, inject } from '@angular/core';
import { filter, map } from 'rxjs';
import {
  NavigationEnd,
  Router,
  RouterLink,
  RouterLinkActive,
} from '@angular/router';
import { HeaderSidebarComponent } from '../header-sidebar/header-sidebar.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-header',
  imports: [RouterLink, RouterLinkActive, HeaderSidebarComponent],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  private readonly router = inject(Router);

  private readonly baseUrlPlaceholder = 'http://www.example.com';

  readonly showTaskItems = toSignal(
    this.router.events.pipe(
      filter((event): event is NavigationEnd => event instanceof NavigationEnd),
      map((navigationEndEvent) => navigationEndEvent.url),
      map((url) => new URL(url, this.baseUrlPlaceholder)),
      map((url) => url.pathname.includes('tasks')),
    ),
    { initialValue: false },
  );
  readonly showBankItems = toSignal(
    this.router.events.pipe(
      filter((event): event is NavigationEnd => event instanceof NavigationEnd),
      map((navigationEndEvent) => navigationEndEvent.url),
      map((url) => new URL(url, this.baseUrlPlaceholder)),
      map((url) => url.pathname.includes('banks')),
    ),
    { initialValue: false },
  );
  readonly showTeamItems = toSignal(
    this.router.events.pipe(
      filter((event): event is NavigationEnd => event instanceof NavigationEnd),
      map((navigationEndEvent) => navigationEndEvent.url),
      map((url) => new URL(url, this.baseUrlPlaceholder)),
      map((url) => url.pathname.includes('teams')),
    ),
    { initialValue: false },
  );
}
