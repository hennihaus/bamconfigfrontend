import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';

export const CORE_ROUTES: Routes = [
  {
    path: '',
    component: HomeComponent,
    title: $localize`Home`,
  },
  {
    path: 'tasks',
    loadChildren: () =>
      import('../tasks/tasks.routes').then((m) => m.TASKS_ROUTES),
  },
  {
    path: 'banks',
    loadChildren: () =>
      import('../banks/banks.routes').then((m) => m.BANKS_ROUTES),
  },
  {
    path: 'teams',
    loadChildren: () =>
      import('../teams/teams.routes').then((m) => m.TEAMS_ROUTES),
  },
  {
    path: 'print',
    loadChildren: () =>
      import('../print/print.routes').then((m) => m.PRINT_ROUTES),
    outlet: 'print',
  },
  {
    path: '**',
    redirectTo: '',
  },
];
