import { Component, inject } from '@angular/core';
import { PrintService } from '../shared/services/print.service';
import {
  RouteConfigLoadEnd,
  RouteConfigLoadStart,
  Router,
  RouterOutlet,
} from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { filter, map } from 'rxjs';
import { LoadingComponent } from '../shared/components/loading/loading.component';
import { toSignal } from '@angular/core/rxjs-interop';

type LazyLoadingEvent = RouteConfigLoadStart | RouteConfigLoadEnd;

@Component({
  selector: 'bam-core',
  imports: [HeaderComponent, RouterOutlet, LoadingComponent],
  templateUrl: './core.component.html',
  styleUrl: './core.component.css',
})
export class CoreComponent {
  private readonly router = inject(Router);

  readonly print = inject(PrintService);

  readonly isLoading = toSignal(
    this.router.events.pipe(
      filter(
        (event): event is LazyLoadingEvent =>
          event instanceof RouteConfigLoadStart ||
          event instanceof RouteConfigLoadEnd,
      ),
      map((event) => {
        if (event instanceof RouteConfigLoadStart) {
          return true;
        }
        return false;
      }),
    ),
  );
}
