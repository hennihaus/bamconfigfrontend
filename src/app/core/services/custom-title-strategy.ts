import { inject, Injectable } from '@angular/core';
import { RouterStateSnapshot, TitleStrategy } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { BASE_TITLE, BASE_TITLE_PREFIX } from '../../../tokens';

@Injectable()
export class CustomTitleStrategy extends TitleStrategy {
  private readonly baseTitle = inject(BASE_TITLE);
  private readonly baseTitlePrefix = inject(BASE_TITLE_PREFIX);
  private readonly title = inject(Title);

  override updateTitle(snapshot: RouterStateSnapshot) {
    const title = this.buildTitle(snapshot);

    if (title) {
      this.title.setTitle(`${title} - ${this.baseTitle}`);
    } else {
      this.title.setTitle(`${this.baseTitlePrefix} - ${this.baseTitle}`);
    }
  }
}
