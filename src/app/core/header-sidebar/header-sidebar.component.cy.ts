import { HeaderSidebarComponent } from './header-sidebar.component';

describe('HeaderSidebarComponent', () => {
  it('sends team distribution request with all banks when dropdown is clicked', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', {
        body: [...banks, ...banks, ...banks],
      }).as('getBanksRequest');
      cy.intercept('POST', '/v1/statistics/3', (req) =>
        req.reply({
          body: '',
          statusCode: 204,
          delay: 45, // milliseconds
        }),
      ).as('recreateStatisticsRequestWithThreeBanks');
      cy.intercept('POST', '/v1/statistics/1', (req) =>
        req.reply({
          body: '',
          statusCode: 204,
          delay: 45, // milliseconds
        }),
      ).as('recreateStatisticsRequestWithOneBank');
      cy.mount(HeaderSidebarComponent, {
        componentProperties: {
          showTaskItems: false,
          showBankItems: true,
          showTeamItems: false,
        },
      });
      cy.wait('@getBanksRequest');

      // when last dropdown button is clicked
      cy.get('[data-cy=distribute-teams-dropdown]').click();
      // eslint-disable-next-line cypress/no-force
      cy.get('[data-cy=distribute-teams-option]').last().click({ force: true });
      cy.on('window:confirm', () => true);
      // then the request should fire with three banks
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@recreateStatisticsRequestWithThreeBanks');
      cy.get('[data-cy=loading-overlay]').should('not.exist');

      // when first dropdown button is clicked
      cy.get('[data-cy=distribute-teams-dropdown]').click();
      // eslint-disable-next-line cypress/no-force
      cy.get('[data-cy=distribute-teams-option]')
        .first()
        .click({ force: true });
      cy.on('window:confirm', () => true);
      // then the request should fire with one bank
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@recreateStatisticsRequestWithOneBank');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
    });
  });

  it('avoids team distribution request with all banks when confirmation is rejected', () => {
    // given
    cy.spy(window, 'alert').as('alertSpy');
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.mount(HeaderSidebarComponent, {
      componentProperties: {
        showTaskItems: false,
        showBankItems: true,
        showTeamItems: false,
      },
    });
    cy.wait('@getBanksRequest');

    // when
    cy.get('[data-cy=distribute-teams-dropdown]').click();
    // eslint-disable-next-line cypress/no-force
    cy.get('[data-cy=distribute-teams-option]').click({ force: true });
    cy.on('window:confirm', () => false);

    // then
    cy.get('[data-cy=loading-overlay]').should('not.exist');
    cy.get('@alertSpy').should('not.be.called');
  });

  it('shows an alert when team distribution request responds with an internal server error', () => {
    // given
    cy.spy(window, 'alert').as('alertSpy');
    cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
      'getBanksRequest',
    );
    cy.intercept('POST', '/v1/statistics/1', { body: {}, statusCode: 500 }).as(
      'recreateStatisticsRequest',
    );
    cy.mount(HeaderSidebarComponent, {
      componentProperties: {
        showTaskItems: false,
        showBankItems: true,
        showTeamItems: false,
      },
    });
    cy.wait('@getBanksRequest');

    // when
    cy.get('[data-cy=distribute-teams-dropdown]').click();
    // eslint-disable-next-line cypress/no-force
    cy.get('[data-cy=distribute-teams-option]').click({ force: true });
    cy.on('window:confirm', () => true);

    // then
    cy.on('window:alert', () => true);
    cy.get('@alertSpy').should('have.been.calledOnce');
  });

  it('shows no team distribution dropdown when banks request responds with an internal server error', () => {
    cy.intercept('GET', '/v1/banks', {
      body: {},
      statusCode: 500,
    }).as('getBanksRequest');
    cy.mount(HeaderSidebarComponent, {
      componentProperties: {
        showTaskItems: false,
        showBankItems: true,
        showTeamItems: false,
      },
    });

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
  });

  it('shows no task, bank and team items when showTaskItems = false, showBankItems = false and showTeamItems = false', () => {
    // given
    cy.mount(HeaderSidebarComponent, {
      componentProperties: {
        showTaskItems: false,
        showBankItems: false,
        showTeamItems: false,
      },
    });

    // then
    cy.get('[data-cy=distribute-teams-dropdown]').should('not.exist');
    cy.get('[data-cy=new-team-button]').should('not.exist');
  });
});
