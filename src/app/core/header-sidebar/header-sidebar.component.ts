import { Component, computed, inject, input, signal } from '@angular/core';
import { catchError, EMPTY, finalize } from 'rxjs';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { StatisticStoreService } from '../../shared/services/statistic-store.service';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { RouterLink } from '@angular/router';
import { rxResource } from '@angular/core/rxjs-interop';
import { ErrorHandlerService } from '../../shared/services/error-handler.service';

@Component({
  selector: 'bam-header-sidebar',
  imports: [RouterLink, LoadingComponent],
  templateUrl: './header-sidebar.component.html',
  styleUrl: './header-sidebar.component.css',
})
export class HeaderSidebarComponent {
  private readonly bankStore = inject(BankStoreService);
  private readonly statisticStore = inject(StatisticStoreService);
  private readonly errorHandler = inject(ErrorHandlerService);

  readonly showTaskItems = input.required<boolean>();
  readonly showBankItems = input.required<boolean>();
  readonly showTeamItems = input.required<boolean>();

  readonly banksResource = rxResource({
    loader: () => this.bankStore.getAll(),
  });
  readonly banks = computed(
    () =>
      this.banksResource
        .value()
        ?.filter((bank) => bank.isActive && bank.isAsync) ?? [],
  );
  readonly isLoading = signal(false);

  onRecreateStatistics(limit: number, event?: KeyboardEvent) {
    if (event && event.key !== 'Enter') {
      return;
    }
    if (
      confirm(
        $localize`Bei der Neuverteilung der Teams auf unterschiedliche Banken werden alle existierende Statistiken zurückgesetzt. Alle Gruppen haben nicht bestanden.`,
      )
    ) {
      this.isLoading.set(true);
      this.statisticStore
        .recreateAll(limit)
        .pipe(
          catchError(() => {
            alert(this.errorHandler.getServerErrorMessage());

            return EMPTY;
          }),
          finalize(() => this.isLoading.set(false)),
        )
        .subscribe();
    }
  }
}
