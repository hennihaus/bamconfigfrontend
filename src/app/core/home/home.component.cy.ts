import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  it('shows success message on reset button and confirmation click', () => {
    // given
    cy.mount(HomeComponent);
    cy.intercept('DELETE', '/v1/activemq', (req) => {
      req.reply({
        body: '',
        statusCode: 204,
        delay: 45, // milliseconds
      });
    }).as('resetBrokerRequest');

    // when button is clicked and popup confirmed
    cy.get('[data-cy=reset-broker-button]').click();
    cy.on('window:confirm', () => true);
    // then it should show right components
    cy.get('[data-cy=message]').should('not.exist');
    cy.get('[data-cy=loading-overlay]').should('be.visible');
    cy.wait('@resetBrokerRequest');
    cy.get('[data-cy=loading-overlay]').should('not.exist');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(44, 102, 45)');

    // when button is clicked and popup confirmed
    cy.get('[data-cy=reset-broker-button]').click();
    cy.on('window:confirm', () => true);
    // then it should show right components
    cy.get('[data-cy=message]').should('not.exist');
    cy.get('[data-cy=loading-overlay]').should('be.visible');
    cy.wait('@resetBrokerRequest');
    cy.get('[data-cy=loading-overlay]').should('not.exist');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(44, 102, 45)');
  });

  it('shows error message on reset button and confirmation click', () => {
    // given
    cy.mount(HomeComponent);
    cy.intercept('DELETE', '/v1/activemq', {
      body: {},
      statusCode: 500,
    }).as('resetBrokerRequest');

    // when
    cy.get('[data-cy=reset-broker-button]').click();
    cy.on('window:confirm', () => true);

    // then
    cy.wait('@resetBrokerRequest');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
  });

  it('shows no message when confirmation button was rejected', () => {
    // given
    cy.mount(HomeComponent);

    // when
    cy.get('[data-cy=reset-broker-button]').click();
    cy.on('window:confirm', () => false);

    // then
    cy.get('[data-cy=loading-overlay]').should('not.exist');
    cy.get('[data-cy=message]').should('not.exist');
  });
});
