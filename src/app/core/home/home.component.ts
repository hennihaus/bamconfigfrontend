import { Component, inject, signal } from '@angular/core';
import { BrokerStoreService } from '../../shared/services/broker-store.service';
import { catchError, map, tap } from 'rxjs';
import { Message } from '../../shared/models/message';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { ACTIVE_MQ_URL, GOOGLE_CLOUD_IAM_URL } from '../../../tokens';
import { ErrorHandlerService } from '../../shared/services/error-handler.service';

@Component({
  selector: 'bam-home',
  imports: [MessageComponent, LoadingComponent],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css',
})
export class HomeComponent {
  private readonly brokerStore = inject(BrokerStoreService);
  private readonly errorHandler = inject(ErrorHandlerService);

  readonly activeMqUrl = inject(ACTIVE_MQ_URL);
  readonly googleCloudIamUrl = inject(GOOGLE_CLOUD_IAM_URL);

  readonly isLoading = signal(false);
  readonly message = signal<Message | null>(null);

  resetBroker() {
    if (
      confirm(
        $localize`Damit werden alle Queues und Topics auf den Uhrsprungszustand zurückgesetzt. Die Nutzungsstatistiken werden zurückgesetzt und Gruppenpasswörter neu generiert.`,
      )
    ) {
      this.isLoading.set(true);
      this.message.set(null);
      this.brokerStore
        .resetBroker()
        .pipe(
          map(() => ({
            text: $localize`Gesamtsystem erfolgreich zurückgesetzt`,
            type: 'positive' as const,
          })),
          catchError(() => this.errorHandler.getServerErrorMessageAsync()),
          tap(() => this.isLoading.set(false)),
        )
        .subscribe((message) => this.message.set(message));
    }
  }
}
