import { ApplicationConfig } from '@angular/core';
import { provideHttpClient, withFetch } from '@angular/common/http';
import {
  provideRouter,
  TitleStrategy,
  withComponentInputBinding,
  withDebugTracing,
} from '@angular/router';
import { CORE_ROUTES } from './core.routes';
import { CustomTitleStrategy } from './services/custom-title-strategy';
import { environment } from '../../environments/environment';
import {
  ACTIVE_MQ_URL,
  ASYNC_TASK_THUMBNAIL_URL,
  BASE_API_URL,
  BASE_API_VERSION,
  BASE_TITLE,
  BASE_TITLE_PREFIX,
  GOOGLE_CLOUD_IAM_URL,
  PASSWORD_LENGTH,
  PASSWORD_SEQUENCE,
} from '../../tokens';
import { PRECONNECT_CHECK_BLOCKLIST } from '@angular/common';

export const appConfig: ApplicationConfig = {
  providers: [
    provideHttpClient(withFetch()),
    provideRouter(CORE_ROUTES, withComponentInputBinding(), withDebugTracing()),
    {
      provide: TitleStrategy,
      useClass: CustomTitleStrategy,
    },
    {
      provide: ACTIVE_MQ_URL,
      useValue: environment.activeMqUrl,
    },
    {
      provide: GOOGLE_CLOUD_IAM_URL,
      useValue: environment.googleCloudIamUrl,
    },
    {
      provide: BASE_TITLE,
      useValue: environment.baseTitle,
    },
    {
      provide: BASE_TITLE_PREFIX,
      useValue: environment.baseTitlePrefix,
    },
    {
      provide: BASE_API_URL,
      useValue: environment.baseApiUrl,
    },
    {
      provide: BASE_API_VERSION,
      useValue: environment.baseApiVersion,
    },
    {
      provide: PASSWORD_LENGTH,
      useValue: environment.passwordLength,
    },
    {
      provide: PASSWORD_SEQUENCE,
      useValue: environment.passwordSequence,
    },
    {
      provide: ASYNC_TASK_THUMBNAIL_URL,
      useValue: environment.asyncTaskThumbnailUrl,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://semantic-ui.com',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://www.kskgg.de',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://upload.wikimedia.org',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://logosmarken.com',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://play-lh.googleusercontent.com',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://www.girokonto.org',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://www.fuerthwiki.de',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://yt3.ggpht.com',
      multi: true,
    },
    {
      provide: PRECONNECT_CHECK_BLOCKLIST,
      useValue: 'https://activemq.apache.org',
      multi: true,
    },
  ],
};
