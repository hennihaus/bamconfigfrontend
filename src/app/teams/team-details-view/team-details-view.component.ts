import { Component, inject, input } from '@angular/core';
import { Team } from '../../shared/models/team';
import { RandomAvatarPipe } from '../pipes/random-avator.pipe';
import { HasPassedStatusPipe } from '../pipes/has-passed-status.pipe';
import { TeamDetailsViewStatisticsComponent } from '../team-details-view-statistics/team-details-view-statistics.component';
import { DatePipe, NgOptimizedImage } from '@angular/common';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';

@Component({
  selector: 'bam-team-details-view',
  imports: [
    TeamDetailsViewStatisticsComponent,
    DatePipe,
    HasPassedStatusPipe,
    RandomAvatarPipe,
    NgOptimizedImage,
    ImageErrorPipe,
  ],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './team-details-view.component.html',
  styleUrl: './team-details-view.component.css',
})
export class TeamDetailsViewComponent {
  readonly imageError = inject(ImageErrorDirective);

  readonly team = input.required<Team>();
  readonly thumbnailUrl = input<string>();
}
