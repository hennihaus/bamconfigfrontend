import { TeamDetailsViewComponent } from './team-details-view.component';

describe('TeamDetailsViewComponent', () => {
  it('shows picture when team has valid thumbnail', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsViewComponent, {
        componentProperties: {
          team,
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-details-image]').should(
        'have.attr',
        'src',
        'https://semantic-ui.com/images/avatar2/large/elyse.png',
      );
    });
  });

  it('shows an error picture when team has invalid thumbnail', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsViewComponent, {
        componentProperties: {
          team,
          thumbnailUrl: 'http://localhost:4200/unknown.png',
        },
      });

      // then
      cy.get('[data-cy=team-details-image]').should(
        'have.attr',
        'src',
        'http://localhost:4200/unknown.png',
      );
    });
  });

  it('shows picture when team has no thumbnail', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsViewComponent, {
        componentProperties: {
          team,
        },
      });

      // then
      cy.get('[data-cy=team-details-image]')
        .should('have.attr', 'src')
        .then((src) => {
          expect(src).to.contain(
            'https://semantic-ui.com/images/avatar2/large',
          );
        });
    });
  });

  it('shows a list of students in the team', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsViewComponent, {
        componentProperties: {
          team: {
            ...team,
            students: [
              {
                uuid: '474e90cb-de6e-47f2-844a-44ea7dc09516',
                firstname: 'Angela',
                lastname: 'Merkel',
              },
              {
                uuid: '31b1631b-66c9-4720-94fc-95891a38f964',
                firstname: 'Max',
                lastname: 'Mustermann',
              },
            ],
          },
        },
      });

      // then
      cy.get('[data-cy=team-details-student]').should('have.length', 2);
      cy.get('[data-cy=team-details-student]')
        .first()
        .contains('Angela Merkel');
      cy.get('[data-cy=team-details-student]')
        .last()
        .contains('Max Mustermann');
      cy.get('[data-cy=team-details-no-students-message]').should('not.exist');
    });
  });

  it('shows a message when no students are in the team', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsViewComponent, {
        componentProperties: {
          team: {
            ...team,
            students: [],
          },
        },
      });

      // then
      cy.get('[data-cy=team-details-student]').should('not.exist');
      cy.get('[data-cy=team-details-no-students-message]').should('be.visible');
    });
  });
});
