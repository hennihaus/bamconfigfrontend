import { inject } from '@angular/core';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { AsyncValidatorFn } from '@angular/forms';
import { catchError, map, of } from 'rxjs';

export const uniqueUsername = (): AsyncValidatorFn => {
  const teamStore = inject(TeamStoreService);

  return (control) => {
    const uuid = control.parent?.get('uuid')?.getRawValue();
    const username = control.getRawValue();

    if (!uuid || typeof uuid !== 'string') {
      return of(null);
    }
    if (!username || typeof username !== 'string') {
      return of(null);
    }

    return teamStore.isUsernameUnique(uuid, username).pipe(
      map((response) => response.isUnique),
      catchError(() => of(true)),
      map((isValid) => (isValid ? null : { unique: { term: username } })),
    );
  };
};
