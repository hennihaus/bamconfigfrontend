import { AsyncValidatorFn } from '@angular/forms';
import { inject } from '@angular/core';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { catchError, map, of } from 'rxjs';

export const uniquePassword = (): AsyncValidatorFn => {
  const teamStore = inject(TeamStoreService);

  return (control) => {
    const uuid = control.parent?.get('uuid')?.getRawValue();
    const password = control.getRawValue();

    if (!uuid || typeof uuid !== 'string') {
      return of(null);
    }
    if (!password || typeof password !== 'string') {
      return of(null);
    }

    return teamStore.isPasswordUnique(uuid, password).pipe(
      map((response) => response.isUnique),
      catchError(() => of(true)),
      map((isValid) => (isValid ? null : { unique: { term: password } })),
    );
  };
};
