import { isFormGroup, ValidatorFn } from '@angular/forms';

export const requiredNameValidator: ValidatorFn = (control) => {
  if (!isFormGroup(control)) {
    return null;
  }
  const firstname = control.get('firstname')?.getRawValue();
  const lastname = control.get('lastname')?.getRawValue();

  if (typeof firstname !== 'string' || typeof lastname !== 'string') {
    return null;
  }
  if (firstname && lastname) {
    return null;
  }
  if (!firstname && !lastname) {
    return null;
  }
  return { required: true };
};
