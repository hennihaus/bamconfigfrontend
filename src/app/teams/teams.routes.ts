import { Routes } from '@angular/router';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamCreateComponent } from './team-create/team-create.component';
import { leaveFormGuard } from '../shared/guards/leave-form.guard';

export const TEAMS_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: TeamListComponent,
    title: $localize`Teams`,
  },
  {
    path: 'details',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'details/:thumbnailUrl/:uuid',
    component: TeamDetailsComponent,
    title: $localize`Team`,
  },
  {
    path: 'details/:uuid',
    component: TeamDetailsComponent,
    title: $localize`Team`,
  },
  {
    path: 'create',
    component: TeamCreateComponent,
    title: $localize`Team erstellen`,
    canDeactivate: [leaveFormGuard],
  },
  {
    path: 'edit',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'edit/:thumbnailUrl/:uuid',
    component: TeamEditComponent,
    title: $localize`Team bearbeiten`,
    canDeactivate: [leaveFormGuard],
  },
  {
    path: 'edit/:uuid',
    component: TeamEditComponent,
    title: $localize`Team bearbeiten`,
    canDeactivate: [leaveFormGuard],
  },
];
