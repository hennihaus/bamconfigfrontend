import { TeamFormComponent } from './team-form.component';
import { createOutputSpy } from 'cypress/angular';

describe('TeamFormComponent', () => {
  it('shows empty and invalid form when team is not set', () => {
    // given
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
          teamSubmit: createOutputSpy('teamSubmitSpy'),
          hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
        },
      });
    });

    // then all fields are empty and error messages are shown
    cy.get('[data-cy=team-form-username-input]').should('have.value', '');
    cy.get('[data-cy=team-form-username-field]').contains('Pflichtfeld');
    cy.get('[data-cy=team-form-jms-queue-input]').should('have.value', '');
    cy.get('[data-cy=team-form-jms-queue-field]').contains('Pflichtfeld');
    cy.get('[data-cy=team-form-password-input]').should('have.value', '');
    cy.get('[data-cy=team-form-password-field]').contains('Pflichtfeld');
    cy.get('[data-cy=team-form-students-firstname-input]').should(
      'have.value',
      '',
    );
    cy.get('[data-cy=team-form-students-lastname-input]').should(
      'have.value',
      '',
    );
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'be.visible',
    );
    cy.get('[data-cy=team-form-unselected-statistic]')
      .should('have.length', 1)
      .should('contain', 'Sparkasse');
    cy.get('[data-cy=team-form-submit-button]')
      .should('have.css', 'background-color')
      .and('eq', 'rgb(219, 40, 40)');

    // when submit button is clicked
    cy.get('[data-cy=team-form-submit-button]').click();
    // then teamSubmitSpy is not called
    cy.get('@teamSubmitSpy').should('not.have.been.called');
    cy.get('@hasUnsavedChangesSpy').should('not.have.been.called');
  });

  it('shows prefilled and valid form when team is set', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/username/${team.username}`,
        { isUnique: true },
      ).as('isUsernameUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/jmsQueue/${team.jmsQueue}`,
        { isUnique: true },
      ).as('isJmsQueueUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/password/${team.password}`,
        { isUnique: true },
      ).as('isPasswordUniqueRequest');
      cy.fixture('banks').then((banks) => {
        cy.mount(TeamFormComponent, {
          componentProperties: {
            team,
            banks,
            isSubmitting: false,
            teamSubmit: createOutputSpy('teamSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        });
      });

      // when all validation is done
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      // then all fields are prefilled right and no errors are shown
      cy.get('[data-cy=team-form-username-input]').should(
        'have.value',
        team.username,
      );
      cy.get('[data-cy=team-form-jms-queue-input]').should(
        'have.value',
        team.jmsQueue,
      );
      cy.get('[data-cy=team-form-password-input]').should(
        'have.value',
        team.password,
      );
      cy.get('[data-cy=team-form-students-firstname-input]').should(
        'have.length',
        team.students.length,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').should(
        'have.length',
        team.students.length,
      );
      cy.get('[data-cy=team-form-students-firstname-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', team.students[index].firstname);
        },
      );
      cy.get('[data-cy=team-form-students-lastname-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', team.students[index].lastname);
        },
      );
      cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
        'be.visible',
      );
      cy.get('[data-cy=team-form-selected-statistic]')
        .should('have.length', 1)
        .should('contain', 'Sparkasse');
      cy.get('[data-cy=form-error-message]').should('not.exist');
      cy.get('[data-cy=team-form-submit-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');

      // when submit button is clicked
      cy.get('[data-cy=team-form-submit-button]').click();
      // then teamSubmitSpy is not called
      cy.get('@teamSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...team,
        createdAt: new Date(team.createdAt),
        updatedAt: new Date(team.updatedAt),
      });
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', false);
    });
  });

  it('creates team when all fields are filled and form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `/v1/teams/*/unique/username/*`, {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/jmsQueue/*`, {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/password/*`, {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.fixture('banks').then((banks) => {
        cy.mount(TeamFormComponent, {
          componentProperties: {
            banks,
            isSubmitting: false,
            teamSubmit: createOutputSpy('teamSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        });
      });

      // when
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=team-form-jms-queue-input]').type(team.jmsQueue);
      cy.get('[data-cy=team-form-password-input]').type(team.password);
      cy.get('[data-cy=team-form-students-add-button]').eq(0).click();
      cy.get('[data-cy=team-form-students-add-button]').eq(1).click();
      cy.get('[data-cy=team-form-students-firstname-input]').each(
        (el, index) => {
          cy.wrap(el).type(team.students[index].firstname);
        },
      );
      cy.get('[data-cy=team-form-students-lastname-input]').each(
        (el, index) => {
          cy.wrap(el).type(team.students[index].lastname);
        },
      );
      cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
      cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@teamSubmitSpy').should('have.been.calledOnce');
    });
  });

  it('updates team when all fields are changed and form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', `/v1/teams/*/unique/username/*`, {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/jmsQueue/*`, {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/password/*`, {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.fixture('banks').then((banks) => {
        cy.mount(TeamFormComponent, {
          componentProperties: {
            team,
            banks,
            isSubmitting: false,
            teamSubmit: createOutputSpy('teamSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        });
      });

      // when
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('TeamChanged');
      cy.get('[data-cy=team-form-jms-queue-input]').clear();
      cy.get('[data-cy=team-form-jms-queue-input]').type(
        'ResponseQueueTeamChanged',
      );
      cy.get('[data-cy=team-form-password-input]').clear();
      cy.get('[data-cy=team-form-password-input]').type('PasswordChanged');
      cy.get('[data-cy=team-form-students-firstname-input]').eq(0).clear();
      cy.get('[data-cy=team-form-students-firstname-input]').eq(0).type('Eric');
      cy.get('[data-cy=team-form-students-lastname-input]').eq(0).clear();
      cy.get('[data-cy=team-form-students-lastname-input]')
        .eq(0)
        .type('Gaitan');
      cy.get('[data-cy=team-form-students-firstname-input]').eq(1).clear();
      cy.get('[data-cy=team-form-students-firstname-input]')
        .eq(1)
        .type('Leandro');
      cy.get('[data-cy=team-form-students-lastname-input]').eq(1).clear();
      cy.get('[data-cy=team-form-students-lastname-input]')
        .eq(1)
        .type('Gaitan');
      cy.get('[data-cy=team-form-students-firstname-input]').eq(2).clear();
      cy.get('[data-cy=team-form-students-firstname-input]')
        .eq(2)
        .type('Celine');
      cy.get('[data-cy=team-form-students-lastname-input]').eq(2).clear();
      cy.get('[data-cy=team-form-students-lastname-input]')
        .eq(2)
        .type('Yilmaz');
      cy.get('[data-cy=team-form-selected-statistic]').dragOffItem();
      cy.get(
        '[data-cy=team-form-unselected-statistic-placeholder]',
      ).dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@teamSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...team,
        username: 'TeamChanged',
        jmsQueue: 'ResponseQueueTeamChanged',
        password: 'PasswordChanged',
        students: [
          {
            uuid: team.students[0].uuid,
            firstname: 'Eric',
            lastname: 'Gaitan',
          },
          {
            uuid: team.students[1].uuid,
            firstname: 'Leandro',
            lastname: 'Gaitan',
          },
          {
            uuid: team.students[2].uuid,
            firstname: 'Celine',
            lastname: 'Yilmaz',
          },
        ],
        statistics: {
          Schufa: 0,
          'Deutsche Bank': 0,
        },
        createdAt: new Date(team.createdAt),
        updatedAt: now,
      });
    });
  });

  it('shows no statistics and emits prefilled team with same statistics when banks are empty', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', `/v1/teams/*/unique/username/*`, {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/jmsQueue/*`, {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/password/*`, {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.mount(TeamFormComponent, {
        componentProperties: {
          team,
          banks: [],
          isSubmitting: false,
          teamSubmit: createOutputSpy('teamSubmitSpy'),
        },
      });
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.get('[data-cy=team-form-statistics]').should('not.exist');
      cy.get('@teamSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...team,
        createdAt: new Date(team.createdAt),
        updatedAt: now,
      });
    });
  });

  it('updates students when students are added and removed', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/username/${team.username}`,
        { isUnique: true },
      ).as('isUsernameUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/jmsQueue/${team.jmsQueue}`,
        { isUnique: true },
      ).as('isJmsQueueUniqueRequest');
      cy.intercept(
        'GET',
        `/v1/teams/${team.uuid}/unique/password/${team.password}`,
        { isUnique: true },
      ).as('isPasswordUniqueRequest');
      cy.fixture('banks').then((banks) => {
        cy.mount(TeamFormComponent, {
          componentProperties: {
            team,
            banks,
            isSubmitting: false,
            teamSubmit: createOutputSpy('teamSubmitSpy'),
          },
        });
      });
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // when student is removed at particular index and submitted
      cy.get('[data-cy=team-form-students-remove-button]').eq(1).click();
      cy.get('[data-cy=team-form-submit-button]').click();
      // then students array is filled without this student
      cy.get('@teamSubmitSpy').should('have.been.calledOnce');
      cy.get('@teamSubmitSpy').should('have.been.calledWithExactly', {
        ...team,
        createdAt: new Date(team.createdAt),
        updatedAt: now,
        students: [team.students[0], team.students[2]],
      });

      // when student is cleared at particular index and submitted
      cy.get('[data-cy=team-form-students-firstname-input]').eq(1).clear();
      cy.get('[data-cy=team-form-students-lastname-input]').eq(1).clear();
      cy.get('[data-cy=team-form-submit-button]').click();
      // then students array is filled without this student
      cy.get('@teamSubmitSpy').should('have.been.calledTwice');
      cy.get('@teamSubmitSpy').should('have.been.calledWithExactly', {
        ...team,
        createdAt: new Date(team.createdAt),
        updatedAt: now,
        students: [team.students[0]],
      });

      // when all students are removed
      cy.get('[data-cy=team-form-students-remove-button]').eq(1).click();
      cy.get('[data-cy=team-form-students-remove-button]').eq(0).click();
      // then there is at least one empty student field
      cy.get('[data-cy=team-form-students-firstname-input]').should(
        'have.value',
        '',
      );
      cy.get('[data-cy=team-form-students-lastname-input]').should(
        'have.value',
        '',
      );
      // when submit button is clicked
      cy.get('[data-cy=team-form-submit-button]').click();
      // then students array is empty
      cy.get('@teamSubmitSpy').should('have.been.calledThrice');
      cy.get('@teamSubmitSpy').should('have.been.calledWithExactly', {
        ...team,
        createdAt: new Date(team.createdAt),
        updatedAt: now,
        students: [],
      });
    });
  });

  it('updates password with right length and characters from sequence', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/password/*', {
      isUnique: true,
    }).as('isPasswordUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when
    cy.get('[data-cy=team-form-generate-password-button]').click();

    // then
    cy.wait(['@isPasswordUniqueRequest']);
    cy.get('[data-cy=team-form-password-input]')
      .invoke('val')
      .should('match', /^([A-Za-z]){10}$/);
  });

  it('shows errors for username', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/username/*', {
      isUnique: true,
    }).as('isUsernameUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when valid field is entered
    cy.get('[data-cy=team-form-username-input]').type('Team01');
    // then no error is shown
    cy.get('[data-cy=team-form-username-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
    // when field is cleared
    cy.get('[data-cy=team-form-username-input]').clear();
    // then error is shown
    cy.get('[data-cy=team-form-username-field]').contains('Pflichtfeld');
    // when too small field is entered
    cy.get('[data-cy=team-form-username-input]').clear();
    cy.get('[data-cy=team-form-username-input]').type('T'.repeat(5));
    // then error is shown
    cy.get('[data-cy=team-form-username-field]').contains('mindestens');
    // when too long field is entered
    cy.get('[data-cy=team-form-username-input]').clear();
    cy.get('[data-cy=team-form-username-input]').type('T'.repeat(51));
    // then error is shown
    cy.get('[data-cy=team-form-username-field]').contains('nicht länger');
    cy.wait('@isUsernameUniqueRequest');
    // when field is not unique
    cy.intercept('GET', '/v1/teams/*/unique/username/*', {
      isUnique: false,
    }).as('isUsernameUniqueRequest');
    cy.get('[data-cy=team-form-username-input]').clear();
    cy.get('[data-cy=team-form-username-input]').type('Team01');
    // then error is shown
    cy.wait('@isUsernameUniqueRequest');
    cy.get('[data-cy=team-form-username-field]').contains('existiert bereits');
  });

  it('shows no errors for valid username and unique username request fails', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/username/*', {
      statusCode: 500,
      body: {},
    }).as('isUsernameUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when
    cy.get('[data-cy=team-form-username-input]').type('Team01');

    // then
    cy.wait('@isUsernameUniqueRequest');
    cy.get('[data-cy=team-form-username-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
  });

  it('shows errors for jms queue', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/jmsQueue/*', {
      isUnique: true,
    }).as('isJmsQueueUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when valid field is entered
    cy.get('[data-cy=team-form-jms-queue-input]').type('Team01');
    // then no error is shown
    cy.get('[data-cy=team-form-jms-queue-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
    // when field is cleared
    cy.get('[data-cy=team-form-jms-queue-input]').clear();
    // then error is shown
    cy.get('[data-cy=team-form-jms-queue-field]').contains('Pflichtfeld');
    // when too small field is entered
    cy.get('[data-cy=team-form-jms-queue-input]').clear();
    cy.get('[data-cy=team-form-jms-queue-input]').type('T'.repeat(5));
    // then error is shown
    cy.get('[data-cy=team-form-jms-queue-field]').contains('mindestens');
    // when too long field is entered
    cy.get('[data-cy=team-form-jms-queue-input]').clear();
    cy.get('[data-cy=team-form-jms-queue-input]').type('T'.repeat(51));
    // then error is shown
    cy.get('[data-cy=team-form-jms-queue-field]').contains('nicht länger');
    cy.wait('@isJmsQueueUniqueRequest');
    // when field is not unique
    cy.intercept('GET', '/v1/teams/*/unique/jmsQueue/*', {
      isUnique: false,
    }).as('isJmsQueueUniqueRequest');
    cy.get('[data-cy=team-form-jms-queue-input]').clear();
    cy.get('[data-cy=team-form-jms-queue-input]').type('Team01');
    // then error is shown
    cy.wait('@isJmsQueueUniqueRequest');
    cy.get('[data-cy=team-form-jms-queue-field]').contains('existiert bereits');
  });

  it('shows no errors for valid jms queue and unique jms queue request fails', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/jmsQueue/*', {
      statusCode: 500,
      body: {},
    }).as('isJmsQueueUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when
    cy.get('[data-cy=team-form-jms-queue-input]').type('Team01');

    // then
    cy.wait('@isJmsQueueUniqueRequest');
    cy.get('[data-cy=team-form-jms-queue-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
  });

  it('shows errors for password', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/password/*', {
      isUnique: true,
    }).as('isPasswordUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when valid field is entered
    cy.get('[data-cy=team-form-password-input]').type('abcdefghij');
    // then no error is shown
    cy.get('[data-cy=team-form-password-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
    // when field is cleared
    cy.get('[data-cy=team-form-password-input]').clear();
    // then error is shown
    cy.get('[data-cy=team-form-password-field]').contains('Pflichtfeld');
    // when too small field is entered
    cy.get('[data-cy=team-form-password-input]').clear();
    cy.get('[data-cy=team-form-password-input]').type('a'.repeat(9));
    // then error is shown
    cy.get('[data-cy=team-form-password-field]').contains('mindestens');
    // when too long field is entered
    cy.get('[data-cy=team-form-password-input]').clear();
    cy.get('[data-cy=team-form-password-input]').type('a'.repeat(51));
    // then error is shown
    cy.get('[data-cy=team-form-password-field]').contains('nicht länger');
    cy.wait('@isPasswordUniqueRequest');
    // when field is not unique
    cy.intercept('GET', '/v1/teams/*/unique/password/*', {
      isUnique: false,
    }).as('isPasswordUniqueRequest');
    cy.get('[data-cy=team-form-password-input]').clear();
    cy.get('[data-cy=team-form-password-input]').type('abcdefghij');
    // then error is shown
    cy.wait('@isPasswordUniqueRequest');
    cy.get('[data-cy=team-form-password-field]').contains('existiert bereits');
  });

  it('shows no errors for valid password and unique password request fails', () => {
    // given
    cy.intercept('GET', '/v1/teams/*/unique/password/*', {
      statusCode: 500,
      body: {},
    }).as('isPasswordUniqueRequest');
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormComponent, {
        componentProperties: {
          banks,
          isSubmitting: false,
        },
      });
    });

    // when
    cy.get('[data-cy=team-form-password-input]').type('abcdefghij');

    // then
    cy.wait('@isPasswordUniqueRequest');
    cy.get('[data-cy=team-form-password-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
  });

  it('shows shows errors for students', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.fixture('banks').then((banks) => {
        cy.mount(TeamFormComponent, {
          componentProperties: {
            banks,
            isSubmitting: false,
          },
        });
      });

      // when valid students are entered
      cy.get('[data-cy=team-form-students-add-button]').click();
      cy.get('[data-cy=team-form-students-firstname-input]').each(
        (el, index) => {
          cy.wrap(el).type(team.students[index].firstname);
        },
      );
      cy.get('[data-cy=team-form-students-lastname-input]').each(
        (el, index) => {
          cy.wrap(el).type(team.students[index].lastname);
        },
      );
      // then no errors are shown
      cy.get('[data-cy=team-form-student-field]').each((el) => {
        cy.wrap(el).find('[data-cy=form-error-message]').should('not.exist');
      });
      // when firstnames are cleared
      cy.get('[data-cy=team-form-students-firstname-input]').each((el) => {
        cy.wrap(el).clear();
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-firstname-field]').each((el) => {
        cy.wrap(el).contains('Pflichtfeld');
      });
      // when lastnames are cleared
      cy.get('[data-cy=team-form-students-firstname-input]').each(
        (el, index) => {
          cy.wrap(el).type(team.students[index].firstname);
        },
      );
      cy.get('[data-cy=team-form-students-lastname-input]').each((el) => {
        cy.wrap(el).clear();
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-lastname-field]').each((el) => {
        cy.wrap(el).contains('Pflichtfeld');
      });
      // when too short lastnames are entered
      cy.get('[data-cy=team-form-students-lastname-input]').each((el) => {
        cy.wrap(el).type('G');
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-lastname-field]').each((el) => {
        cy.wrap(el).contains('mindestens');
      });
      // when too long lastnames are entered
      cy.get('[data-cy=team-form-students-lastname-input]').each((el) => {
        cy.wrap(el).clear();
        cy.wrap(el).type('G'.repeat(51));
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-lastname-field]').each((el) => {
        cy.wrap(el).contains('nicht länger');
      });
      // when too short firstnames are entered
      cy.get('[data-cy=team-form-students-firstname-input]').each((el) => {
        cy.wrap(el).clear();
        cy.wrap(el).type('G');
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-firstname-field]').each((el) => {
        cy.wrap(el).contains('mindestens');
      });
      // when too long firstnames are entered
      cy.get('[data-cy=team-form-students-firstname-input]').each((el) => {
        cy.wrap(el).clear();
        cy.wrap(el).type('G'.repeat(51));
      });
      // then errors are shown
      cy.get('[data-cy=team-form-student-firstname-field]').each((el) => {
        cy.wrap(el).contains('nicht länger');
      });
    });
  });
});
