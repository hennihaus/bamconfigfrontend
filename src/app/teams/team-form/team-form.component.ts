import {
  Component,
  computed,
  effect,
  inject,
  input,
  output,
} from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  FormRecord,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
  ValueChangeEvent,
} from '@angular/forms';
import { StudentForm, TeamForm } from '../../shared/models/team-form';
import { v4 as uuidv4 } from 'uuid';
import { TeamType } from '../../shared/models/team-type';
import { Bank } from '../../shared/models/bank';
import { Statistics, Team } from '../../shared/models/team';
import { requiredNameValidator } from '../validators/required-name.validator';
import { Student } from '../../shared/models/student';
import { filter, map } from 'rxjs';
import { AsyncStatisticsPipe } from '../pipes/async-statistics.pipe';
import { TeamFormStatisticsComponent } from '../team-form-statistics/team-form-statistics.component';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import { PASSWORD_LENGTH, PASSWORD_SEQUENCE } from '../../../tokens';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { uniqueUsername } from '../validators/unique-username.validator';
import { uniqueJmsQueue } from '../validators/unique-jms-queue.validator';
import { uniquePassword } from '../validators/unique-password.validator';

@Component({
  selector: 'bam-team-form',
  imports: [
    ReactiveFormsModule,
    FormErrorMessageComponent,
    TeamFormStatisticsComponent,
    AsyncStatisticsPipe,
  ],
  templateUrl: './team-form.component.html',
  styleUrl: './team-form.component.css',
})
export class TeamFormComponent {
  private readonly passwordLength = inject(PASSWORD_LENGTH);
  private readonly passwordSequence = inject(PASSWORD_SEQUENCE);
  private readonly formBuilder = inject(NonNullableFormBuilder);

  readonly isSubmitting = input.required<boolean>();
  readonly team = input<Team>();
  readonly banks = input.required<Bank[]>();

  readonly teamSubmit = output<Team>();
  readonly hasUnsavedChanges = output<boolean>();

  readonly asyncBanks = computed(() =>
    this.banks().filter((bank) => bank.isAsync),
  );

  private readonly initialTeamId = uuidv4();
  private readonly initialStudentId = uuidv4();
  private readonly initialForm = this.createEmptyTeamFormValues();

  readonly form = this.createEmptyTeamFormValues();

  constructor() {
    effect(() => {
      const team = this.team();
      const banks = this.banks();

      /*
       * TEMPORARY WORKAROUND!!!
       * This is a bridge between Signals and Zone.js.
       * We should use banks input signal directly when creating the empty form in the future
       * and should remove the code here. The input prop banks is not available when create empty form is called.
       */
      this.setInitialStatisticsFormValues(banks);
      this.setStatisticsFormValues(banks);

      if (team) {
        this.setInitialTeamFormValues(team);
        this.setTeamFormValues(team);
      }
    });

    this.form.events
      .pipe(
        takeUntilDestroyed(),
        filter(
          (event): event is ValueChangeEvent<unknown> =>
            event instanceof ValueChangeEvent,
        ),
        map(() => [this.initialForm.getRawValue(), this.form.getRawValue()]),
        map(
          ([initialFormValue, currentFormValue]) =>
            JSON.stringify(initialFormValue) !==
            JSON.stringify(currentFormValue),
        ),
      )
      .subscribe((hasUnsavedChanges) =>
        this.hasUnsavedChanges.emit(hasUnsavedChanges),
      );
  }

  generatePassword() {
    let password = '';

    for (let i = 0; i < this.passwordLength; i++) {
      password = `${password}${this.passwordSequence.charAt(
        Math.floor(Math.random() * this.passwordSequence.length),
      )}`;
    }

    this.form.patchValue({ password });
  }

  addStudent() {
    const student = this.getBaseStudent();
    const control = this.buildStudentFormValues(student);
    this.students.push(control);
  }

  removeStudent(index: number) {
    const FIRST_STUDENT_ITEM = 0;
    const ONE_STUDENT = 1;

    if (index === FIRST_STUDENT_ITEM && this.students.length === ONE_STUDENT) {
      const student = this.getBaseStudent();
      this.students.reset([student]);
    } else {
      this.students.removeAt(index);
    }
  }

  replaceStatistics(statistics: Statistics) {
    const control = this.formBuilder.record<number>(
      this.getSyncBankNames(this.banks()).reduce(
        (statistics, bank) => ({
          ...statistics,
          [bank]: this.statistics.get(bank)?.getRawValue() ?? 0,
        }),
        {},
      ),
    );

    for (const statistic in statistics) {
      control.addControl(
        statistic,
        this.formBuilder.control(statistics[statistic]),
      );
    }

    this.form.setControl('statistics', control);
  }

  submitForm() {
    if (this.form.invalid) {
      return;
    }

    const team = this.form.getRawValue();
    const students = team?.students.filter(
      (student) => !!student.firstname && !!student.lastname,
    );
    const createdAt = this.team() ? new Date(team.createdAt) : new Date();
    const updatedAt = new Date();

    this.teamSubmit.emit({
      ...team,
      students,
      createdAt,
      updatedAt,
    });
  }

  private setInitialStatisticsFormValues(banks: Bank[]) {
    const baseStatistics = this.getBaseStatistics(banks);
    const statistics = this.buildStatisticsFormValues(baseStatistics);

    this.initialForm.setControl('statistics', statistics);
  }

  private setInitialTeamFormValues(team: Team) {
    const students = this.buildStudentsFormValues(team.students);
    const statistics = this.buildStatisticsFormValues(team.statistics);

    this.initialForm.patchValue(team);
    this.initialForm.setControl('students', students);
    this.initialForm.setControl('statistics', statistics);
  }

  private setStatisticsFormValues(banks: Bank[]) {
    const baseStatistics = this.getBaseStatistics(banks);
    const statistics = this.buildStatisticsFormValues(baseStatistics);

    this.initialForm.setControl('statistics', statistics);
  }

  private setTeamFormValues(team: Team) {
    const students = this.buildStudentsFormValues(team.students);
    const statistics = this.buildStatisticsFormValues(team.statistics);

    this.form.patchValue(team);
    this.form.setControl('students', students);
    this.form.setControl('statistics', statistics);
  }

  private createEmptyTeamFormValues(): FormGroup<TeamForm> {
    const student = this.getBaseStudent();

    return this.formBuilder.group({
      uuid: this.initialTeamId,
      type: this.formBuilder.control(TeamType.REGULAR),
      username: this.formBuilder.control('', {
        validators: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
        asyncValidators: [uniqueUsername()],
      }),
      password: this.formBuilder.control('', {
        validators: [
          Validators.required,
          Validators.minLength(this.passwordLength),
          Validators.maxLength(50),
        ],
        asyncValidators: [uniquePassword()],
      }),
      jmsQueue: this.formBuilder.control('', {
        validators: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
        asyncValidators: [uniqueJmsQueue()],
      }),
      students: this.buildStudentsFormValues([
        { ...student, uuid: this.initialStudentId },
      ]),
      statistics: this.buildStatisticsFormValues({}),
      hasPassed: this.formBuilder.control(false),
      createdAt: new Date(),
    });
  }

  private buildStudentsFormValues(
    students: Student[],
  ): FormArray<FormGroup<StudentForm>> {
    return this.formBuilder.array(
      students.map((student) => this.buildStudentFormValues(student)),
    );
  }

  private buildStudentFormValues(student: Student): FormGroup<StudentForm> {
    return this.formBuilder.group(
      {
        uuid: student.uuid,
        firstname: [
          student.firstname,
          [Validators.minLength(2), Validators.maxLength(50)],
        ],
        lastname: [
          student.lastname,
          [Validators.minLength(2), Validators.maxLength(50)],
        ],
      },
      { validators: [requiredNameValidator] },
    );
  }

  private buildStatisticsFormValues(
    statistics: Statistics,
  ): FormRecord<FormControl<number>> {
    return this.formBuilder.record(statistics);
  }

  private getBaseStudent(): Student {
    return {
      uuid: uuidv4(),
      firstname: '',
      lastname: '',
    };
  }

  private getBaseStatistics(banks: Bank[]): Statistics {
    return this.getSyncBankNames(banks).reduce(
      (statistics, bank) => ({ ...statistics, [bank]: 0 }),
      {},
    );
  }

  private getSyncBankNames(banks: Bank[]): string[] {
    return banks.filter((bank) => !bank.isAsync).map((bank) => bank.name);
  }

  get students() {
    return this.form.controls.students;
  }

  get statistics() {
    return this.form.controls.statistics;
  }
}
