import { Component, inject, input } from '@angular/core';
import { Team } from '../../shared/models/team';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { TeamDetailsOptionsComponent } from '../team-details-options/team-details-options.component';
import { TeamDetailsViewComponent } from '../team-details-view/team-details-view.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-team-details',
  imports: [
    TeamDetailsViewComponent,
    TeamDetailsOptionsComponent,
    MessageComponent,
    LoadingComponent,
  ],
  templateUrl: './team-details.component.html',
  styleUrl: './team-details.component.css',
})
export class TeamDetailsComponent {
  private readonly teamStore = inject(TeamStoreService);

  readonly uuid = input.required<string>();
  readonly thumbnailUrl = input<string>();

  readonly teamResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.teamStore.getSingle(uuid),
  });

  updateTeam(team: Team) {
    this.teamResource.set(team);
  }
}
