import { TeamDetailsComponent } from './team-details.component';

describe('TeamDetailsComponent', () => {
  it('shows team details with options on init', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.intercept('GET', `v1/teams/${team.uuid}`, {
        body: team,
        delay: 250, // milliseconds
      }).as('getTeamRequest');
      cy.mount(TeamDetailsComponent, {
        componentProperties: {
          uuid: team.uuid,
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });
    });

    // when
    cy.get('[data-cy=loading-overlay]').should('be.visible');
    cy.wait('@getTeamRequest');

    // then
    cy.get('[data-cy=loading-overlay]').should('not.exist');
    cy.get('[data-cy=team-details-image]').should(
      'have.attr',
      'src',
      'https://semantic-ui.com/images/avatar2/large/elyse.png',
    );
    cy.get('[data-cy=team-details-left-table]').should('be.visible');
    cy.get('[data-cy=team-details-right-table]').should('be.visible');
    cy.get('[data-cy=team-details-edit-link]').should('be.visible');
  });

  it('shows error message with internal server error on init', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.intercept('GET', `v1/teams/${team.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('getTeamRequest');
      cy.mount(TeamDetailsComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });
    });

    // when
    cy.wait('@getTeamRequest');

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=team-details-image]').should('not.exist');
    cy.get('[data-cy=team-details-left-table]').should('not.exist');
    cy.get('[data-cy=team-details-right-table]').should('not.exist');
    cy.get('[data-cy=team-details-edit-link]').should('not.exist');
  });

  it('updates has passed status and stats in view when team statistics are reset', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', `v1/teams/${team.uuid}`, {
        body: {
          ...team,
          hasPassed: true,
          statistics: {
            Schufa: 1,
            'Deutsche Bank': 2,
            Sparkasse: 3,
          },
        },
      }).as('getTeamRequest');
      cy.intercept('DELETE', `v1/teams/${team.uuid}/statistics`, {
        body: {
          ...team,
          hasPassed: false,
          statistics: {
            Schufa: 0,
            'Deutsche Bank': 0,
            Sparkasse: 0,
          },
        },
        delay: 250, // milliseconds
      }).as('resetStatisticsRequest');
      cy.mount(TeamDetailsComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });
      cy.wait('@getTeamRequest');
      cy.get('[data-cy=team-details-has-passed-status]').contains('Bestanden');
      cy.get('[data-cy=team-details-has-passed-status]').should(
        'not.contain',
        'Nicht',
      );
      cy.get('[data-cy=team-details-left-table-icon]').each(($icon) =>
        expect($icon).to.have.class('icon checkmark green'),
      );
      cy.get('[data-cy=team-details-right-table-icon]').each(($icon) =>
        expect($icon).to.have.class('icon checkmark green'),
      );

      // when
      cy.get('[data-cy=team-details-reset-statistics-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@resetStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=team-details-has-passed-status]').contains(
        'Nicht Bestanden',
      );
      cy.get('[data-cy=team-details-left-table-icon]').each(($icon) =>
        expect($icon).to.have.class('icon close red'),
      );
      cy.get('[data-cy=team-details-right-table-icon]').each(($icon) =>
        expect($icon).to.have.class('icon close red'),
      );
    });
  });
});
