import {
  Component,
  computed,
  inject,
  input,
  linkedSignal,
  output,
} from '@angular/core';
import {
  TeamQuery,
  TeamQueryForm,
  TeamQueryParams,
} from '../../shared/models/team-query';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { TeamType } from '../../shared/models/team-type';
import { FormsModule } from '@angular/forms';
import { TypeStatusPipe } from '../pipes/type-status.pipe';
import { KeyValuePipe, LowerCasePipe } from '@angular/common';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-team-list-filter',
  imports: [FormsModule, LowerCasePipe, KeyValuePipe, TypeStatusPipe],
  templateUrl: './team-list-filter.component.html',
  styleUrl: './team-list-filter.component.css',
})
export class TeamListFilterComponent {
  private readonly bankStore = inject(BankStoreService);

  readonly teamQuery = input.required<TeamQuery>();
  readonly isSubmitting = input.required<boolean>();
  readonly teamQueryParamsSubmit = output<TeamQueryParams>();

  readonly teamQueryForm = linkedSignal(() => {
    const teamQuery = this.teamQuery();

    return this.getInitialFormValues(teamQuery);
  });

  readonly banksResource = rxResource({
    loader: () => this.bankStore.getAll(),
  });
  readonly asyncBanks = computed(
    () => this.banksResource.value()?.filter((bank) => bank.isAsync) ?? [],
  );

  setBanks(event: Event) {
    const value = (event.target as HTMLInputElement).value;
    const isChecked = (event.target as HTMLInputElement).checked;

    if (isChecked) {
      this.teamQueryForm().banks.add(value);
    } else {
      this.teamQueryForm().banks.delete(value);
    }
  }

  resetForm() {
    this.teamQueryForm.set(this.getInitialFormValues());
    this.submitForm();
  }

  submitForm() {
    const queryParams = this.buildTeamQueryParams();
    this.teamQueryParamsSubmit.emit(queryParams);
  }

  private getInitialFormValues(query?: TeamQuery): TeamQueryForm {
    if (query) {
      return {
        username: query.username ? query.username : '',
        jmsQueue: query.jmsQueue ? query.jmsQueue : '',
        ...(query.hasPassed !== undefined && {
          hasPassed: query.hasPassed ? 'PASSED' : 'NOT_PASSED',
        }),
        ...(query.type && { type: query.type }),
        minRequests: query.minRequests?.toString()
          ? `${query.minRequests}`
          : '',
        maxRequests: query.maxRequests?.toString()
          ? `${query.maxRequests}`
          : '',
        studentFirstname: query.studentFirstname ? query.studentFirstname : '',
        studentLastname: query.studentLastname ? query.studentLastname : '',
        banks: query.banks ? new Set(query.banks) : new Set(),
      };
    }
    return {
      username: '',
      jmsQueue: '',
      minRequests: '',
      maxRequests: '',
      studentFirstname: '',
      studentLastname: '',
      banks: new Set(),
    };
  }

  private buildTeamQueryParams() {
    const query: TeamQueryParams = {
      ...this.teamQueryForm(),
      banks: Array.from(this.teamQueryForm().banks).sort(),
      minRequests: this.teamQueryForm().minRequests?.toString(),
      maxRequests: this.teamQueryForm().maxRequests?.toString(),
    };

    if (!query.type) {
      delete query.type;
    }
    if (!query.username) {
      delete query.username;
    }
    if (!query.password) {
      delete query.password;
    }
    if (!query.jmsQueue) {
      delete query.jmsQueue;
    }
    if (!query.hasPassed) {
      delete query.hasPassed;
    }
    if (!query.minRequests) {
      delete query.minRequests;
    }
    if (!query.maxRequests) {
      delete query.maxRequests;
    }
    if (!query.studentFirstname) {
      delete query.studentFirstname;
    }
    if (!query.studentLastname) {
      delete query.studentLastname;
    }
    if (!query.banks?.length) {
      delete query.banks;
    }

    return query;
  }

  protected readonly TeamType = TeamType;
}
