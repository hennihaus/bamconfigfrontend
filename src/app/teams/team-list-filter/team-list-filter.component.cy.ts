import { TeamListFilterComponent } from './team-list-filter.component';
import { createOutputSpy } from 'cypress/angular';
import { TeamType } from '../../shared/models/team-type';

describe('TeamListFilterComponent', () => {
  it('emits empty form when component has no initial team query', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', 'v1/banks', {
        body: [
          { ...bank, isAsync: true, name: 'Sparkasse' },
          { ...bank, isAsync: true, name: 'Deutsche Bank' },
          { ...bank, isAsync: true, name: 'Commerzbank' },
        ],
      }).as('getBanksRequest');
      cy.mount(TeamListFilterComponent, {
        componentProperties: {
          teamQuery: {
            limit: 0,
          },
          isSubmitting: false,
          teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
        },
      });

      // when
      cy.get('[data-cy=team-list-filter-submit-button]').click();

      // then
      cy.wait('@getBanksRequest');
      cy.get('[data-cy=team-list-filter-passed-radio-button]').should(
        'not.be.checked',
      );
      cy.get('[data-cy=team-list-filter-not-passed-radio-button]').should(
        'not.be.checked',
      );
      cy.get('[data-cy=team-list-filter-both-passed-radio-button]').should(
        'be.checked',
      );
      cy.get('[data-cy=team-list-filter-type-radio-button]')
        .should('have.length', 2)
        .each(($el) => {
          cy.wrap($el).should('not.be.checked');
        });
      cy.get('[data-cy=team-list-filter-both-types-radio-button]').should(
        'be.checked',
      );
      cy.get('[data-cy=team-list-filter-left-bank-checkbox]')
        .should('have.length', 2)
        .each(($el) => {
          cy.wrap($el).should('not.be.checked');
        });
      cy.get('[data-cy=team-list-filter-right-bank-checkbox]')
        .should('have.length', 1)
        .each(($el) => {
          cy.wrap($el).should('not.be.checked');
        });
      cy.get('@teamQueryParamsSubmit').should(
        'have.been.calledOnceWithExactly',
        {},
      );
    });
  });

  it('emits filled form when component has no initial team query', () => {
    cy.fixture('bank').then((bank) => {
      cy.fixture('team').then((team) => {
        // given
        cy.intercept('GET', 'v1/banks', {
          body: [
            { ...bank, isAsync: true, name: 'Sparkasse' },
            { ...bank, isAsync: true, name: 'Deutsche Bank' },
            { ...bank, isAsync: true, name: 'Commerzbank' },
          ],
        }).as('getBanksRequest');
        cy.mount(TeamListFilterComponent, {
          componentProperties: {
            teamQuery: {
              limit: 0,
            },
            isSubmitting: false,
            teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
          },
        });

        // when
        cy.get('[data-cy=team-list-filter-username-input]').type(team.username);
        cy.get('[data-cy=team-list-filter-jms-queue-input]').type(
          team.jmsQueue,
        );
        cy.get('[data-cy=team-list-filter-passed-radio-button]').check();
        cy.get('[data-cy=team-list-filter-type-radio-button]').first().check();
        cy.get('[data-cy=team-list-filter-min-requests-input]').type('0');
        cy.get('[data-cy=team-list-filter-max-requests-input]').type('1');
        cy.get('[data-cy=team-list-filter-student-firstname-input]').type(
          team.students[0].firstname,
        );
        cy.get('[data-cy=team-list-filter-student-lastname-input]').type(
          team.students[0].lastname,
        );
        cy.get('[data-cy=team-list-filter-left-bank-checkbox]').each((el) =>
          cy.wrap(el).check(),
        );
        cy.get('[data-cy=team-list-filter-right-bank-checkbox]').each((el) =>
          cy.wrap(el).check(),
        );
        cy.get('[data-cy=team-list-filter-submit-button]').click();

        // then
        cy.wait('@getBanksRequest');
        cy.get('@teamQueryParamsSubmit').should(
          'have.been.calledOnceWithExactly',
          {
            type: 'EXAMPLE',
            username: team.username,
            jmsQueue: team.jmsQueue,
            hasPassed: 'PASSED',
            minRequests: '0',
            maxRequests: '1',
            studentFirstname: team.students[0].firstname,
            studentLastname: team.students[0].lastname,
            banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
          },
        );
      });
    });
  });

  it('emits prefilled form when component is initialized with team query', () => {
    cy.fixture('bank').then((bank) => {
      cy.fixture('team').then((team) => {
        // given
        cy.intercept('GET', 'v1/banks', {
          body: [
            { ...bank, isAsync: true, name: 'Sparkasse' },
            { ...bank, isAsync: true, name: 'Deutsche Bank' },
            { ...bank, isAsync: true, name: 'Commerzbank' },
          ],
        }).as('getBanksRequest');
        cy.mount(TeamListFilterComponent, {
          componentProperties: {
            teamQuery: {
              limit: 0,
              type: TeamType.EXAMPLE,
              username: team.username,
              jmsQueue: team.jmsQueue,
              hasPassed: true,
              minRequests: 0,
              maxRequests: 1,
              studentFirstname: team.students[0].firstname,
              studentLastname: team.students[0].lastname,
              banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
            },
            isSubmitting: false,
            teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
          },
        });
        cy.wait('@getBanksRequest');

        // when
        cy.get('[data-cy=team-list-filter-submit-button]').click();

        // then
        cy.get('[data-cy=team-list-filter-username-input]').should(
          'have.value',
          team.username,
        );
        cy.get('[data-cy=team-list-filter-jms-queue-input]').should(
          'have.value',
          team.jmsQueue,
        );
        cy.get('[data-cy=team-list-filter-passed-radio-button]').should(
          'be.checked',
        );
        cy.get('[data-cy=team-list-filter-not-passed-radio-button]').should(
          'not.be.checked',
        );
        cy.get('[data-cy=team-list-filter-both-passed-radio-button]').should(
          'not.be.checked',
        );
        cy.get('[data-cy=team-list-filter-type-radio-button]')
          .should('have.length', 2)
          .first()
          .should('be.checked');
        cy.get('[data-cy=team-list-filter-type-radio-button]')
          .should('have.length', 2)
          .last()
          .should('not.be.checked');
        cy.get('[data-cy=team-list-filter-both-types-radio-button]').should(
          'not.be.checked',
        );
        cy.get('[data-cy=team-list-filter-min-requests-input]').should(
          'have.value',
          '0',
        );
        cy.get('[data-cy=team-list-filter-max-requests-input]').should(
          'have.value',
          '1',
        );
        cy.get('[data-cy=team-list-filter-student-firstname-input]').should(
          'have.value',
          team.students[0].firstname,
        );
        cy.get('[data-cy=team-list-filter-student-lastname-input]').should(
          'have.value',
          team.students[0].lastname,
        );
        cy.get('[data-cy=team-list-filter-left-bank-checkbox]').each(($el) => {
          cy.wrap($el).should('be.checked');
        });
        cy.get('[data-cy=team-list-filter-right-bank-checkbox]').each(($el) => {
          cy.wrap($el).should('be.checked');
        });
        cy.get('@teamQueryParamsSubmit').should(
          'have.been.calledOnceWithExactly',
          {
            type: 'EXAMPLE',
            username: team.username,
            jmsQueue: team.jmsQueue,
            hasPassed: 'PASSED',
            minRequests: '0',
            maxRequests: '1',
            studentFirstname: team.students[0].firstname,
            studentLastname: team.students[0].lastname,
            banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
          },
        );
      });
    });
  });

  it('emits prefilled form when no banks are loaded', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', 'v1/banks', { body: [] }).as('getBanksRequest');
      cy.mount(TeamListFilterComponent, {
        componentProperties: {
          teamQuery: {
            limit: 0,
            type: TeamType.EXAMPLE,
            username: team.username,
            jmsQueue: team.jmsQueue,
            hasPassed: true,
            minRequests: 0,
            maxRequests: 1,
            studentFirstname: team.students[0].firstname,
            studentLastname: team.students[0].lastname,
            banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
          },
          isSubmitting: false,
          teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
        },
      });
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-list-filter-submit-button]').click();

      // then
      cy.get('[data-cy=team-list-filter-banks-label]').should('not.exist');
      cy.get('[data-cy=team-list-filter-left-bank-checkbox]').should(
        'not.exist',
      );
      cy.get('[data-cy=team-list-filter-right-bank-checkbox]').should(
        'not.exist',
      );
      cy.get('@teamQueryParamsSubmit').should(
        'have.been.calledOnceWithExactly',
        {
          type: 'EXAMPLE',
          username: team.username,
          jmsQueue: team.jmsQueue,
          hasPassed: 'PASSED',
          minRequests: '0',
          maxRequests: '1',
          studentFirstname: team.students[0].firstname,
          studentLastname: team.students[0].lastname,
          banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
        },
      );
    });
  });

  it('emits prefilled form when no banks are loaded with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', 'v1/banks', { body: {}, statusCode: 500 }).as(
        'getBanksRequest',
      );
      cy.mount(TeamListFilterComponent, {
        componentProperties: {
          teamQuery: {
            limit: 0,
            type: TeamType.EXAMPLE,
            username: team.username,
            jmsQueue: team.jmsQueue,
            hasPassed: true,
            minRequests: 0,
            maxRequests: 1,
            studentFirstname: team.students[0].firstname,
            studentLastname: team.students[0].lastname,
            banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
          },
          isSubmitting: false,
          teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
        },
      });
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-list-filter-submit-button]').click();

      // then
      cy.get('[data-cy=team-list-filter-banks-label]').should('not.exist');
      cy.get('[data-cy=team-list-filter-left-bank-checkbox]').should(
        'not.exist',
      );
      cy.get('[data-cy=team-list-filter-right-bank-checkbox]').should(
        'not.exist',
      );
      cy.get('@teamQueryParamsSubmit').should(
        'have.been.calledOnceWithExactly',
        {
          type: 'EXAMPLE',
          username: team.username,
          jmsQueue: team.jmsQueue,
          hasPassed: 'PASSED',
          minRequests: '0',
          maxRequests: '1',
          studentFirstname: team.students[0].firstname,
          studentLastname: team.students[0].lastname,
          banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
        },
      );
    });
  });

  it('emits empty form when prefilled is reset', () => {
    cy.fixture('bank').then((bank) => {
      cy.fixture('team').then((team) => {
        // given
        cy.intercept('GET', 'v1/banks', {
          body: [
            { ...bank, isAsync: true, name: 'Sparkasse' },
            { ...bank, isAsync: true, name: 'Deutsche Bank' },
            { ...bank, isAsync: true, name: 'Commerzbank' },
          ],
        }).as('getBanksRequest');
        cy.mount(TeamListFilterComponent, {
          componentProperties: {
            teamQuery: {
              limit: 0,
              type: TeamType.EXAMPLE,
              username: team.username,
              jmsQueue: team.jmsQueue,
              hasPassed: true,
              minRequests: 0,
              maxRequests: 1,
              studentFirstname: team.students[0].firstname,
              studentLastname: team.students[0].lastname,
              banks: ['Commerzbank', 'Deutsche Bank', 'Sparkasse'],
            },
            isSubmitting: false,
            teamQueryParamsSubmit: createOutputSpy('teamQueryParamsSubmit'),
          },
        });

        // when
        cy.get('[data-cy=team-list-filter-reset-button]').click();

        // then
        cy.wait('@getBanksRequest');
        cy.get('@teamQueryParamsSubmit').should(
          'have.been.calledOnceWithExactly',
          {},
        );
      });
    });
  });

  it('is not possible to insert negative in min and max requests', () => {
    // given
    cy.intercept('GET', 'v1/banks', { fixture: 'banks' }).as('getBanksRequest');
    cy.mount(TeamListFilterComponent, {
      componentProperties: {
        teamQuery: {
          limit: 0,
        },
        isSubmitting: false,
      },
    });

    // when
    cy.get('[data-cy=team-list-filter-min-requests-input]').type('-');
    cy.get('[data-cy=team-list-filter-min-requests-input]').type('1');
    cy.get('[data-cy=team-list-filter-max-requests-input]').type('-');
    cy.get('[data-cy=team-list-filter-max-requests-input]').type('1');

    // then
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=team-list-filter-min-requests-input]').should(
      'have.value',
      '1',
    );
    cy.get('[data-cy=team-list-filter-max-requests-input]').should(
      'have.value',
      '1',
    );
  });

  it('is not possible to insert decimals in min and max requests', () => {
    // given
    cy.intercept('GET', 'v1/banks', { fixture: 'banks' }).as('getBanksRequest');
    cy.mount(TeamListFilterComponent, {
      componentProperties: {
        teamQuery: {
          limit: 0,
        },
        isSubmitting: false,
      },
    });

    // when
    cy.get('[data-cy=team-list-filter-min-requests-input]').type('1.1');
    cy.get('[data-cy=team-list-filter-min-requests-input]').type('1,1');
    cy.get('[data-cy=team-list-filter-max-requests-input]').type('1.1');
    cy.get('[data-cy=team-list-filter-max-requests-input]').type('1,1');

    // then
    cy.wait('@getBanksRequest');
    cy.get('[data-cy=team-list-filter-min-requests-input]').should(
      'have.value',
      '111',
    );
    cy.get('[data-cy=team-list-filter-max-requests-input]').should(
      'have.value',
      '111',
    );
  });
});
