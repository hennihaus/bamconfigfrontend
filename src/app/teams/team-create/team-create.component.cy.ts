import { TeamCreateComponent } from './team-create.component';
import { Student } from '../../shared/models/student';

describe('TeamCreateComponent', () => {
  it('creates team when team form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', '/v1/banks', { fixture: 'banks', delay: 250 }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', '/v1/teams/*/unique/username/*', {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', '/v1/teams/*/unique/jmsQueue/*', {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', '/v1/teams/*/unique/password/*', {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', '/v1/teams/*', {
        body: team,
        delay: 250, // milliseconds
      }).as('updateTeamRequest');
      cy.mount(TeamCreateComponent);

      // when component is loaded
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@getBanksRequest');
      // then it shows team create form
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=team-create-headline]').should('be.visible');
      cy.get('[data-cy=team-form]').should('be.visible');
      cy.get('[data-cy=team-form-statistics]').should('be.visible');

      // when team form is submitted
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=team-form-jms-queue-input]').type(team.jmsQueue);
      cy.get('[data-cy=team-form-password-input]').type(team.password);
      cy.get('[data-cy=team-form-students-firstname-input]').type(
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').type(
        team.students[0].lastname,
      );
      cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
      cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();
      // then it creates team
      cy.get('[data-cy=team-form-submit-button]').should('be.disabled');
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      cy.wait('@updateTeamRequest')
        .its('request.body')
        .then((requestBody) => {
          expect(requestBody).to.deep.equal({
            ...team,
            uuid: requestBody.uuid,
            students: requestBody.students.map((student: Student) => ({
              ...student,
              uuid: requestBody.students[0].uuid,
            })),
            createdAt: '2024-05-26T12:00:00',
            updatedAt: '2024-05-26T12:00:00',
          });
        });
      cy.get('[data-cy=loading-overlay]').should('not.exist');
    });
  });

  it('shows error message and team form when team submit responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/*/unique/username/*`, {
        isUnique: true,
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/jmsQueue/*`, {
        isUnique: true,
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/*/unique/password/*`, {
        isUnique: true,
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `v1/teams/*`, {
        statusCode: 500,
        body: {},
      }).as('updateTeamRequest');
      cy.mount(TeamCreateComponent);
      cy.wait('@getBanksRequest');

      // when
      cy.get('[data-cy=team-form-username-input]').type(team.username);
      cy.get('[data-cy=team-form-jms-queue-input]').type(team.jmsQueue);
      cy.get('[data-cy=team-form-password-input]').type(team.password);
      cy.get('[data-cy=team-form-students-firstname-input]').type(
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').type(
        team.students[0].lastname,
      );
      cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
      cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
      cy.get('[data-cy=team-form-submit-button]').click();

      // then
      cy.wait([
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
        '@updateTeamRequest',
      ]);
      cy.get('[data-cy=team-form-username-input]').should(
        'have.value',
        team.username,
      );
      cy.get('[data-cy=team-form-jms-queue-input]').should(
        'have.value',
        team.jmsQueue,
      );
      cy.get('[data-cy=team-form-password-input]').should(
        'have.value',
        team.password,
      );
      cy.get('[data-cy=team-form-students-firstname-input]').should(
        'have.value',
        team.students[0].firstname,
      );
      cy.get('[data-cy=team-form-students-lastname-input]').should(
        'have.value',
        team.students[0].lastname,
      );
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows form with no statistics drag and drop with internal server error on page load', () => {
    // given
    cy.intercept('GET', '/v1/banks', { statusCode: 500, body: {} }).as(
      'getBanksRequest',
    );
    cy.mount(TeamCreateComponent);

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.get('[data-cy=team-form]').should('be.visible');
    cy.get('[data-cy=messsage]').should('not.exist');
    cy.get('[data-cy=team-form-statistics]').should('not.exist');
  });
});
