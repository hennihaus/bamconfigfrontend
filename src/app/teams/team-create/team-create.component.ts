import { Component, inject, linkedSignal, signal } from '@angular/core';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { catchError, EMPTY, finalize, tap } from 'rxjs';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { Team } from '../../shared/models/team';
import { Router } from '@angular/router';
import { LeaveFormGuard } from '../../shared/guards/leave-form.guard';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { TeamFormComponent } from '../team-form/team-form.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-team-create',
  imports: [MessageComponent, TeamFormComponent, LoadingComponent],
  templateUrl: './team-create.component.html',
  styleUrl: './team-create.component.css',
})
export class TeamCreateComponent implements LeaveFormGuard {
  private readonly bankStore = inject(BankStoreService);
  private readonly teamStore = inject(TeamStoreService);
  private readonly router = inject(Router);

  readonly banksResource = rxResource({
    loader: () => this.bankStore.getAll(),
  });
  readonly isLoading = linkedSignal(() => this.banksResource.isLoading());

  readonly hasError = signal(false);
  readonly hasUnsavedChanges = signal(false);

  setHasUnsavedChanges(hasUnsavedChanges: boolean) {
    this.hasUnsavedChanges.set(hasUnsavedChanges);
  }

  createTeam(team: Team) {
    this.isLoading.set(true);
    this.hasError.set(false);
    this.teamStore
      .update(team)
      .pipe(
        tap(() => this.hasUnsavedChanges.set(false)),
        tap({
          next: () => this.hasError.set(false),
          error: () => this.hasError.set(true),
        }),
        catchError(() => EMPTY),
        finalize(() => this.isLoading.set(false)),
      )
      .subscribe((team) => this.router.navigate(['/teams/details', team.uuid]));
  }
}
