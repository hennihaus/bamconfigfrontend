import { TeamDetailsOptionsComponent } from './team-details-options.component';
import { TeamType } from '../../shared/models/team-type';
import { createOutputSpy } from 'cypress/angular';
import { Team } from '../../shared/models/team';

describe('TeamDetailsOptionsComponent', () => {
  it('shows four buttons when team is a regular team', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team: {
            ...team,
            type: TeamType.REGULAR,
          },
        },
      });
    });

    // then
    cy.get('[data-cy=team-details-delete-button]').should('be.visible');
    cy.get('[data-cy=team-details-edit-link]').should('be.visible');
    cy.get('[data-cy=team-details-reset-queue-button]').should('be.visible');
    cy.get('[data-cy=team-details-reset-statistics-button]').should(
      'be.visible',
    );
  });

  it('shows three buttons when team is an example team', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team: {
            ...team,
            type: TeamType.EXAMPLE,
          },
        },
      });
    });

    // then
    cy.get('[data-cy=team-details-delete-button]').should('not.exist');
    cy.get('[data-cy=team-details-edit-link]').should('be.visible');
    cy.get('[data-cy=team-details-reset-queue-button]').should('be.visible');
    cy.get('[data-cy=team-details-reset-statistics-button]').should(
      'be.visible',
    );
  });

  it('shows error message when team deletion responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });
      cy.intercept('DELETE', `v1/teams/${team.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('deleteTeamRequest');

      // when
      cy.get('[data-cy=team-details-delete-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@deleteTeamRequest');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows success message when queue is reset', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });
      cy.intercept('DELETE', `v1/activemq/${team.jmsQueue}`, {
        statusCode: 204,
        body: '',
        delay: 45, // milliseconds
      }).as('resetQueueRequest');

      // when
      cy.get('[data-cy=team-details-reset-queue-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@resetQueueRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(44, 102, 45)');
      cy.get('@teamChangeSpy').should('not.have.been.called');
    });
  });

  it('shows no message when queue reset was rejected in the confirmation dialog', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });

      // when
      cy.get('[data-cy=team-details-reset-queue-button]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]').should('not.exist');
    });
  });

  it('shows error message when deleting a queue responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });
      cy.intercept('DELETE', `v1/activemq/${team.jmsQueue}`, {
        statusCode: 500,
        body: {},
      }).as('resetQueueRequest');

      // when
      cy.get('[data-cy=team-details-reset-queue-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@resetQueueRequest');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('emits new team when statistics are reset', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team: {
            ...team,
            statistics: {
              Schufa: 1,
              'Deutsche Bank': 2,
              Sparkasse: 3,
            },
          },
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });
      cy.intercept('DELETE', `v1/teams/${team.uuid}/statistics`, {
        statusCode: 200,
        body: {
          ...team,
          statistics: {
            Schufa: 0,
            'Deutsche Bank': 0,
            Sparkasse: 0,
          },
        },
        delay: 45, // milliseconds
      }).as('resetStatisticsRequest');

      // when
      cy.get('[data-cy=team-details-reset-statistics-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.get('[data-cy=message]').should('not.exist');
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@resetStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]').should('not.exist');
      cy.get('@teamChangeSpy').should('have.been.calledOnceWithExactly', {
        ...team,
        statistics: {
          Schufa: 0,
          'Deutsche Bank': 0,
          Sparkasse: 0,
        },
      });
    });
  });

  it('emits no team when statistics reset was rejected in the confirmation dialog', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });

      // when
      cy.get('[data-cy=team-details-reset-statistics-button]').click();
      cy.on('window:confirm', () => false);

      // then
      cy.get('@teamChangeSpy').should('not.have.been.called');
      cy.get('[data-cy=message]').should('not.exist');
    });
  });

  it('shows error message when statistics reset responds with internal server error', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamDetailsOptionsComponent, {
        componentProperties: {
          team,
          teamChange: createOutputSpy<Team>('teamChangeSpy'),
        },
      });
      cy.intercept('DELETE', `v1/teams/${team.uuid}/statistics`, {
        statusCode: 500,
        body: {},
      }).as('resetStatisticsRequest');

      // when
      cy.get('[data-cy=team-details-reset-statistics-button]').click();
      cy.on('window:confirm', () => true);

      // then
      cy.wait('@resetStatisticsRequest');
      cy.get('@teamChangeSpy').should('not.have.been.called');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });
});
