import {
  Component,
  computed,
  inject,
  input,
  output,
  signal,
} from '@angular/core';
import { Team } from '../../shared/models/team';
import { TeamType } from '../../shared/models/team-type';
import { Router, RouterLink } from '@angular/router';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { catchError, tap } from 'rxjs';
import { Message } from '../../shared/models/message';
import { BrokerStoreService } from '../../shared/services/broker-store.service';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { ErrorHandlerService } from '../../shared/services/error-handler.service';

@Component({
  selector: 'bam-team-details-options',
  imports: [RouterLink, MessageComponent, LoadingComponent],
  templateUrl: './team-details-options.component.html',
  styleUrl: './team-details-options.component.css',
})
export class TeamDetailsOptionsComponent {
  private readonly teamStore = inject(TeamStoreService);
  private readonly brokerStore = inject(BrokerStoreService);
  private readonly errorHandler = inject(ErrorHandlerService);
  private readonly router = inject(Router);

  readonly thumbnailUrl = input<string>();

  // TODO: migrate to model when cypress component testing is able to deal with it
  readonly team = input.required<Team>();
  readonly teamChange = output<Team>();

  readonly teamEditLink = computed(() => {
    const BASE_PATH = '/teams/edit';

    const thumbnailUrl = this.thumbnailUrl();
    const team = this.team();

    if (thumbnailUrl) {
      return [BASE_PATH, thumbnailUrl, team.uuid];
    }
    return [BASE_PATH, team.uuid];
  });

  readonly isLoading = signal(false);
  readonly message = signal<Message | null>(null);

  deleteTeam() {
    if (confirm($localize`Team wirklich löschen?`)) {
      this.isLoading.set(true);
      this.message.set(null);
      this.teamStore
        .delete(this.team().uuid)
        .pipe(
          catchError(() => this.errorHandler.getServerErrorMessageAsync()),
          tap(() => this.isLoading.set(false)),
        )
        .subscribe((response) => {
          if (typeof response === 'string') {
            this.router.navigateByUrl('/teams');
          } else {
            this.message.set(response);
          }
        });
    }
  }

  deleteQueue() {
    if (confirm($localize`JMS-Queue wirklich leeren?`)) {
      this.isLoading.set(true);
      this.message.set(null);
      this.brokerStore
        .deleteQueue(this.team().jmsQueue)
        .pipe(
          catchError(() => this.errorHandler.getServerErrorMessageAsync()),
          tap(() => this.isLoading.set(false)),
        )
        .subscribe((response) => {
          if (typeof response === 'string') {
            this.message.set({
              text: $localize`:jmsQueue is a placeholder and should stay in every language:JMS-Queue ${this.team().jmsQueue} erfolgreich zurückgesetzt`,
              type: 'positive' as const,
            });
          } else {
            this.message.set(response);
          }
        });
    }
  }

  resetStatistics() {
    if (
      confirm(
        $localize`Statistiken wirklich zurücksetzen? Der Status wird damit auf nicht bestanden zurückgesetzt.`,
      )
    ) {
      this.isLoading.set(true);
      this.message.set(null);
      this.teamStore
        .resetStatistics(this.team().uuid)
        .pipe(
          catchError(() => this.errorHandler.getServerErrorMessageAsync()),
          tap(() => this.isLoading.set(false)),
        )
        .subscribe((response) => {
          if ('uuid' in response) {
            this.teamChange.emit(response);
          } else {
            this.message.set(response);
          }
        });
    }
  }

  protected readonly TeamType = TeamType;
}
