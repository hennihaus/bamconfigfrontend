import { Pipe, PipeTransform } from '@angular/core';
import { Statistics } from '../../shared/models/team';
import { Bank } from '../../shared/models/bank';

@Pipe({
  name: 'asyncStatistics',
  pure: false,
})
export class AsyncStatisticsPipe implements PipeTransform {
  transform(oldStatistics: Statistics, asyncBanks: Bank[]): Statistics {
    return Object.entries(oldStatistics)
      .filter(([bank]) => asyncBanks.map((bank) => bank.name).includes(bank))
      .reduce(
        (statistics, [bank, requests]) => ({ ...statistics, [bank]: requests }),
        {},
      );
  }
}
