import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hasPassedStatus',
  pure: false,
})
export class HasPassedStatusPipe implements PipeTransform {
  transform(hasPassed: boolean): string {
    return hasPassed ? $localize`Bestanden` : $localize`'Nicht Bestanden'`;
  }
}
