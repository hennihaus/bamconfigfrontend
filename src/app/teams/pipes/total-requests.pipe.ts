import { Pipe, PipeTransform } from '@angular/core';
import { Statistics } from '../../shared/models/team';

@Pipe({
  name: 'totalRequests',
  pure: false,
})
export class TotalRequestsPipe implements PipeTransform {
  transform(statistics: Statistics): number {
    return Object.values(statistics).reduce(
      (requests, request) => requests + request,
      0,
    );
  }
}
