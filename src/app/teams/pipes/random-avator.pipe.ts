import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'randomAvatar',
  pure: false,
})
export class RandomAvatarPipe implements PipeTransform {
  private random = Math.random();

  transform(size = 'large' as const): string {
    const names = [
      'elyse',
      'kristy',
      'lena',
      'lindsay',
      'mark',
      'matthew',
      'molly',
      'patrick',
      'rachel',
    ];
    const name = names[Math.floor(this.random * names.length)];

    return `https://semantic-ui.com/images/avatar2/${size}/${name}.png`;
  }
}
