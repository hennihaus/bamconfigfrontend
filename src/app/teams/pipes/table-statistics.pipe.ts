import { Pipe, PipeTransform } from '@angular/core';
import { Statistics } from '../../shared/models/team';
import { TableStatistic } from '../../shared/models/table-statistic';

@Pipe({
  name: 'tableStatistics',
  pure: false,
})
export class TableStatisticsPipe implements PipeTransform {
  transform(statistics: Statistics, totalRequests: number): TableStatistic[] {
    const ZERO_REQUESTS = 0;

    return Object.entries(statistics).map(([bank, requests]) => ({
      bank,
      requests,
      percentage: requests ? requests / totalRequests : ZERO_REQUESTS,
      hasPassed: requests > ZERO_REQUESTS,
    }));
  }
}
