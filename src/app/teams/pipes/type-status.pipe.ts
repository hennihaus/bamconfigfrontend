import { Pipe, PipeTransform } from '@angular/core';
import { TeamType } from '../../shared/models/team-type';

@Pipe({
  name: 'typeStatus',
  pure: false,
})
export class TypeStatusPipe implements PipeTransform {
  transform(type: TeamType): string {
    switch (type) {
      case TeamType.EXAMPLE:
        return $localize`Beispiel`;
      case TeamType.REGULAR:
        return $localize`Regulär`;
    }
  }
}
