import { Component, computed, input } from '@angular/core';
import { Statistics } from '../../shared/models/team';
import { TableStatisticsPipe } from '../pipes/table-statistics.pipe';
import { PercentPipe } from '@angular/common';

@Component({
  selector: 'bam-team-details-view-statistics',
  imports: [PercentPipe, TableStatisticsPipe],
  templateUrl: './team-details-view-statistics.component.html',
  styleUrl: './team-details-view-statistics.component.css',
})
export class TeamDetailsViewStatisticsComponent {
  readonly statistics = input.required<Statistics>();

  readonly totalRequests = computed(() => {
    const statistics = this.statistics();

    return Object.values(statistics).reduce(
      (requests, request) => requests + request,
      0,
    );
  });
}
