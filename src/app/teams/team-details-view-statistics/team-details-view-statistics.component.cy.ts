import { TeamDetailsViewStatisticsComponent } from './team-details-view-statistics.component';

describe('TeamDetailsViewStatisticsComponent', () => {
  it('shows one bank only in one table', () => {
    cy.fixture('team').then(({ statistics }) => {
      // given
      cy.mount(TeamDetailsViewStatisticsComponent, {
        componentProperties: {
          statistics,
        },
      });

      // then
      cy.get('[data-cy=team-details-left-table-row]').should('have.length', 2);
      cy.get('[data-cy=team-details-left-table-row]').should(
        'contain',
        'Schufa',
      );
      cy.get('[data-cy=team-details-left-table-row]').should(
        'contain',
        'Sparkasse',
      );
      cy.get('[data-cy=team-details-left-table-row]').should(
        'not.contain',
        'Deutsche Bank',
      );
      cy.get('[data-cy=team-details-right-table-row]').should('have.length', 1);
      cy.get('[data-cy=team-details-right-table-row]').should(
        'not.contain',
        'Schufa',
      );
      cy.get('[data-cy=team-details-right-table-row]').should(
        'not.contain',
        'Sparkasse',
      );
      cy.get('[data-cy=team-details-right-table-row]').should(
        'contain',
        'Deutsche Bank',
      );
    });
  });

  it('shows red icon when team has no requests for bank', () => {
    // given
    cy.mount(TeamDetailsViewStatisticsComponent, {
      componentProperties: {
        statistics: {
          Schufa: 0,
          'Deutsche Bank': 0,
        },
      },
    });

    // then
    cy.get('[data-cy=team-details-left-table-icon]').should(
      'have.class',
      'icon close red',
    );
    cy.get('[data-cy=team-details-left-table-icon]').should(
      'not.have.class',
      'icon checkmark green',
    );
    cy.get('[data-cy=team-details-right-table-icon]').should(
      'have.class',
      'icon close red',
    );
    cy.get('[data-cy=team-details-right-table-icon]').should(
      'not.have.class',
      'icon checkmark green',
    );
  });

  it('shows green icon when team has more than one requests for bank', () => {
    // given
    cy.mount(TeamDetailsViewStatisticsComponent, {
      componentProperties: {
        statistics: {
          Schufa: 1,
          'Deutsche Bank': 1,
        },
      },
    });

    // then
    cy.get('[data-cy=team-details-left-table-icon]').should(
      'not.have.class',
      'icon close red',
    );
    cy.get('[data-cy=team-details-left-table-icon]').should(
      'have.class',
      'icon checkmark green',
    );
    cy.get('[data-cy=team-details-right-table-icon]').should(
      'not.have.class',
      'icon close red',
    );
    cy.get('[data-cy=team-details-right-table-icon]').should(
      'have.class',
      'icon checkmark green',
    );
  });

  it('shows right percentages for banks', () => {
    // given
    cy.mount(TeamDetailsViewStatisticsComponent, {
      componentProperties: {
        statistics: {
          Schufa: 0,
          'Deutsche Bank': 1,
          Sparkasse: 2,
        },
      },
    });

    // then
    cy.get('[data-cy=team-details-left-table-percentage]')
      .first()
      .should('contain', '0,00');
    cy.get('[data-cy=team-details-left-table-percentage]')
      .last()
      .should('contain', '66,67');
    cy.get('[data-cy=team-details-right-table-percentage]').should(
      'contain',
      '33,33',
    );
  });
});
