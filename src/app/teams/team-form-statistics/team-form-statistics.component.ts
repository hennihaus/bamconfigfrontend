import { Component, computed, input, output, signal } from '@angular/core';
import { Statistics } from '../../shared/models/team';
import { Bank } from '../../shared/models/bank';
import {
  CdkDrag,
  CdkDragDrop,
  CdkDragEnter,
  CdkDropList,
  CdkDropListGroup,
  transferArrayItem,
} from '@angular/cdk/drag-drop';
import { StatisticForm } from '../../shared/models/team-form';

@Component({
  selector: 'bam-team-form-statistics',
  imports: [CdkDropListGroup, CdkDropList, CdkDrag],
  templateUrl: './team-form-statistics.component.html',
  styleUrl: './team-form-statistics.component.css',
})
export class TeamFormStatisticsComponent {
  readonly statistics = input.required<Statistics>();
  readonly banks = input.required<Bank[]>();

  readonly statisticsDrop = output<Statistics>();

  readonly selectedStatistics = computed(() =>
    Object.entries(this.statistics()).map(([bank, requests]) => ({
      bank,
      requests,
    })),
  );
  readonly unselectedStatistics = computed(() =>
    this.banks()
      .filter(
        (bank) => !Object.keys(this.statistics() ?? {}).includes(bank.name),
      )
      .map((bank) => ({ bank: bank.name, requests: 0 })),
  );

  readonly showSelectedStatisticsPlaceholder = signal(false);
  readonly showUnselectedStatisticsPlaceholder = signal(false);

  changeStatistic(event: CdkDragDrop<StatisticForm[]>) {
    transferArrayItem(
      event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex,
    );

    this.showSelectedStatisticsPlaceholder.set(false);
    this.showUnselectedStatisticsPlaceholder.set(false);

    this.statisticsDrop.emit(
      this.selectedStatistics().reduce(
        (statistics, { bank, requests }) => ({
          ...statistics,
          [bank]: requests,
        }),
        {},
      ),
    );
  }

  setSelectedPlaceholder(event: CdkDragEnter<StatisticForm[]>) {
    if (event.item.dropContainer.data.length <= 1) {
      this.showSelectedStatisticsPlaceholder.set(true);
    }
  }

  setUnselectedPlaceholder(event: CdkDragEnter<StatisticForm[]>) {
    if (event.item.dropContainer.data.length <= 1) {
      this.showUnselectedStatisticsPlaceholder.set(true);
    }
  }
}
