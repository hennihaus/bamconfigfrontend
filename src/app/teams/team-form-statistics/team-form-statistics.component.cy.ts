import { TeamFormStatisticsComponent } from './team-form-statistics.component';
import { createOutputSpy } from 'cypress/angular';

describe('TeamFormStatisticsComponent', () => {
  it('shows right selected and unselected statistics', () => {
    // given
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormStatisticsComponent, {
        componentProperties: {
          banks,
          statistics: {
            Schufa: 0,
          },
        },
      });
    });

    // then
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 1);
    cy.get('[data-cy=team-form-selected-statistic]').eq(0).contains('Schufa');
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 2);
    cy.get('[data-cy=team-form-unselected-statistic]')
      .eq(0)
      .contains('Deutsche Bank');
    cy.get('[data-cy=team-form-unselected-statistic]')
      .eq(1)
      .contains('Sparkasse');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );
  });

  it('emits statistics and shows placeholder when all statistics are selected', () => {
    // given
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormStatisticsComponent, {
        componentProperties: {
          banks,
          statistics: {},
          statisticsDrop: createOutputSpy('statisticsDropSpy'),
        },
      });
    });

    // when first item is moved
    cy.get('[data-cy=team-form-unselected-statistic]').eq(0).dragOffItem();
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 1);
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 2);
    cy.get('@statisticsDropSpy').should('have.been.calledOnce');
    cy.get('@statisticsDropSpy').should('have.been.calledOnceWithExactly', {
      Schufa: 0,
    });
    cy.get('[data-cy=team-form-selected-statistic]').contains('Schufa');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );

    // when second item is moved
    cy.get('[data-cy=team-form-unselected-statistic]').eq(0).dragOffItem();
    cy.get('[data-cy=team-form-selected-statistic]').dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 2);
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 1);
    cy.get('@statisticsDropSpy').should('have.been.calledTwice');
    cy.get('@statisticsDropSpy').should('have.been.calledWithExactly', {
      Schufa: 0,
      'Deutsche Bank': 0,
    });
    cy.get('[data-cy=team-form-selected-statistic]').contains('Deutsche Bank');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );

    // when third item is moved
    cy.get('[data-cy=team-form-unselected-statistic]').dragOffItem();
    cy.get('[data-cy=team-form-selected-statistic]').eq(0).dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 3);
    cy.get('[data-cy=team-form-unselected-statistic]').should('not.exist');
    cy.get('@statisticsDropSpy').should('have.been.calledThrice');
    cy.get('@statisticsDropSpy').should('have.been.calledWithExactly', {
      Schufa: 0,
      'Deutsche Bank': 0,
      Sparkasse: 0,
    });
    cy.get('[data-cy=team-form-selected-statistic]').contains('Sparkasse');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'be.visible',
    );
  });

  it('emits statistics and shows placeholder when all statistics are unselected', () => {
    // given
    cy.fixture('banks').then((banks) => {
      cy.mount(TeamFormStatisticsComponent, {
        componentProperties: {
          banks,
          statistics: {
            Schufa: 0,
            'Deutsche Bank': 1,
            Sparkasse: 2,
          },
          statisticsDrop: createOutputSpy('statisticsDropSpy'),
        },
      });
    });

    // when first item is moved
    cy.get('[data-cy=team-form-selected-statistic]').eq(0).dragOffItem();
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 2);
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 1);
    cy.get('@statisticsDropSpy').should('have.been.calledOnce');
    cy.get('@statisticsDropSpy').should('have.been.calledOnceWithExactly', {
      'Deutsche Bank': 1,
      Sparkasse: 2,
    });
    cy.get('[data-cy=team-form-unselected-statistic]').contains('Schufa');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );

    // when second item is moved
    cy.get('[data-cy=team-form-selected-statistic]').eq(0).dragOffItem();
    cy.get('[data-cy=team-form-unselected-statistic]').dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('have.length', 1);
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 2);
    cy.get('@statisticsDropSpy').should('have.been.calledTwice');
    cy.get('@statisticsDropSpy').should('have.been.calledWithExactly', {
      Sparkasse: 2,
    });
    cy.get('[data-cy=team-form-unselected-statistic]').contains(
      'Deutsche Bank',
    );
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'not.exist',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );

    // when third item is moved
    cy.get('[data-cy=team-form-selected-statistic]').dragOffItem();
    cy.get('[data-cy=team-form-unselected-statistic]').eq(0).dropOnItem();
    // then event is emitted and items are on right side
    cy.get('[data-cy=team-form-selected-statistic]').should('not.exist');
    cy.get('[data-cy=team-form-unselected-statistic]').should('have.length', 3);
    cy.get('@statisticsDropSpy').should('have.been.calledThrice');
    cy.get('@statisticsDropSpy').should('have.been.calledWithExactly', {});
    cy.get('[data-cy=team-form-unselected-statistic]').contains('Sparkasse');
    cy.get('[data-cy=team-form-selected-statistic-placeholder]').should(
      'be.visible',
    );
    cy.get('[data-cy=team-form-unselected-statistic-placeholder]').should(
      'not.exist',
    );
  });
});
