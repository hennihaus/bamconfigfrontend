import { TeamListItemComponent } from './team-list-item.component';
import { TeamType } from '../../shared/models/team-type';

describe('TeamListItemComponent', () => {
  it('shows picture when team has valid thumbnail', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team,
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-image]').should(
        'have.attr',
        'src',
        'https://semantic-ui.com/images/avatar2/large/elyse.png',
      );
    });
  });

  it('shows an error picture when team has invalid thumbnail', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team,
          thumbnailUrl: 'http://localhost:4200/unknown.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-image]').should(
        'have.attr',
        'src',
        '/placeholder.png',
      );
    });
  });

  it('shows correct type status when team is an example team', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team: {
            ...team,
            type: TeamType.EXAMPLE,
          },
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-type]').contains('Beispiel');
    });
  });

  it('shows correct type status when team is a regular team', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team: {
            ...team,
            type: TeamType.REGULAR,
          },
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-type]').contains('Regulär');
    });
  });

  it('shows correct total requests when team has made 6 bank requests', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team: {
            ...team,
            statistics: {
              First: 1,
              Second: 2,
              Third: 3,
            },
          },
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-total-requests]').contains('6');
    });
  });

  it('shows correct total requests when team has no banks', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team: {
            ...team,
            statistics: {},
          },
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-total-requests]').contains('0');
    });
  });

  it('shows correct text when team has three students', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team,
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-students]').contains(
        'Angela Merkel, Max Mustermann, Thomas Müller',
      );
      cy.get('[data-cy=team-list-item-students]').should(
        'not.contain',
        'Thomas Müller,',
      );
    });
  });

  it('shows correct text when team has no students', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.mount(TeamListItemComponent, {
        componentProperties: {
          team: {
            ...team,
            students: [],
          },
          thumbnailUrl:
            'https://semantic-ui.com/images/avatar2/large/elyse.png',
        },
      });

      // then
      cy.get('[data-cy=team-list-item-students]').contains('Keine Studenten');
    });
  });
});
