import { Component, inject, input } from '@angular/core';
import { Team } from '../../shared/models/team';
import { HasPassedStatusPipe } from '../pipes/has-passed-status.pipe';
import { TotalRequestsPipe } from '../pipes/total-requests.pipe';
import { TypeStatusPipe } from '../pipes/type-status.pipe';
import { NgOptimizedImage } from '@angular/common';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';

@Component({
  selector: 'bam-team-list-item',
  imports: [
    TypeStatusPipe,
    TotalRequestsPipe,
    HasPassedStatusPipe,
    NgOptimizedImage,
    ImageErrorPipe,
  ],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './team-list-item.component.html',
  styleUrl: './team-list-item.component.css',
})
export class TeamListItemComponent {
  readonly imageError = inject(ImageErrorDirective);

  readonly team = input.required<Team>();
  readonly thumbnailUrl = input.required<string>();
}
