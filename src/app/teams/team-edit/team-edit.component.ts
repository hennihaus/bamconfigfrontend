import {
  Component,
  effect,
  inject,
  input,
  linkedSignal,
  ResourceStatus,
  signal,
} from '@angular/core';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { Router } from '@angular/router';
import { catchError, EMPTY, finalize, tap } from 'rxjs';
import { Team } from '../../shared/models/team';
import { LeaveFormGuard } from '../../shared/guards/leave-form.guard';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { TeamFormComponent } from '../team-form/team-form.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-team-edit',
  imports: [MessageComponent, TeamFormComponent, LoadingComponent],
  templateUrl: './team-edit.component.html',
  styleUrl: './team-edit.component.css',
})
export class TeamEditComponent implements LeaveFormGuard {
  private readonly bankStore = inject(BankStoreService);
  private readonly teamStore = inject(TeamStoreService);
  private readonly router = inject(Router);

  readonly uuid = input.required<string>();
  readonly thumbnailUrl = input<string>();

  readonly banksResource = rxResource({
    loader: () => this.bankStore.getAll(),
  });
  readonly teamResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.teamStore.getSingle(uuid),
  });
  readonly isLoading = linkedSignal(
    () => this.banksResource.isLoading() || this.teamResource.isLoading(),
  );
  readonly error = linkedSignal(() => this.teamResource.error());

  readonly hasUnsavedChanges = signal(false);

  constructor() {
    effect(() => {
      if (this.teamResource.status() === ResourceStatus.Resolved) {
        this.hasUnsavedChanges.set(false);
      }
    });
  }

  setHasUnsavedChanges(hasUnsavedChanges: boolean) {
    this.hasUnsavedChanges.set(hasUnsavedChanges);
  }

  updateTeam(team: Team) {
    this.isLoading.set(true);
    this.error.set(null);
    this.teamStore
      .update(team)
      .pipe(
        tap(() => this.hasUnsavedChanges.set(false)),
        tap({
          next: () => this.error.set(null),
          error: (error) => this.error.set(error),
        }),
        catchError(() => EMPTY),
        finalize(() => this.isLoading.set(false)),
      )
      .subscribe((team) => this.navigateToDetailsPage(team.uuid));
  }

  navigateToDetailsPage(uuid: string) {
    const TEAM_DETAILS_PATH = '/teams/details';

    if (this.thumbnailUrl()) {
      this.router.navigate([TEAM_DETAILS_PATH, this.thumbnailUrl(), uuid]);
    } else {
      this.router.navigate([TEAM_DETAILS_PATH, uuid]);
    }
  }
}
