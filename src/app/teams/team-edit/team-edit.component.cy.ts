import { TeamEditComponent } from './team-edit.component';

describe('TeamEditComponent', () => {
  it('updates team when team form is submitted', () => {
    cy.fixture('team').then((team) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', '/v1/banks', { fixture: 'banks', delay: 250 }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, {
        body: team,
        delay: 250, // milliseconds
      }).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `/v1/teams/${team.uuid}`, {
        body: team,
        delay: 250, // milliseconds
      }).as('updateTeamRequest');
      cy.mount(TeamEditComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });

      // when component is loaded
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
      // then it shows team edit form
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=team-edit-headline]').should('be.visible');
      cy.get('[data-cy=team-form]').should('be.visible');
      cy.get('[data-cy=team-form-statistics]').should('be.visible');

      // when team form is submitted
      cy.get('[data-cy=team-form-username-input]').clear();
      cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
      cy.get('[data-cy=team-form-submit-button]').click();
      // then it updates team
      cy.get('[data-cy=team-form-submit-button]').should('be.disabled');
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@updateTeamRequest')
        .its('request.body')
        .should('deep.equal', {
          ...team,
          username: 'NewTeamUsername',
          createdAt: '2022-12-26T07:49:22',
          updatedAt: '2024-05-26T12:00:00',
        });
      cy.get('[data-cy=loading-overlay]').should('not.exist');
    });
  });

  it('shows error message and team form when submit responds with internal server error', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, team).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.intercept('PUT', `/v1/teams/${team.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateTeamRequest');
      cy.mount(TeamEditComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);
    });

    // when
    cy.get('[data-cy=team-form-username-input]').clear();
    cy.get('[data-cy=team-form-username-input]').type('NewTeamUsername');
    cy.get('[data-cy=team-form-submit-button]').click();

    // then
    cy.wait('@updateTeamRequest');
    cy.get('[data-cy=team-form-username-input]').should(
      'have.value',
      'NewTeamUsername',
    );
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
  });

  it('shows error message with internal server error on load with team', () => {
    // given
    cy.fixture('team').then((team) => {
      cy.intercept('GET', '/v1/banks', { fixture: 'banks' }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('getTeamRequest');
      cy.mount(TeamEditComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });
    });

    // when
    cy.wait(['@getBanksRequest', '@getTeamRequest']);

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=team-edit-headline]').should('not.exist');
    cy.get('[data-cy=team-form]').should('not.exist');
    cy.get('[data-cy=team-form-statistics]').should('not.exist');
  });

  it('shows form with no statistics drag and drop with internal server error on load with banks', () => {
    cy.fixture('team').then((team) => {
      // given
      cy.intercept('GET', '/v1/banks', { body: {}, statusCode: 500 }).as(
        'getBanksRequest',
      );
      cy.intercept('GET', `/v1/teams/${team.uuid}`, {
        body: team,
      }).as('getTeamRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/username/*`, {
        body: { isUnique: true },
      }).as('isUsernameUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/jmsQueue/*`, {
        body: { isUnique: true },
      }).as('isJmsQueueUniqueRequest');
      cy.intercept('GET', `/v1/teams/${team.uuid}/unique/password/*`, {
        body: { isUnique: true },
      }).as('isPasswordUniqueRequest');
      cy.mount(TeamEditComponent, {
        componentProperties: {
          uuid: team.uuid,
        },
      });

      // when
      cy.wait([
        '@getBanksRequest',
        '@getTeamRequest',
        '@isUsernameUniqueRequest',
        '@isJmsQueueUniqueRequest',
        '@isPasswordUniqueRequest',
      ]);

      // then
      cy.get('[data-cy=message]').should('not.exist');
      cy.get('[data-cy=team-edit-headline]').should('be.visible');
      cy.get('[data-cy=team-form]').should('be.visible');
      cy.get('[data-cy=team-form-statistics]').should('not.exist');
    });
  });
});
