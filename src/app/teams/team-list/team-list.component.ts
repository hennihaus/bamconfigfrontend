import {
  Component,
  computed,
  inject,
  input,
  model,
  signal,
} from '@angular/core';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { Router, RouterLink } from '@angular/router';
import { PaginationService } from '../../shared/services/pagination.service';
import {
  HasPassed,
  TeamQuery,
  TeamQueryParams,
} from '../../shared/models/team-query';
import { TeamType } from '../../shared/models/team-type';
import { RandomAvatarPipe } from '../pipes/random-avator.pipe';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { TeamListFilterComponent } from '../team-list-filter/team-list-filter.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { TeamListItemComponent } from '../team-list-item/team-list-item.component';
import { CursorPaginationComponent } from '../../shared/components/cursor-pagination/cursor-pagination.component';
import { Message } from '../../shared/models/message';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-team-list',
  imports: [
    CursorPaginationComponent,
    TeamListItemComponent,
    RouterLink,
    MessageComponent,
    TeamListFilterComponent,
    LoadingComponent,
    RandomAvatarPipe,
  ],
  host: {
    '(window:resize)': 'calculateItemsPerWindow()',
  },
  templateUrl: './team-list.component.html',
  styleUrl: './team-list.component.css',
})
export class TeamListComponent {
  private readonly router = inject(Router);
  private readonly pagination = inject(PaginationService);
  private readonly teamStore = inject(TeamStoreService);

  readonly cursor = model<string>();
  readonly type = input<TeamType>();
  readonly username = input<string>();
  readonly password = input<string>();
  readonly jmsQueue = input<string>();
  readonly hasPassed = input<HasPassed>();
  readonly minRequests = input<string>();
  readonly maxRequests = input<string>();
  readonly studentFirstname = input<string>();
  readonly studentLastname = input<string>();
  readonly banks = input<string[]>();

  private readonly itemsPerWindow = signal(this.pagination.getItemsPerWindow());

  readonly teamQuery = computed<TeamQuery>(() => this.buildTeamQuery());

  readonly teamPaginationResource = rxResource({
    request: () => this.buildTeamQuery(),
    loader: ({ request: query }) => this.teamStore.getAll(query),
  });

  get notFoundMessage(): Message {
    return { text: $localize`Keine Teams gefunden`, type: 'negative' };
  }

  calculateItemsPerWindow() {
    const itemsPerWindow = this.pagination.getItemsPerWindow();

    if (itemsPerWindow !== this.itemsPerWindow()) {
      this.itemsPerWindow.set(itemsPerWindow);
      this.cursor.set(undefined);
    }
  }

  searchTeams(queryParams: TeamQueryParams) {
    this.router.navigate(['/teams', 'list'], { queryParams });
  }

  private buildTeamQuery(): TeamQuery {
    const limit = this.itemsPerWindow();
    const cursor = this.cursor();
    const type = this.type();
    const username = this.username();
    const password = this.password();
    const jmsQueue = this.jmsQueue();
    const hasPassed = this.hasPassed();
    const minRequests = this.minRequests();
    const maxRequests = this.maxRequests();
    const studentFirstname = this.studentFirstname();
    const studentLastname = this.studentLastname();
    const banks = this.banks();

    return {
      limit,
      ...(cursor && { cursor }),
      ...(username && { username }),
      ...(password && { password }),
      ...(jmsQueue && { jmsQueue }),
      ...(studentFirstname && { studentFirstname }),
      ...(studentLastname && { studentLastname }),
      ...(minRequests && { minRequests: +minRequests }),
      ...(maxRequests && { maxRequests: +maxRequests }),
      ...(type && { type }),
      ...(hasPassed && { hasPassed: this.buildHasPassed(hasPassed) }),
      ...(banks && banks.length && { banks }),
    };
  }

  private buildHasPassed(hasPassed?: HasPassed) {
    switch (hasPassed) {
      case 'PASSED':
        return true;
      case 'NOT_PASSED':
        return false;
      default:
        return undefined;
    }
  }
}
