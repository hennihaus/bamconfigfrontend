import { TeamListComponent } from './team-list.component';

describe('TeamListComponent', () => {
  it('shows error message and no pagination when no teams are found', () => {
    cy.fixture('teams').then((teams) => {
      cy.fixture('bank').then((bank) => {
        // given
        cy.intercept('GET', '/v1/teams?limit=3', {
          body: { ...teams, items: [] },
        }).as('getTeamsRequest');
        cy.intercept('GET', '/v1/banks', {
          body: [{ ...bank, isAsync: true }],
        }).as('getBanksRequest');
        cy.mount(TeamListComponent);

        // then
        cy.wait('@getTeamsRequest');
        cy.wait('@getBanksRequest');
        cy.get('[data-cy=team-list-item]').should('not.exist');
        cy.get('[data-cy=message]')
          .should('have.css', 'color')
          .and('eq', 'rgb(159, 58, 56)');
        cy.get('[data-cy=cursor-pagination-menu]').should('not.exist');
      });
    });
  });

  it('shows error message and no pagination when an internal server error occurs', () => {
    // given
    cy.intercept('GET', '/v1/teams?limit=3', {
      body: {},
      statusCode: 500,
    }).as('getTeamsRequest');
    cy.mount(TeamListComponent);

    // then
    cy.wait('@getTeamsRequest');
    cy.get('[data-cy=team-list-item]').should('not.exist');
    cy.get('[data-cy=team-list-filter]').should('not.exist');
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=cursor-pagination-menu]').should('not.exist');
  });
});
