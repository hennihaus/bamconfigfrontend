import { Component } from '@angular/core';
import { CoreComponent } from './core/core.component';

@Component({
  selector: 'bam-root',
  imports: [CoreComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent {}
