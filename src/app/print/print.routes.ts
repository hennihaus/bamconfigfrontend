import { Routes } from '@angular/router';
import { PrintComponent } from './print.component';
import { tasksResolver } from './resolvers/tasks.resolver';
import { TaskListPrintComponent } from '../tasks/task-list-print/task-list-print.component';

export const PRINT_ROUTES: Routes = [
  {
    path: '',
    component: PrintComponent,
    children: [
      {
        path: 'tasks',
        component: TaskListPrintComponent,
        title: 'Aufgaben drucken',
        resolve: {
          printData: tasksResolver,
        },
      },
    ],
  },
];
