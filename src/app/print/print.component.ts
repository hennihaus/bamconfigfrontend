import { afterRenderEffect, Component, inject } from '@angular/core';
import { PrintService } from '../shared/services/print.service';
import { Router, RouterOutlet } from '@angular/router';

@Component({
  selector: 'bam-print',
  imports: [RouterOutlet],
  host: {
    '(window:afterprint)': 'closePrinting()',
  },
  templateUrl: './print.component.html',
  styleUrl: './print.component.css',
})
export class PrintComponent {
  private readonly print = inject(PrintService);
  private readonly router = inject(Router);

  constructor() {
    afterRenderEffect({
      read: () => this.print.printWindow(),
    });
  }

  closePrinting() {
    this.print.stopPrinting();

    this.router.navigate([{ outlets: { print: null } }]);
  }
}
