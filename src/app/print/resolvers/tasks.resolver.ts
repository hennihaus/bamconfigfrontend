import { ResolveFn } from '@angular/router';
import { inject } from '@angular/core';
import { TaskStoreService } from '../../shared/services/task-store.service';
import { Task } from '../../shared/models/task';
import { catchError } from 'rxjs';
import { PrintService } from '../../shared/services/print.service';
import { ErrorHandlerService } from '../../shared/services/error-handler.service';
import { Message } from '../../shared/models/message';

export const tasksResolver: ResolveFn<Task[] | Message> = () => {
  const print = inject(PrintService);
  const taskStore = inject(TaskStoreService);
  const errorHandler = inject(ErrorHandlerService);

  print.startPrinting();

  return taskStore
    .getAll()
    .pipe(catchError(() => errorHandler.getMassiveServerErrorMessageAsync()));
};
