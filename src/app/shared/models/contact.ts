export interface Contact {
  uuid: string;
  firstname: string;
  lastname: string;
  email: string;
}
