export interface Response {
  uuid: string;
  httpStatusCode: number;
  contentType: string;
  description: string;
  example: string;
}
