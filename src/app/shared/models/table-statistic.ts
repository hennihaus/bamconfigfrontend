export interface TableStatistic {
  bank: string;
  requests: number;
  percentage: number;
  hasPassed: boolean;
}
