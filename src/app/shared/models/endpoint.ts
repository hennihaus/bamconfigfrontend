export interface Endpoint {
  uuid: string;
  type: string;
  url: string;
  docsUrl: string;
}
