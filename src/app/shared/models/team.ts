import { TeamType } from './team-type';
import { Student } from './student';

export type Statistics = Record<string, number>;

export interface Team {
  uuid: string;
  type: TeamType;
  username: string;
  password: string;
  jmsQueue: string;
  students: Student[];
  statistics: Statistics;
  hasPassed: boolean;
  createdAt: Date;
  updatedAt: Date;
}
