export interface Parameter {
  uuid: string;
  name: string;
  type: string;
  description: string;
  example: string;
}
