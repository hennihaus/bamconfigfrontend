import { Team } from './team';
import { FormArray, FormControl, FormGroup, FormRecord } from '@angular/forms';
import { Student } from './student';

export interface StatisticForm {
  bank: string;
  requests: number;
}

export type StudentForm = {
  [field in keyof Student]: FormControl<Student[field]>;
};

export type TeamForm = {
  [field in keyof Omit<
    Team,
    'students' | 'statistics' | 'updatedAt'
  >]: FormControl<Team[field]>;
} & {
  students: FormArray<FormGroup<StudentForm>>;
  statistics: FormRecord<FormControl<number>>;
};
