export interface Student {
  uuid: string;
  firstname: string;
  lastname: string;
}
