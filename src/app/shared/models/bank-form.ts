import { FormControl, FormGroup } from '@angular/forms';
import { Bank } from './bank';
import { CreditConfiguration } from './credit-configuration';

export type AmountInEurosForm = {
  [field in keyof Pick<
    CreditConfiguration,
    'minAmountInEuros' | 'maxAmountInEuros'
  >]: FormControl<CreditConfiguration[field]>;
};

export type TermInMonthsForm = {
  [field in keyof Pick<
    CreditConfiguration,
    'minTermInMonths' | 'maxTermInMonths'
  >]: FormControl<CreditConfiguration[field]>;
};

export type SchufaRatingForm = {
  [field in keyof Pick<
    CreditConfiguration,
    'minSchufaRating' | 'maxSchufaRating'
  >]: FormControl<CreditConfiguration[field]>;
};

export type BankForm = {
  [field in keyof Omit<Bank, 'creditConfiguration'>]: FormControl<Bank[field]>;
} & {
  hasCreditConfiguration: FormControl<boolean>;
  amountInEuros: FormGroup<AmountInEurosForm>;
  termInMonths: FormGroup<TermInMonthsForm>;
  schufaRating: FormGroup<SchufaRatingForm>;
};
