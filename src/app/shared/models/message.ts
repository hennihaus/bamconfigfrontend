export interface Message {
  text: string;
  description?: string;
  type: 'positive' | 'negative' | 'warning';
  size?: string;
}
