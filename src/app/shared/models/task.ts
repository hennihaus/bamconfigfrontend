import { Contact } from './contact';
import { Endpoint } from './endpoint';
import { Parameter } from './parameter';
import { Bank } from './bank';
import { Response } from './response';

export interface Task {
  uuid: string;
  title: string;
  description: string;
  integrationStep: number;
  isOpenApiVerbose: boolean;
  contact: Contact;
  endpoints: Endpoint[];
  parameters: Parameter[];
  responses: Response[];
  banks: Bank[];
  updatedAt: Date;
}
