import { Task } from './task';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Contact } from './contact';
import { Endpoint } from './endpoint';
import { Parameter } from './parameter';
import { Response } from './response';
import { CreditConfiguration } from './credit-configuration';
import { Bank } from './bank';

export type ContactForm = {
  [field in keyof Contact]: FormControl<Contact[field]>;
};

export type EndpointForm = {
  [field in keyof Endpoint]: FormControl<Endpoint[field]>;
};

export type ParameterForm = {
  [field in keyof Parameter]: FormControl<Parameter[field]>;
};

export type ResponseForm = {
  [field in keyof Response]: FormControl<Response[field]>;
};

export type CreditConfigurationForm = {
  [field in keyof CreditConfiguration]: FormControl<CreditConfiguration[field]>;
};

export type BankForm = {
  [field in keyof Omit<Bank, 'creditConfiguration'>]: FormControl<Bank[field]>;
} & {
  creditConfiguration?: FormGroup<CreditConfigurationForm>;
};

export type TaskForm = {
  [field in keyof Omit<
    Task,
    'contact' | 'endpoints' | 'parameters' | 'responses' | 'banks'
  >]: FormControl<Task[field]>;
} & {
  contact: FormGroup<ContactForm>;
  endpoints: FormArray<FormGroup<EndpointForm>>;
  parameters: FormArray<FormGroup<ParameterForm>>;
  responses: FormArray<FormGroup<ResponseForm>>;
  banks: FormArray<FormGroup<BankForm>>;
};
