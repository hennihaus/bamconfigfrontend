import { CreditConfiguration } from './credit-configuration';

export interface Bank {
  uuid: string;
  name: string;
  jmsQueue: string;
  thumbnailUrl: string;
  isAsync: boolean;
  isActive: boolean;
  creditConfiguration?: CreditConfiguration;
  updatedAt: Date;
}
