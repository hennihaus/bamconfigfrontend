import { RatingLevel } from './rating-level';

export interface CreditConfiguration {
  minAmountInEuros: number;
  maxAmountInEuros: number;
  minTermInMonths: number;
  maxTermInMonths: number;
  minSchufaRating: RatingLevel;
  maxSchufaRating: RatingLevel;
}
