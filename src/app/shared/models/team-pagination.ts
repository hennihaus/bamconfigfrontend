import { Pagination } from './pagination';
import { Team } from './team';

export interface TeamPagination {
  pagination: Pagination;
  items: Team[];
}
