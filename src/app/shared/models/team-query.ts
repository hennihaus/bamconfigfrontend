import { TeamType } from './team-type';

export interface TeamQuery {
  limit: number;
  cursor?: string;
  type?: TeamType;
  username?: string;
  password?: string;
  jmsQueue?: string;
  hasPassed?: boolean;
  minRequests?: number;
  maxRequests?: number;
  studentFirstname?: string;
  studentLastname?: string;
  banks?: string[];
}

export type HasPassed = 'PASSED' | 'NOT_PASSED';

export type TeamQueryForm = Pick<
  TeamQuery,
  | 'type'
  | 'username'
  | 'password'
  | 'jmsQueue'
  | 'studentFirstname'
  | 'studentLastname'
> & {
  hasPassed?: HasPassed;
  minRequests?: string;
  maxRequests?: string;
  banks: Set<string>;
};

export type TeamQueryParams = Omit<TeamQueryForm, 'banks'> & {
  banks?: string[];
};
