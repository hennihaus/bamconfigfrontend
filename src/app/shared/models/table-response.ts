import { Response } from './response';

export type TableResponse = Omit<Response, 'httpStatusCode'> & {
  httpStatusCode: string;
};
