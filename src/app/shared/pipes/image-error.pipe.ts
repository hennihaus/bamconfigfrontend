import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'imageError',
  pure: false,
})
export class ImageErrorPipe implements PipeTransform {
  transform(ngSrc: string | undefined, hasError: boolean): string {
    if (hasError) {
      return '/placeholder.png';
    }
    if (!ngSrc) {
      return '/placeholder.png';
    }
    return ngSrc;
  }
}
