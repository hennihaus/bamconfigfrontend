import { CanDeactivateFn } from '@angular/router';
import { WritableSignal } from '@angular/core';

export interface LeaveFormGuard {
  hasUnsavedChanges: WritableSignal<boolean>;
}

export const leaveFormGuard: CanDeactivateFn<LeaveFormGuard> = (
  component: LeaveFormGuard,
) => {
  if (!component.hasUnsavedChanges()) {
    return true;
  }
  return confirm(
    $localize`Wollen Sie das Formular wirklich verlassen? Ihre Änderungen gehen verloren.`,
  );
};
