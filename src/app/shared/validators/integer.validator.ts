import { Validators } from '@angular/forms';

export const integer = Validators.pattern(/^-?[0-9]+$/);
