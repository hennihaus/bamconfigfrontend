import { isFormControl, ValidatorFn } from '@angular/forms';

export const json: ValidatorFn = (control) => {
  if (!control.value || !isFormControl(control)) {
    return null;
  }
  try {
    JSON.parse(control.value);
    return null;
  } catch {
    return {
      json: true,
    };
  }
};
