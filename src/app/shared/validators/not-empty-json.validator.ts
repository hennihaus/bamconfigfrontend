import { isFormControl, ValidatorFn } from '@angular/forms';

export const notEmptyJson: ValidatorFn = (control) => {
  if (!control.value || !isFormControl(control)) {
    return null;
  }
  if (
    control.value.replace(/\s/g, '') === '{}' ||
    control.value.replace(/\s/g, '') === '[]'
  ) {
    return {
      notemptyjson: true,
    };
  }
  return null;
};
