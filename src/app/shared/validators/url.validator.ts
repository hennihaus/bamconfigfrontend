import {
  AbstractControl,
  isFormControl,
  ValidationErrors,
  ValidatorFn,
} from '@angular/forms';

export const urlValidator = (pattern?: string | RegExp): ValidatorFn => {
  return function (control: AbstractControl<string>): ValidationErrors | null {
    if (!control.value || !isFormControl(control)) {
      return null;
    }
    try {
      new URL(control.value);
    } catch {
      return {
        url: true,
      };
    }
    if (!pattern) {
      return null;
    }
    if (new RegExp(pattern).test(control.value)) {
      return null;
    }
    return {
      url: true,
    };
  };
};
