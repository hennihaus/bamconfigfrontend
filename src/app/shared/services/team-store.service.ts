import { inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TeamPagination } from '../models/team-pagination';
import { TeamQuery } from '../models/team-query';
import { Team } from '../models/team';
import { Unique } from '../models/unique';
import { DateTimeService } from './date-time.service';
import { BASE_API_URL, BASE_API_VERSION } from '../../../tokens';

@Injectable({
  providedIn: 'root',
})
export class TeamStoreService {
  private readonly baseApiUrl = inject(BASE_API_URL);
  private readonly baseApiVersion = inject(BASE_API_VERSION);
  private readonly dateTime = inject(DateTimeService);
  private readonly http = inject(HttpClient);

  getAll(query: TeamQuery): Observable<TeamPagination> {
    const params = new HttpParams({
      fromObject: {
        ...query,
      },
    });

    return this.http.get<TeamPagination>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams`,
      {
        params,
      },
    );
  }

  getSingle(uuid: string): Observable<Team> {
    return this.http.get<Team>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}`,
    );
  }

  update(team: Team): Observable<Team> {
    const createdAt = this.dateTime.convertUTCDateTime(team.createdAt);
    const updatedAt = this.dateTime.convertUTCDateTime(team.updatedAt);

    return this.http.put<Team>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${team.uuid}`,
      {
        ...team,
        createdAt,
        updatedAt,
      },
    );
  }

  delete(uuid: string): Observable<string> {
    return this.http.delete(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}`,
      {
        responseType: 'text',
      },
    );
  }

  resetStatistics(uuid: string): Observable<Team> {
    return this.http.delete<Team>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}/statistics`,
    );
  }

  isUsernameUnique(uuid: string, username: string): Observable<Unique> {
    return this.http.get<Unique>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}/unique/username/${username}`,
    );
  }

  isJmsQueueUnique(uuid: string, jmsQueue: string): Observable<Unique> {
    return this.http.get<Unique>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}/unique/jmsQueue/${jmsQueue}`,
    );
  }

  isPasswordUnique(uuid: string, password: string): Observable<Unique> {
    return this.http.get<Unique>(
      `${this.baseApiUrl}/${this.baseApiVersion}/teams/${uuid}/unique/password/${password}`,
    );
  }
}
