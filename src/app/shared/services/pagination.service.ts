import { inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class PaginationService {
  private readonly document = inject(DOCUMENT);

  private readonly PAGE_BORDER_HEIGHT_IN_PIXEL = 30;
  private readonly HEADER_HEIGHT_IN_PIXEL = 60;
  private readonly PAGINATION_NUMBERS_HEIGHT_IN_PIXEL = 40;
  private readonly MAIN_CONTENT_BORDER_HEIGHT_IN_PIXEL = 10;
  private readonly ITEM_HEIGHT_IN_PIXEL = 95;
  private readonly DEFAULT_ITEMS_PER_WINDOW = 10;

  getItemsPerWindow() {
    const innerWindowHeight = this.document.defaultView?.innerHeight;

    if (!innerWindowHeight) {
      return this.DEFAULT_ITEMS_PER_WINDOW;
    }
    return Math.floor(
      (innerWindowHeight -
        (this.PAGE_BORDER_HEIGHT_IN_PIXEL +
          this.HEADER_HEIGHT_IN_PIXEL +
          this.MAIN_CONTENT_BORDER_HEIGHT_IN_PIXEL +
          this.PAGINATION_NUMBERS_HEIGHT_IN_PIXEL)) /
        this.ITEM_HEIGHT_IN_PIXEL,
    );
  }
}
