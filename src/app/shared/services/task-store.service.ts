import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models/task';
import { DateTimeService } from './date-time.service';
import { Unique } from '../models/unique';
import { BASE_API_URL, BASE_API_VERSION } from '../../../tokens';

@Injectable({
  providedIn: 'root',
})
export class TaskStoreService {
  private readonly baseApiUrl = inject(BASE_API_URL);
  private readonly baseApiVersion = inject(BASE_API_VERSION);
  private readonly dateTime = inject(DateTimeService);
  private readonly http = inject(HttpClient);

  getAll(): Observable<Task[]> {
    return this.http.get<Task[]>(
      `${this.baseApiUrl}/${this.baseApiVersion}/tasks`,
    );
  }

  getSingle(uuid: string): Observable<Task> {
    return this.http.get<Task>(
      `${this.baseApiUrl}/${this.baseApiVersion}/tasks/${uuid}`,
    );
  }

  update(task: Task): Observable<Task> {
    const updatedAt = this.dateTime.convertUTCDateTime(task.updatedAt);

    return this.http.patch<Task>(
      `${this.baseApiUrl}/${this.baseApiVersion}/tasks/${task.uuid}`,
      { ...task, updatedAt },
    );
  }

  isTitleUnique(uuid: string, title: string): Observable<Unique> {
    return this.http.get<Unique>(
      `${this.baseApiUrl}/${this.baseApiVersion}/tasks/${uuid}/unique/title/${title}`,
    );
  }
}
