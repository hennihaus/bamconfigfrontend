import { Injectable, signal } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PrintService {
  readonly isPrinting = signal(false);

  startPrinting() {
    this.isPrinting.set(true);
  }

  stopPrinting() {
    this.isPrinting.set(false);
  }

  printWindow() {
    setTimeout(() => window.print());
  }
}
