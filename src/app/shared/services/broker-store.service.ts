import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BASE_API_URL, BASE_API_VERSION } from '../../../tokens';

@Injectable({
  providedIn: 'root',
})
export class BrokerStoreService {
  private readonly baseApiUrl = inject(BASE_API_URL);
  private readonly baseApiVersion = inject(BASE_API_VERSION);
  private readonly http = inject(HttpClient);

  resetBroker(): Observable<string> {
    return this.http.delete(
      `${this.baseApiUrl}/${this.baseApiVersion}/activemq`,
      {
        responseType: 'text',
      },
    );
  }

  deleteQueue(jmsQueue: string): Observable<string> {
    return this.http.delete(
      `${this.baseApiUrl}/${this.baseApiVersion}/activemq/${jmsQueue}`,
      { responseType: 'text' },
    );
  }
}
