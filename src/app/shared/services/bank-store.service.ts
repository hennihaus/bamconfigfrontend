import { inject, Injectable } from '@angular/core';
import { Bank } from '../models/bank';
import { DateTimeService } from './date-time.service';
import { BASE_API_URL, BASE_API_VERSION } from '../../../tokens';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BankStoreService {
  private readonly baseApiUrl = inject(BASE_API_URL);
  private readonly baseApiVersion = inject(BASE_API_VERSION);
  private readonly dateTime = inject(DateTimeService);
  private readonly http = inject(HttpClient);

  getAll(): Observable<Bank[]> {
    return this.http.get<Bank[]>(
      `${this.baseApiUrl}/${this.baseApiVersion}/banks`,
    );
  }

  getSingle(uuid: string): Observable<Bank> {
    return this.http.get<Bank>(
      `${this.baseApiUrl}/${this.baseApiVersion}/banks/${uuid}`,
    );
  }

  update(bank: Bank): Observable<Bank> {
    const updatedAt = this.dateTime.convertUTCDateTime(bank.updatedAt);

    return this.http.patch<Bank>(
      `${this.baseApiUrl}/${this.baseApiVersion}/banks/${bank.uuid}`,
      {
        ...bank,
        updatedAt,
      },
    );
  }
}
