import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService {
  public readonly ERROR_MESSAGE = $localize`Interner Server Fehler`;

  getServerErrorMessage(): Message {
    return {
      text: this.ERROR_MESSAGE,
      type: 'negative',
    };
  }

  getServerErrorMessageAsync(): Observable<Message> {
    return of({
      text: this.ERROR_MESSAGE,
      type: 'negative',
    });
  }

  getMassiveServerErrorMessageAsync(): Observable<Message> {
    return of({
      text: this.ERROR_MESSAGE,
      type: 'negative',
      size: 'massive',
    });
  }
}
