import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BASE_API_URL, BASE_API_VERSION } from '../../../tokens';

@Injectable({
  providedIn: 'root',
})
export class StatisticStoreService {
  private readonly baseApiUrl = inject(BASE_API_URL);
  private readonly baseApiVersion = inject(BASE_API_VERSION);
  private readonly http = inject(HttpClient);

  recreateAll(limit: number): Observable<string> {
    return this.http.post(
      `${this.baseApiUrl}/${this.baseApiVersion}/statistics/${limit}`,
      {},
      { responseType: 'text' },
    );
  }

  saveStatistics(bankId: string): Observable<string> {
    return this.http.put(
      `${this.baseApiUrl}/${this.baseApiVersion}/statistics/${bankId}`,
      {},
      { responseType: 'text' },
    );
  }

  deleteStatistics(bankId: string): Observable<string> {
    return this.http.delete(
      `${this.baseApiUrl}/${this.baseApiVersion}/statistics/${bankId}`,
      {
        responseType: 'text',
      },
    );
  }
}
