import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateTimeService {
  convertUTCDateTime(date: Date) {
    date.setMilliseconds(0);
    return date.toISOString().split('.000Z')[0];
  }
}
