import { Component, computed, input } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'bam-form-error-message',
  imports: [],
  templateUrl: './form-error-message.component.html',
  styleUrl: './form-error-message.component.css',
})
export class FormErrorMessageComponent {
  readonly errors = input.required<ValidationErrors | null>();
  readonly fieldName = input<string>();

  private readonly messages: Record<string, string> = {
    required: $localize`:{field} is a placeholder and should stay in every language:{field} ist ein Pflichtfeld`,
    url: $localize`:{field} is a placeholder and should stay in every language:{field} ist keine gültige URL`,
    pattern: $localize`:{field} is a placeholder and should stay in every language:Das Format von {field} ist ungültig`,
    min: $localize`:{field} and {min} are placeholders and should stay in every language:{field} muss mindestens {min} sein`,
    max: $localize`:{field} and {max} are placeholders and should stay in every language:{field} darf maximal {max} sein`,
    minlength: $localize`:{field} and {length} are placeholders and should stay in every language:{field} muss mindestens {length} Zeichen lang sein`,
    maxlength: $localize`:{field} and {length} are placeholders and should stay in every language:{field} darf nicht länger als {length} Zeichen sein`,
    unique: $localize`:{field} is a placeholder and should stay in every language:{field} existiert bereits`,
    email: $localize`:{field} is a placeholder and should stay in every language:{field} muss eine gültige E-Mail-Adresse sein`,
    amountineuros: $localize`Kreditminimum darf nicht größer als Kreditmaximum`,
    terminmonths: $localize`Mindestlaufzeit darf nicht größer als Maximallaufzeit`,
    schufarating: $localize`:Min. and Max. is an abbreviation for minimum and maximum:Min. Schufa Rating darf nicht größer als Max. Schufa Rating`,
    json: $localize`{field} muss ein gültiges JSON sein`,
    notemptyjson: $localize`{field} darf nicht leeres JSON sein`,
  };

  readonly errorMessages = computed(() => {
    const errors = this.errors();
    const fieldName = this.fieldName();

    if (!errors) {
      return [];
    }
    return Object.keys(errors)
      .map((errorCode) => this.messages[errorCode])
      .map((message) => {
        if (fieldName) {
          return message.replaceAll('{field}', fieldName);
        }
        return message;
      })
      .map((message) => {
        if (message.includes('{min}')) {
          return message.replaceAll('{min}', errors['min']?.min);
        }
        return message;
      })
      .map((message) => {
        if (message.includes('{max}')) {
          return message.replaceAll('{max}', errors['max']?.max);
        }
        return message;
      })
      .map((message) => {
        if (message.includes('{length}') && errors['minlength']) {
          return message.replaceAll(
            '{length}',
            errors['minlength']?.['requiredLength'],
          );
        }
        return message;
      })
      .map((message) => {
        if (message.includes('{length}') && errors['maxlength']) {
          return message.replaceAll(
            '{length}',
            errors['maxlength']?.['requiredLength'],
          );
        }
        return message;
      });
  });
}
