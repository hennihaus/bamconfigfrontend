import {
  Component,
  computed,
  inject,
  input,
  model,
  signal,
  TemplateRef,
} from '@angular/core';
import { PaginationService } from '../../services/pagination.service';
import { NgTemplateOutlet, SlicePipe } from '@angular/common';

@Component({
  selector: 'bam-frontend-pagination',
  imports: [NgTemplateOutlet, SlicePipe],
  host: {
    '(window:resize)': 'calculateItemsPerWindow()',
  },
  templateUrl: './frontend-pagination.component.html',
  styleUrl: './frontend-pagination.component.css',
})
export class FrontendPaginationComponent<Item extends { uuid: string }> {
  private readonly pagination = inject(PaginationService);

  readonly items = input.required<Item[]>();
  readonly pageNumber = model.required<number>();
  readonly listItemTemplate = input.required<TemplateRef<unknown>>();
  readonly messageTemplate = input.required<TemplateRef<unknown>>();

  private readonly itemsPerWindow = signal(this.pagination.getItemsPerWindow());

  readonly start = computed(() => this.index());

  readonly end = computed(() => this.index() + this.itemsPerWindow());

  readonly hasPages = computed(() => this.totalPages() > 1);

  readonly pageNumbers = computed(() => {
    const FIRST_PAGE_NUMBER = 1;
    const PAGE_STEPPER_RANGE = 2;

    return [...Array(this.totalPages()).keys()]
      .map((number) => number + 1)
      .filter((number) => {
        if (number === FIRST_PAGE_NUMBER) {
          return true;
        }
        if (number === this.totalPages()) {
          return true;
        }
        return Math.abs(number - this.currentPage()) < PAGE_STEPPER_RANGE;
      });
  });

  readonly currentPage = computed(() => {
    const FIRST_PAGE_NUMBER = 1;

    if (this.pageNumber() < FIRST_PAGE_NUMBER) {
      return FIRST_PAGE_NUMBER;
    }
    if (this.pageNumber() > this.totalPages()) {
      return this.totalPages();
    }
    return this.pageNumber();
  });

  private readonly index = computed(
    () => this.currentPage() * this.itemsPerWindow() - this.itemsPerWindow(),
  );

  private readonly totalPages = computed(() =>
    Math.ceil(this.items().length / this.itemsPerWindow()),
  );

  calculateItemsPerWindow() {
    const itemsPerWindow = this.pagination.getItemsPerWindow();

    this.itemsPerWindow.set(itemsPerWindow);
  }

  onPageClick(number: number, event?: KeyboardEvent) {
    if (event && event.key !== 'Enter') {
      return;
    }
    this.pageNumber.set(number);
  }
}
