import { Component, computed, input } from '@angular/core';
import {
  CdkStepper,
  CdkStepperNext,
  CdkStepperPrevious,
} from '@angular/cdk/stepper';
import { NgTemplateOutlet } from '@angular/common';

@Component({
  selector: 'bam-form-stepper',
  imports: [NgTemplateOutlet, CdkStepperPrevious, CdkStepperNext],
  providers: [{ provide: CdkStepper, useExisting: FormStepperComponent }],
  templateUrl: './form-stepper.component.html',
  styleUrl: './form-stepper.component.css',
})
export class FormStepperComponent extends CdkStepper {
  readonly titles = input.required<string[]>();
  readonly isSubmitting = input.required<boolean>();

  readonly title = computed(() => this.titles()[this.selectedIndex]);
}
