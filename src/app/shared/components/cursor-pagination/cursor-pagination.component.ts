import { Component, input } from '@angular/core';
import { Pagination } from '../../models/pagination';
import { RouterLink } from '@angular/router';

@Component({
  selector: 'bam-cursor-pagination',
  imports: [RouterLink],
  templateUrl: './cursor-pagination.component.html',
  styleUrl: './cursor-pagination.component.css',
})
export class CursorPaginationComponent {
  readonly pagination = input.required<Pagination>();
  readonly baseRouterLink = input.required<string>();
  readonly showFilter = input.required<boolean>();
}
