import { Component } from '@angular/core';

@Component({
  selector: 'bam-loading',
  templateUrl: './loading.component.html',
  styleUrl: './loading.component.css',
})
export class LoadingComponent {}
