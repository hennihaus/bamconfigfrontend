import { Component, inject, input } from '@angular/core';
import { Message } from '../../models/message';
import { ErrorHandlerService } from '../../services/error-handler.service';

@Component({
  selector: 'bam-message',
  templateUrl: './message.component.html',
  styleUrl: './message.component.css',
})
export class MessageComponent {
  private readonly errorHandler = inject(ErrorHandlerService);

  readonly message = input<Message>(this.errorHandler.getServerErrorMessage());
}
