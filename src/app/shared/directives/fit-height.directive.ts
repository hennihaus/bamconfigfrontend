import {
  afterRenderEffect,
  Directive,
  ElementRef,
  inject,
  signal,
} from '@angular/core';

@Directive({
  selector: '[bamFitHeight]',
  host: {
    '[style.height]': 'height()',
    '[style.max-height]': 'height()',
  },
})
export class FitHeightDirective {
  private readonly el = inject<ElementRef<HTMLElement>>(
    ElementRef<HTMLElement>,
  );

  readonly height = signal<string | null>(null);

  constructor() {
    afterRenderEffect({
      earlyRead: () => this.el.nativeElement.scrollHeight,
      write: (scrollHeight) => {
        const paddingTop = 5; // random number
        const paddingBottom = 5; // random number

        this.height.set(`${scrollHeight() + paddingTop + paddingBottom}px`);
      },
    });
  }
}
