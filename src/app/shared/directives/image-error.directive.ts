import { Directive, signal } from '@angular/core';

@Directive({
  selector: '[bamImageError]',
})
export class ImageErrorDirective {
  readonly hasImageLoadingError = signal(false);

  showFallbackImage() {
    this.hasImageLoadingError.set(true);
  }
}
