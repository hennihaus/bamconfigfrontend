import { TaskDetailsComponent } from './task-details.component';

describe('TaskDetailsComponent', () => {
  it('shows task details on init', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, {
        body: task,
        delay: 250, // milliseconds
      }).as('getTaskRequest');
      cy.mount(TaskDetailsComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });

      // when
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@getTaskRequest');

      // then
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=task-details-headline]').should('be.visible');
      cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
      cy.get('[data-cy=task-details-responses-table]').should('be.visible');
    });
  });

  it('shows task details without tables on init', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, {
        body: {
          ...task,
          parameters: [],
          responses: [],
        },
      }).as('getTaskRequest');
      cy.mount(TaskDetailsComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });

      // when
      cy.wait('@getTaskRequest');

      // then
      cy.get('[data-cy=task-details-headline]').should('be.visible');
      cy.get('[data-cy=task-details-parameters-table]').should('not.exist');
      cy.get('[data-cy=task-details-responses-table]').should('not.exist');
    });
  });

  it('shows error message with internal server error on init', () => {
    cy.fixture('task').then((task) => {
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, {
        body: {},
        statusCode: 500,
      }).as('getTaskRequest');
      cy.mount(TaskDetailsComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });

      // when
      cy.wait('@getTaskRequest');

      // then
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
      cy.get('[data-cy=task-details-headline]').should('not.exist');
      cy.get('[data-cy=task-details-parameters-table]').should('not.exist');
      cy.get('[data-cy=task-details-responses-table]').should('not.exist');
    });
  });
});
