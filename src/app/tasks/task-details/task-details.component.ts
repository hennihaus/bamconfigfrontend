import { Component, inject, input } from '@angular/core';
import { TaskStoreService } from '../../shared/services/task-store.service';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { TaskDetailsTableComponent } from '../task-details-table/task-details-table.component';
import { TaskDetailsViewComponent } from '../task-details-view/task-details-view.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-task-details',
  imports: [
    TaskDetailsViewComponent,
    TaskDetailsTableComponent,
    MessageComponent,
    LoadingComponent,
  ],
  templateUrl: './task-details.component.html',
  styleUrl: './task-details.component.css',
})
export class TaskDetailsComponent {
  private readonly taskStore = inject(TaskStoreService);

  readonly uuid = input.required<string>();

  readonly taskResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.taskStore.getSingle(uuid),
  });
}
