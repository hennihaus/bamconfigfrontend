import { Component, inject } from '@angular/core';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import { FitHeightDirective } from '../../shared/directives/fit-height.directive';

@Component({
  selector: 'bam-task-form-description-placeholder',
  imports: [ReactiveFormsModule, FormErrorMessageComponent, FitHeightDirective],
  templateUrl: './task-form-description-placeholder.component.html',
  styleUrl: './task-form-description-placeholder.component.css',
})
export class TaskFormDescriptionPlaceholderComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);

  get form() {
    return this.taskFormStore.form;
  }
}
