import { TaskFormDescriptionPlaceholderComponent } from './task-form-description-placeholder.component';
import { TaskFormStoreService } from '../services/task-form-store.service';

describe('TaskFormDescriptionPlaceholderComponent', () => {
  it('shows prefilled and valid form', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-description-placeholder /><form>',
        {
          imports: [TaskFormDescriptionPlaceholderComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );

      // then
      cy.get('[data-cy=task-form-description-placeholder-textarea]').should(
        'have.value',
        task.description,
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows errors for description', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-description-placeholder /><form>',
        {
          imports: [TaskFormDescriptionPlaceholderComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-description-placeholder-textarea]').should(
        'have.value',
        task.description,
      );

      // when field is cleared
      cy.get('[data-cy=task-form-description-placeholder-textarea]').clear();
      // then error is shown
      cy.get('[data-cy=task-form-description-placeholder-field]').contains(
        'Pflichtfeld',
      );
      // when too long field is entered
      cy.get('[data-cy=task-form-description-placeholder-textarea]').type(
        'T'.repeat(2_001),
      );
      // then error is shown
      cy.get('[data-cy=task-form-description-placeholder-field]').contains(
        'nicht länger',
      );
    });
  });
});
