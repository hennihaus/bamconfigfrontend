import { TaskFormParametersComponent } from './task-form-parameters.component';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { TeamType } from '../../shared/models/team-type';

describe('TaskFormParametersComponent', () => {
  it('shows prefilled and valid form for first integration step', () => {
    cy.fixture('tasks').then(([task]) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-parameters-description-label]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy^=task-form-parameters][data-cy$=-example-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-label]').each(
        (el, index) => {
          cy.wrap(el).contains(
            `${task.parameters[index].name} (${task.parameters[index].type.toLowerCase()})`,
          );
        },
      );
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.parameters[index].description);
        },
      );
      cy.get('[data-cy=task-form-parameters-example-input]').should(
        'have.value',
        task.parameters[0].example,
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).should('have.value', task.parameters[1].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).should('have.value', task.parameters[2].example);
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'have.value',
        task.parameters[3].example,
      );
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-username-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'have.value',
        task.parameters[4].example,
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-password-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows prefilled and valid form for second integration step', () => {
    cy.fixture('tasks').then(([_, task]) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-parameters-description-label]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy^=task-form-parameters][data-cy$=-example-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-label]').each(
        (el, index) => {
          cy.wrap(el).contains(
            `${task.parameters[index].name} (${task.parameters[index].type.toLowerCase()})`,
          );
        },
      );
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.parameters[index].description);
        },
      );
      cy.get(
        '[data-cy=task-form-parameters-amount-in-euros-example-input]',
      ).should('have.value', task.parameters[0].example);
      cy.get(
        '[data-cy=task-form-parameters-amount-in-euros-example-input]',
      ).should('be.disabled');
      cy.get(
        '[data-cy=task-form-parameters-term-in-months-example-input]',
      ).should('have.value', task.parameters[1].example);
      cy.get(
        '[data-cy=task-form-parameters-term-in-months-example-input]',
      ).should('be.disabled');
      cy.get('[data-cy=task-form-parameters-bank-edit-button]').should(
        'have.length',
        2,
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).should('have.value', task.parameters[2].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).should('have.value', task.parameters[3].example);
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'have.value',
        task.parameters[4].example,
      );
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-username-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'have.value',
        task.parameters[5].example,
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-password-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows prefilled and valid form for third integration step', () => {
    cy.fixture('tasks').then(([_1, _2, task]) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-parameters-description-label]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy^=task-form-parameters][data-cy$=-example-input]').should(
        'have.length',
        task.parameters.length,
      );
      cy.get('[data-cy=task-form-parameters-description-label]').each(
        (el, index) => {
          cy.wrap(el).contains(
            `${task.parameters[index].name} (${task.parameters[index].type.toLowerCase()})`,
          );
        },
      );
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.parameters[index].description);
        },
      );
      cy.get('[data-cy=task-form-parameters-example-input]').should(
        'have.value',
        task.parameters[0].example,
      );
      cy.get(
        '[data-cy=task-form-parameters-amount-in-euros-example-input]',
      ).should('have.value', task.parameters[1].example);
      cy.get(
        '[data-cy=task-form-parameters-amount-in-euros-example-input]',
      ).should('be.disabled');
      cy.get(
        '[data-cy=task-form-parameters-term-in-months-example-input]',
      ).should('have.value', task.parameters[2].example);
      cy.get(
        '[data-cy=task-form-parameters-term-in-months-example-input]',
      ).should('be.disabled');
      cy.get('[data-cy=task-form-parameters-banks-edit-button]').should(
        'have.length',
        2,
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).should('have.value', task.parameters[3].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).should('have.value', task.parameters[4].example);
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'have.value',
        task.parameters[5].example,
      );
      cy.get('[data-cy=task-form-parameters-username-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-username-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'have.value',
        task.parameters[6].example,
      );
      cy.get('[data-cy=task-form-parameters-password-example-input]').should(
        'be.disabled',
      );
      cy.get('[data-cy=task-form-parameters-password-edit-button]').should(
        'be.visible',
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows errors for description fields', () => {
    // given
    cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
      'isTitleUniqueRequest',
    );
    cy.fixture('task').then((task) => {
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });
    });
    cy.wait('@isTitleUniqueRequest');

    cy.get('[data-cy=task-form-parameters-description-input]').each(
      (el, index) => {
        // when too long field is entered
        cy.wrap(el).clear();
        cy.wrap(el).type('T'.repeat(101));
        // then error is shown
        cy.get('[data-cy=task-form-parameters-description-field]')
          .eq(index)
          .contains('nicht länger');
      },
    );
  });

  it('shows errors for delayInMilliseconds', () => {
    // given
    cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
      'isTitleUniqueRequest',
    );
    cy.fixture('task').then((task) => {
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).should('have.value', task.parameters[3].example);
    });
    cy.wait('@isTitleUniqueRequest');

    // when field is cleared
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).clear();
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(3)
      .contains('Pflichtfeld');
    // when invalid number is entered
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).type('1.2');
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(3)
      .contains('Format')
      .contains('ungültig');
    // when too small number is entered
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).clear();
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).type('-1');
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(3)
      .contains('mindestens');
    // when too high number is entered
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).clear();
    cy.get(
      '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
    ).type(`${Number.MAX_SAFE_INTEGER + 1}`);
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(3)
      .contains('maximal');
  });

  it('shows errors for defaults', () => {
    // given
    cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
      'isTitleUniqueRequest',
    );
    cy.fixture('tasks').then(([task]) => {
      cy.fixture('team').then((team) => {
        cy.mount(
          '<form class="ui equal width form"><bam-task-form-parameters [exampleTeam]="exampleTeam" /></form>',
          {
            imports: [TaskFormParametersComponent],
            componentProperties: {
              exampleTeam: {
                ...team,
                type: TeamType.EXAMPLE,
              },
            },
            providers: [
              {
                provide: TaskFormStoreService,
                useFactory: () => {
                  const taskFormStore = new TaskFormStoreService();

                  taskFormStore.setInitialTaskFormValues(task);
                  taskFormStore.setTaskFormValues(task);

                  return taskFormStore;
                },
              },
            ],
          },
        );
      });
      cy.get('[data-cy=task-form-parameters-example-input]').should(
        'have.value',
        task.parameters[0].example,
      );
    });
    cy.wait('@isTitleUniqueRequest');

    // when field is cleared
    cy.get('[data-cy=task-form-parameters-example-input]').clear();
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(0)
      .contains('Pflichtfeld');
    // when too long field is entered
    cy.get('[data-cy=task-form-parameters-example-input]').type('T'.repeat(51));
    // then error is shown
    cy.get('[data-cy=task-form-parameters-example-field]')
      .eq(0)
      .contains('nicht länger');
  });
});
