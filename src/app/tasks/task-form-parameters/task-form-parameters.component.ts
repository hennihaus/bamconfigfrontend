import { Component, inject, input } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { RatingLevel } from '../../shared/models/rating-level';
import { Team } from '../../shared/models/team';
import { RouterLink } from '@angular/router';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import { KeyValuePipe, LowerCasePipe } from '@angular/common';

@Component({
  selector: 'bam-task-form-parameters',
  imports: [
    ReactiveFormsModule,
    FormErrorMessageComponent,
    RouterLink,
    LowerCasePipe,
    KeyValuePipe,
  ],
  templateUrl: './task-form-parameters.component.html',
  styleUrl: './task-form-parameters.component.css',
})
export class TaskFormParametersComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);

  readonly exampleTeam = input<Team>();

  get bankId() {
    return (
      this.taskFormStore.form.controls.banks
        .getRawValue()
        .find(
          (bank) => bank.isActive && !bank.isAsync && bank.creditConfiguration,
        )?.uuid ?? ''
    );
  }

  get form() {
    return this.taskFormStore.form;
  }

  get parameters() {
    return this.taskFormStore.form.controls.parameters;
  }

  protected readonly RatingLevel = RatingLevel;
}
