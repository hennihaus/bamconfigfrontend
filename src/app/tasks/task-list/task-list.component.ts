import { Component, inject, input } from '@angular/core';
import { Message } from '../../shared/models/message';
import { TaskStoreService } from '../../shared/services/task-store.service';
import { Router, RouterLink } from '@angular/router';
import { AsyncBanksPipe } from '../pipes/async-banks.pipe';
import { HasNoAsyncBanksActivatedPipe } from '../pipes/has-no-async-banks-activated.pipe';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { TaskListItemComponent } from '../task-list-item/task-list-item.component';
import { FrontendPaginationComponent } from '../../shared/components/frontend-pagination/frontend-pagination.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-task-list',
  imports: [
    FrontendPaginationComponent,
    TaskListItemComponent,
    RouterLink,
    MessageComponent,
    LoadingComponent,
    HasNoAsyncBanksActivatedPipe,
    AsyncBanksPipe,
  ],
  templateUrl: './task-list.component.html',
  styleUrl: './task-list.component.css',
})
export class TaskListComponent {
  private readonly taskStore = inject(TaskStoreService);
  private readonly router = inject(Router);

  readonly pageNumber = input(1, {
    transform: (value?: string) => (value ? +value : 1),
  });

  readonly tasksResource = rxResource({
    loader: () => this.taskStore.getAll(),
  });

  navigateToPageNumber(pageNumber: number) {
    this.router.navigate(['/tasks/list', pageNumber]);
  }

  get notFoundMessage(): Message {
    return { text: $localize`Keine Aufgaben gefunden`, type: 'negative' };
  }

  get deactivatedMessage(): Message {
    return {
      text: $localize`Alle asynchrone Banken sind deaktiviert`,
      description: $localize`3. Integrationsschritt ist nicht mehr umsetzbar und wird nicht in der Aufgabenstellung exportiert.`,
      type: 'warning',
    };
  }
}
