import { TaskListComponent } from './task-list.component';

describe('TaskListComponent', () => {
  it('shows six tasks and pagination with two numbers when tasks are provided', () => {
    cy.fixture('tasks').then((tasks) => {
      // given
      cy.intercept('GET', '/v1/tasks', (req) =>
        req.reply({
          body: [...tasks, ...tasks],
          delay: 45, // milliseconds
        }),
      ).as('getTasksRequest');
      cy.mount(TaskListComponent);

      // when
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@getTasksRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');

      // then
      cy.get('[data-cy=task-list-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 2);
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(0)
        .should('have.class', 'active');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(1)
        .should('not.have.class', 'active');
    });
  });

  it('shows tasks and a warning message when third integration step has no active banks', () => {
    // given
    cy.fixture('tasks').then((tasks) => {
      cy.fixture('bank').then((bank) => {
        cy.intercept('GET', '/v1/tasks', {
          body: [
            tasks[0],
            tasks[1],
            {
              ...tasks[2],
              banks: [
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
              ],
            },
          ],
        }).as('getTasksRequest');
        cy.mount(TaskListComponent);
      });
    });

    // when
    cy.wait('@getTasksRequest');

    // then
    cy.get('[data-cy=task-list-item]').should('have.length', 3);
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(87, 58, 8)');
  });

  it('shows an error message and no pagination when no tasks are provided', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { body: [] }).as('getTasksRequest');
    cy.mount(TaskListComponent);

    // when
    cy.wait('@getTasksRequest');

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=frontend-pagination-menu]').should('not.exist');
  });

  it('shows an error message and no pagination when an internal server error occurs', () => {
    // given
    cy.intercept('GET', '/v1/tasks', { body: {}, statusCode: 500 }).as(
      'getTasksRequest',
    );
    cy.mount(TaskListComponent);

    // when
    cy.wait('@getTasksRequest');

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=frontend-pagination-menu]').should('not.exist');
  });
});
