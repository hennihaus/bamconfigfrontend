import { TaskEditComponent } from './task-edit.component';

describe('TaskEditComponent', () => {
  it('updates task when task form is submitted', () => {
    cy.fixture('task').then((task) => {
      // given
      const now = new Date(2024, 9, 21, 7, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, {
        body: task,
        delay: 250, // milliseconds
      }).as('getTaskRequest');
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.intercept('PATCH', `/v1/tasks/${task.uuid}`, {
        body: task,
        delay: 250, // milliseconds
      }).as('updateTaskRequest');
      cy.mount(TaskEditComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });

      // when component is loaded
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);
      // then it shows task edit form
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=task-form]').should('be.visible');

      // when task form is submitted
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=form-stepper-submit-button]').click();
      // then it updates task
      cy.get('[data-cy=form-stepper-previous-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-submit-button]').should('be.disabled');
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@updateTaskRequest')
        .its('request.body')
        .should('deep.equal', {
          ...task,
          title: 'New task title',
          updatedAt: '2024-10-21T05:00:00',
        });
      cy.get('[data-cy=loading-overlay]').should('not.exist');
    });
  });

  it('shows error message and task form when submit responds with internal server error', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.intercept('PATCH', `/v1/tasks/${task.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateTaskRequest');
      cy.mount(TaskEditComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });
      cy.wait([
        '@getTaskRequest',
        '@getExampleTeamRequest',
        '@isTitleUniqueRequest',
      ]);

      // when form is changed in every step and submitted
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type('New task title');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .clear();
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .type('New parameter description');
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').first().clear();
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .type('New response description');
      cy.get('[data-cy=form-stepper-submit-button]').click();
      // then shows error message and unsubmitted values on last step
      cy.wait('@updateTaskRequest');
      cy.get('[data-cy=task-form-responses-description-input]')
        .first()
        .should('have.value', 'New response description');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
      // when second step is visible
      cy.get('[data-cy=form-stepper-previous-button]').click();
      // then it shows error message and unsubmitted values on second step
      cy.get('[data-cy=task-form-parameters-description-input]')
        .first()
        .should('have.value', 'New parameter description');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
      // when first step is visible
      cy.get('[data-cy=form-stepper-previous-button]').click();
      // then it shows error message and unsubmitted values on first step
      cy.get('[data-cy=task-form-title-input]').should(
        'have.value',
        'New task title',
      );
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows error message with internal server error on load with task', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('getTaskRequest');
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        fixture: 'teams',
      }).as('getExampleTeamRequest');
      cy.mount(TaskEditComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });
    });

    // when
    cy.wait(['@getTaskRequest', '@getExampleTeamRequest']);

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=task-form]').should('not.exist');
  });

  it('shows form with no example team buttons with internal server error on load with example team', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', `/v1/tasks/${task.uuid}`, { body: task }).as(
        'getTaskRequest',
      );
      cy.intercept('GET', '/v1/teams?limit=1&type=EXAMPLE', {
        statusCode: 500,
        body: {},
      }).as('getExampleTeamRequest');
      cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.mount(TaskEditComponent, {
        componentProperties: {
          uuid: task.uuid,
        },
      });
    });

    // when first step is visible
    cy.wait([
      '@getTaskRequest',
      '@getExampleTeamRequest',
      '@isTitleUniqueRequest',
    ]);
    // then it shows no error message and task form
    cy.get('[data-cy=message]').should('not.exist');
    cy.get('[data-cy=task-form]').should('be.visible');
    // when second step is visible
    cy.get('[data-cy=form-stepper-next-button]').click();
    // then it shows no error message and no example team buttons
    cy.get('[data-cy=message]').should('not.exist');
    cy.get('[data-cy=task-form-parameters-username-edit-button]').should(
      'not.exist',
    );
    cy.get('[data-cy=task-form-parameters-password-edit-button]').should(
      'not.exist',
    );
    // when third step is visible
    cy.get('[data-cy=form-stepper-next-button]').click();
    // then it shows no error message and task form
    cy.get('[data-cy=message]').should('not.exist');
    cy.get('[data-cy=task-form]').should('be.visible');
  });
});
