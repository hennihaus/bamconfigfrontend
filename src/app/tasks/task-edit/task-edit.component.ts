import {
  Component,
  computed,
  effect,
  inject,
  input,
  linkedSignal,
  ResourceStatus,
  signal,
} from '@angular/core';
import { catchError, EMPTY, finalize, tap } from 'rxjs';
import { Task } from '../../shared/models/task';
import { TaskStoreService } from '../../shared/services/task-store.service';
import { Router } from '@angular/router';
import { TeamStoreService } from '../../shared/services/team-store.service';
import { TeamType } from '../../shared/models/team-type';
import { LeaveFormGuard } from '../../shared/guards/leave-form.guard';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { TaskFormComponent } from '../task-form/task-form.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-task-edit',
  imports: [MessageComponent, TaskFormComponent, LoadingComponent],
  templateUrl: './task-edit.component.html',
  styleUrl: './task-edit.component.css',
})
export class TaskEditComponent implements LeaveFormGuard {
  private readonly taskStore = inject(TaskStoreService);
  private readonly teamStore = inject(TeamStoreService);
  private readonly router = inject(Router);

  readonly uuid = input.required<string>();

  readonly taskResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.taskStore.getSingle(uuid),
  });
  readonly exampleTeamResource = rxResource({
    loader: () => this.teamStore.getAll({ limit: 1, type: TeamType.EXAMPLE }),
  });
  readonly exampleTeam = computed(
    () => this.exampleTeamResource.value()?.items[0],
  );
  readonly isLoading = linkedSignal(
    () => this.taskResource.isLoading() || this.exampleTeamResource.isLoading(),
  );
  readonly error = linkedSignal(() => this.taskResource.error());

  constructor() {
    effect(() => {
      if (this.taskResource.status() === ResourceStatus.Resolved) {
        this.hasUnsavedChanges.set(false);
      }
    });
  }

  readonly hasUnsavedChanges = signal(false);

  setHasUnsavedChanges(hasUnsavedChanges: boolean) {
    this.hasUnsavedChanges.set(hasUnsavedChanges);
  }

  updateTask(task: Task) {
    this.isLoading.set(true);
    this.error.set(null);
    this.taskStore
      .update(task)
      .pipe(
        tap(() => this.hasUnsavedChanges.set(false)),
        tap({
          next: () => this.error.set(null),
          error: (error) => this.error.set(error),
        }),
        catchError(() => EMPTY),
        finalize(() => this.isLoading.set(false)),
      )
      .subscribe((task) => this.router.navigate(['/tasks/details', task.uuid]));
  }
}
