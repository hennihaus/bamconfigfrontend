import { inject } from '@angular/core';
import { TaskStoreService } from '../../shared/services/task-store.service';
import { AsyncValidatorFn } from '@angular/forms';
import { catchError, map, of } from 'rxjs';

export const uniqueTitle = (): AsyncValidatorFn => {
  const taskStore = inject(TaskStoreService);

  return (control) => {
    const uuid = control.parent?.get('uuid')?.getRawValue();
    const title = control.getRawValue();

    if (!uuid || typeof uuid !== 'string') {
      return of(null);
    }
    if (!title || typeof title !== 'string') {
      return of(null);
    }

    return taskStore.isTitleUnique(uuid, title).pipe(
      map((response) => response.isUnique),
      catchError(() => of(true)),
      map((isValid) => (isValid ? null : { unique: { term: title } })),
    );
  };
};
