import { TaskFormComponent } from './task-form.component';
import { TeamType } from '../../shared/models/team-type';
import { createOutputSpy } from 'cypress/angular';
import { Task } from '../../shared/models/task';
import { DeferBlockFixture, DeferBlockState } from '@angular/core/testing';

describe('TaskFormComponent', () => {
  it('navigates forward and backward with right title and buttons', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.fixture('team').then((team) => {
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.mount(TaskFormComponent, {
          componentProperties: {
            task,
            exampleTeam: {
              ...team,
              type: TeamType.EXAMPLE,
            },
            isSubmitting: false,
          },
        }).then((wrapper) => {
          cy.wrap(wrapper.fixture.getDeferBlocks()).each(
            (deferBlock: DeferBlockFixture) => {
              cy.wrap(deferBlock.render(DeferBlockState.Complete));
            },
          );
        });
      });

      // when
      cy.wait('@isTitleUniqueRequest');

      // then title and stepper buttons of first step are shown
      cy.get('[data-cy=form-stepper-headline]').contains(
        `Aufgaben von '${task.title}' bearbeiten (Schritt 1/3)`,
      );
      cy.get('[data-cy=form-stepper-previous-button]').should('not.exist');
      cy.get('[data-cy=form-stepper-next-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-submit-button]').should('not.exist');

      // when next button is pressed
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.wait('@isTitleUniqueRequest');
      // then title and stepper buttons of second step are shown
      cy.get('[data-cy=form-stepper-headline]').contains(
        `Parameter von '${task.title}' bearbeiten (Schritt 2/3)`,
      );
      cy.get('[data-cy=form-stepper-previous-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-previous-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(242, 113, 28)');
      cy.get('[data-cy=form-stepper-previous-button]').should(
        'not.be.disabled',
      );
      cy.get('[data-cy=form-stepper-next-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(14, 164, 50)');
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-submit-button]').should('not.exist');

      // when next button is pressed
      cy.get('[data-cy=form-stepper-next-button]').click();
      // then title and stepper buttons of third step are shown
      cy.get('[data-cy=form-stepper-headline]').contains(
        `Antworten von '${task.title}' bearbeiten (Schritt 3/3)`,
      );
      cy.get('[data-cy=form-stepper-previous-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-previous-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(242, 113, 28)');
      cy.get('[data-cy=form-stepper-previous-button]').should(
        'not.be.disabled',
      );
      cy.get('[data-cy=form-stepper-next-button]').should('not.exist');
      cy.get('[data-cy=form-stepper-submit-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-submit-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      cy.get('[data-cy=form-stepper-submit-button]').should('not.be.disabled');

      // when previous button is pressed
      cy.get('[data-cy=form-stepper-previous-button]').click();
      // then title and stepper buttons of second step are shown
      cy.get('[data-cy=form-stepper-headline]').contains(
        `Parameter von '${task.title}' bearbeiten (Schritt 2/3)`,
      );
      cy.get('[data-cy=form-stepper-previous-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-previous-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(229, 91, 0)');
      cy.get('[data-cy=form-stepper-previous-button]').should(
        'not.be.disabled',
      );
      cy.get('[data-cy=form-stepper-next-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-submit-button]').should('not.exist');

      // when previous button is pressed
      cy.get('[data-cy=form-stepper-previous-button]').click();
      // then title and stepper buttons of first step are shown
      cy.get('[data-cy=form-stepper-headline]').contains(
        `Aufgaben von '${task.title}' bearbeiten (Schritt 1/3)`,
      );
      cy.get('[data-cy=form-stepper-previous-button]').should('not.exist');
      cy.get('[data-cy=form-stepper-next-button]').should('be.visible');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-submit-button]').should('not.exist');
    });
  });

  it('updates task when all fields are changed and form is submitted for first integration step', () => {
    cy.fixture('tasks').then(([task]) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.fixture('team').then((team) => {
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.mount(TaskFormComponent, {
          componentProperties: {
            task,
            exampleTeam: {
              ...team,
              type: TeamType.EXAMPLE,
            },
            isSubmitting: false,
            taskSubmit: createOutputSpy('taskSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        }).then((wrapper) => {
          cy.wrap(wrapper.fixture.getDeferBlocks()).each(
            (deferBlock: DeferBlockFixture) => {
              cy.wrap(deferBlock.render(DeferBlockState.Complete));
            },
          );
        });
      });
      const updatedTask: Task = {
        ...task,
        title: 'Commerzbank',
        contact: {
          ...task.contact,
          firstname: 'Max',
          lastname: 'Mustermann',
          email: 'max.mustermann@google.de',
        },
        description: 'New description',
        isOpenApiVerbose: false,
        parameters: [
          {
            ...task.parameters[0],
            description: 'New description for parameter',
            example: 'NEW121',
          },
          {
            ...task.parameters[1],
            description: 'New description for parameter',
            example: 'B',
          },
          {
            ...task.parameters[2],
            description: 'New description for parameter',
            example: '1000',
          },
          {
            ...task.parameters[3],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[4],
            description: 'New description for parameter',
          },
        ],
        responses: [
          {
            ...task.responses[0],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[1],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[2],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[3],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
        ],
        updatedAt: now,
      };
      delete updatedTask.banks[0].creditConfiguration;

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type(updatedTask.title);
      cy.get('[data-cy=task-form-contact-firstname-input]').clear();
      cy.get('[data-cy=task-form-contact-firstname-input]').type(
        updatedTask.contact.firstname,
      );
      cy.get('[data-cy=task-form-contact-lastname-input]').clear();
      cy.get('[data-cy=task-form-contact-lastname-input]').type(
        updatedTask.contact.lastname,
      );
      cy.get('[data-cy=task-form-contact-email-input]').clear();
      cy.get('[data-cy=task-form-contact-email-input]').type(
        updatedTask.contact.email,
      );
      cy.get('[data-cy=task-form-is-open-api-verbose-select]').select('Nein');
      cy.get('[data-cy=task-form-description-textarea]')
        .find('.ck-content')
        .then((el) => {
          // https://github.com/cypress-io/cypress/issues/26155
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          const editor = el[0].ckeditorInstance;
          editor.setData(updatedTask.description);
        });
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.parameters[index].description);
        },
      );
      cy.get('[data-cy=task-form-parameters-example-input]').clear();
      cy.get('[data-cy=task-form-parameters-example-input]').type(
        updatedTask.parameters[0].example,
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).select(updatedTask.parameters[1].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).clear();
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).type(updatedTask.parameters[2].example);
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].description);
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].example, {
            parseSpecialCharSequences: false,
          });
        },
      );
      cy.get('[data-cy=form-stepper-submit-button]').click();

      // then
      cy.wait('@isTitleUniqueRequest');
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@taskSubmitSpy').should('have.been.calledWith', {
        ...updatedTask,
        description: `<p>${updatedTask.description}</p>`,
      });
    });
  });

  it('updates task when all fields are changed and form is submitted for second integration step', () => {
    cy.fixture('tasks').then(([_, task]) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.fixture('team').then((team) => {
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.mount(TaskFormComponent, {
          componentProperties: {
            task,
            exampleTeam: {
              ...team,
              type: TeamType.EXAMPLE,
            },
            isSubmitting: false,
            taskSubmit: createOutputSpy('taskSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        }).then((wrapper) => {
          cy.wrap(wrapper.fixture.getDeferBlocks()).each(
            (deferBlock: DeferBlockFixture) => {
              cy.wrap(deferBlock.render(DeferBlockState.Complete));
            },
          );
        });
      });
      const updatedTask: Task = {
        ...task,
        title: 'Commerzbank',
        contact: {
          ...task.contact,
          firstname: 'Max',
          lastname: 'Mustermann',
          email: 'max.mustermann@google.de',
        },
        isOpenApiVerbose: false,
        description: 'New description',
        parameters: [
          {
            ...task.parameters[0],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[1],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[2],
            description: 'New description for parameter',
            example: 'B',
          },
          {
            ...task.parameters[3],
            description: 'New description for parameter',
            example: '1000',
          },
          {
            ...task.parameters[4],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[5],
            description: 'New description for parameter',
          },
        ],
        responses: [
          {
            ...task.responses[0],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[1],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[2],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
          {
            ...task.responses[3],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
        ],
        updatedAt: now,
      };
      delete updatedTask.banks[0].creditConfiguration;

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type(updatedTask.title);
      cy.get('[data-cy=task-form-contact-firstname-input]').clear();
      cy.get('[data-cy=task-form-contact-firstname-input]').type(
        updatedTask.contact.firstname,
      );
      cy.get('[data-cy=task-form-contact-lastname-input]').clear();
      cy.get('[data-cy=task-form-contact-lastname-input]').type(
        updatedTask.contact.lastname,
      );
      cy.get('[data-cy=task-form-contact-email-input]').clear();
      cy.get('[data-cy=task-form-contact-email-input]').type(
        updatedTask.contact.email,
      );
      cy.get('[data-cy=task-form-is-open-api-verbose-select]').select('Nein');
      cy.get('[data-cy=task-form-description-textarea]')
        .find('.ck-content')
        .then((el) => {
          // https://github.com/cypress-io/cypress/issues/26155
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          const editor = el[0].ckeditorInstance;
          editor.setData(updatedTask.description);
        });
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.parameters[index].description);
        },
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).select(updatedTask.parameters[2].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).clear();
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).type(updatedTask.parameters[3].example);
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].description);
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].example, {
            parseSpecialCharSequences: false,
          });
        },
      );
      cy.get('[data-cy=form-stepper-submit-button]').click();

      // then
      cy.wait('@isTitleUniqueRequest');
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@taskSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...updatedTask,
        description: `<p>${updatedTask.description}</p>`,
      });
    });
  });

  it('updates task when all fields are changed and form is submitted for third integration step', () => {
    cy.fixture('tasks').then(([_1, _2, task]) => {
      // given
      const now = new Date(2024, 4, 26, 14, 0, 0, 0);
      cy.clock(now);
      cy.fixture('team').then((team) => {
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.mount(TaskFormComponent, {
          componentProperties: {
            task,
            exampleTeam: {
              ...team,
              type: TeamType.EXAMPLE,
            },
            isSubmitting: false,
            taskSubmit: createOutputSpy('taskSubmitSpy'),
            hasUnsavedChanges: createOutputSpy('hasUnsavedChangesSpy'),
          },
        }).then((wrapper) => {
          cy.wrap(wrapper.fixture.getDeferBlocks()).each(
            (deferBlock: DeferBlockFixture) => {
              cy.wrap(deferBlock.render(DeferBlockState.Complete));
            },
          );
        });
      });
      const updatedTask: Task = {
        ...task,
        title: 'New title for async banks',
        contact: {
          ...task.contact,
          firstname: 'Max',
          lastname: 'Mustermann',
          email: 'max.mustermann@google.de',
        },
        isOpenApiVerbose: false,
        description: 'New description',
        parameters: [
          {
            ...task.parameters[0],
            description: 'New description for parameter',
            example: 'df75294582e84d46',
          },
          {
            ...task.parameters[1],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[2],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[3],
            description: 'New description for parameter',
            example: 'B',
          },
          {
            ...task.parameters[4],
            description: 'New description for parameter',
            example: '1000',
          },
          {
            ...task.parameters[5],
            description: 'New description for parameter',
          },
          {
            ...task.parameters[6],
            description: 'New description for parameter',
          },
        ],
        responses: [
          {
            ...task.responses[0],
            description: 'New description for response',
            example:
              '{"reasons": [{"message": "error", "exception": "Exception"}], "dateTime": "2024-08-16T00:00:00"}',
          },
        ],
        updatedAt: now,
      };
      delete updatedTask.banks[0].creditConfiguration;

      // when
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type(updatedTask.title);
      cy.get('[data-cy=task-form-contact-firstname-input]').clear();
      cy.get('[data-cy=task-form-contact-firstname-input]').type(
        updatedTask.contact.firstname,
      );
      cy.get('[data-cy=task-form-contact-lastname-input]').clear();
      cy.get('[data-cy=task-form-contact-lastname-input]').type(
        updatedTask.contact.lastname,
      );
      cy.get('[data-cy=task-form-contact-email-input]').clear();
      cy.get('[data-cy=task-form-contact-email-input]').type(
        updatedTask.contact.email,
      );
      cy.get('[data-cy=task-form-is-open-api-verbose-select]').select('Nein');
      cy.get('[data-cy=task-form-description-textarea]')
        .find('.ck-content')
        .then((el) => {
          // https://github.com/cypress-io/cypress/issues/26155
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          const editor = el[0].ckeditorInstance;
          editor.setData(updatedTask.description);
        });
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.parameters[index].description);
        },
      );
      cy.get('[data-cy=task-form-parameters-example-input]').clear();
      cy.get('[data-cy=task-form-parameters-example-input]').type(
        updatedTask.parameters[0].example,
      );
      cy.get(
        '[data-cy=task-form-parameters-rating-level-example-input]',
      ).select(updatedTask.parameters[3].example);
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).clear();
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).type(updatedTask.parameters[4].example);
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].description);
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          cy.wrap(el).clear();
          cy.wrap(el).type(updatedTask.responses[index].example, {
            parseSpecialCharSequences: false,
          });
        },
      );
      cy.get('[data-cy=form-stepper-submit-button]').click();

      // then
      cy.wait('@isTitleUniqueRequest');
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@taskSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...updatedTask,
        description: `<p>${updatedTask.description}</p>`,
      });
    });
  });

  it('shows errors when one field is invalid and disable buttons', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.fixture('team').then((team) => {
        cy.intercept('GET', `/v1/tasks/${task.uuid}/unique/title/*`, {
          isUnique: true,
        }).as('isTitleUniqueRequest');
        cy.mount(TaskFormComponent, {
          componentProperties: {
            task,
            exampleTeam: {
              ...team,
              type: TeamType.EXAMPLE,
            },
            isSubmitting: false,
            taskSubmit: createOutputSpy('taskSubmitSpy'),
          },
        }).then((wrapper) => {
          cy.wrap(wrapper.fixture.getDeferBlocks()).each(
            (deferBlock: DeferBlockFixture) => {
              cy.wrap(deferBlock.render(DeferBlockState.Complete));
            },
          );
        });
      });
      cy.wait('@isTitleUniqueRequest');

      // first step
      // when title in first step is invalid
      cy.get('[data-cy=task-form-title-input]').clear();
      // then it disables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get('[data-cy=task-form-title-input]').type(task.title);
      // then it enables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // when contact firstname in first step is invalid
      cy.get('[data-cy=task-form-contact-firstname-input]').clear();
      // then it disabled next button
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get('[data-cy=task-form-contact-firstname-input]').type(
        task.contact.firstname,
      );
      // then it enables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // when contact lastname in first step is invalid
      cy.get('[data-cy=task-form-contact-lastname-input]').clear();
      // then it disabled next button
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get('[data-cy=task-form-contact-lastname-input]').type(
        task.contact.lastname,
      );
      // then it enables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // when contact email in first step is invalid
      cy.get('[data-cy=task-form-contact-email-input]').clear();
      // then it disabled next button
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get('[data-cy=task-form-contact-email-input]').type(
        task.contact.email,
      );
      // then it enables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // when description in first step is invalid
      cy.get('[data-cy=task-form-description-textarea]')
        .find('.ck-content')
        .clear();
      // then it disabled next button
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get('[data-cy=task-form-description-textarea]')
        .find('.ck-content')
        .then((el) => {
          // https://github.com/cypress-io/cypress/issues/26155
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-expect-error
          const editor = el[0].ckeditorInstance;
          editor.setData(task.description);
        });
      // then it enables and change color of next button
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // second step
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-parameters-description-input]').each(
        (el, index) => {
          // when description in second step is invalid
          cy.wrap(el).clear();
          cy.wrap(el).type('T'.repeat(101));
          // then it disables and change color of previous and next button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'be.disabled',
          );
          cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          cy.get('[data-cy=form-stepper-next-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          // when error is fixed
          cy.wrap(el).clear();
          cy.wrap(el).type(task.parameters[index].description);
          // then it enables and change color of previous and next button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-next-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(242, 113, 28)');
          cy.get('[data-cy=form-stepper-next-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(33, 186, 69)');
        },
      );
      // when delay in milliseconds in second step is invalid
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).clear();
      // then it disables and change color of previous and next button
      cy.get('[data-cy=form-stepper-previous-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-next-button]').should('be.disabled');
      cy.get('[data-cy=form-stepper-previous-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(219, 40, 40)');
      // when error is fixed
      cy.get(
        '[data-cy=task-form-parameters-delay-in-milliseconds-example-input]',
      ).type(task.parameters[3].example);
      // then it enables and change color of previous and next button
      cy.get('[data-cy=form-stepper-previous-button]').should(
        'not.be.disabled',
      );
      cy.get('[data-cy=form-stepper-next-button]').should('not.be.disabled');
      cy.get('[data-cy=form-stepper-previous-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(242, 113, 28)');
      cy.get('[data-cy=form-stepper-next-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');
      // third step
      cy.get('[data-cy=form-stepper-next-button]').click();
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          // when description in third step is invalid
          cy.wrap(el).clear();
          // then it disables and change color of previous and submit button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'be.disabled',
          );
          cy.get('[data-cy=form-stepper-submit-button]').should('be.disabled');
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          cy.get('[data-cy=form-stepper-submit-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          // when error is fixed
          cy.wrap(el).type(task.responses[index].description);
          // then it enables and change color of previous and submit button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-submit-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(242, 113, 28)');
          cy.get('[data-cy=form-stepper-submit-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(33, 186, 69)');
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          // when example in third step is invalid
          cy.wrap(el).clear();
          // then it disables and change color of previous and submit button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'be.disabled',
          );
          cy.get('[data-cy=form-stepper-submit-button]').should('be.disabled');
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          cy.get('[data-cy=form-stepper-submit-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(219, 40, 40)');
          // when error is fixed
          cy.wrap(el).type(task.responses[index].example, {
            parseSpecialCharSequences: false,
          });
          // then it enables and change color of previous and submit button
          cy.get('[data-cy=form-stepper-previous-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-submit-button]').should(
            'not.be.disabled',
          );
          cy.get('[data-cy=form-stepper-previous-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(242, 113, 28)');
          cy.get('[data-cy=form-stepper-submit-button]')
            .should('have.css', 'background-color')
            .and('eq', 'rgb(33, 186, 69)');
        },
      );
      // then no submit is triggered
      cy.get('@taskSubmitSpy').should('not.have.been.called');
    });
  });
});
