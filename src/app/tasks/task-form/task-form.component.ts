import { Component, effect, inject, input, output } from '@angular/core';
import { Task } from '../../shared/models/task';
import { Team } from '../../shared/models/team';
import { filter, map } from 'rxjs';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { ReactiveFormsModule, ValueChangeEvent } from '@angular/forms';
import { TaskFormResponsesComponent } from '../task-form-responses/task-form-responses.component';
import { TaskFormParametersComponent } from '../task-form-parameters/task-form-parameters.component';
import { TaskFormCommonComponent } from '../task-form-common/task-form-common.component';
import { CdkStep } from '@angular/cdk/stepper';
import { FormStepperComponent } from '../../shared/components/form-stepper/form-stepper.component';
import { TaskFormDescriptionComponent } from '../task-form-description/task-form-description.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { TaskFormDescriptionPlaceholderComponent } from '../task-form-description-placeholder/task-form-description-placeholder.component';

@Component({
  selector: 'bam-task-form',
  imports: [
    ReactiveFormsModule,
    FormStepperComponent,
    CdkStep,
    TaskFormCommonComponent,
    TaskFormParametersComponent,
    TaskFormResponsesComponent,
    TaskFormDescriptionComponent,
    TaskFormDescriptionPlaceholderComponent,
  ],
  providers: [TaskFormStoreService],
  templateUrl: './task-form.component.html',
  styleUrl: './task-form.component.css',
})
export class TaskFormComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);

  readonly isSubmitting = input.required<boolean>();
  readonly task = input.required<Task>();
  readonly exampleTeam = input<Team>();

  readonly taskSubmit = output<Task>();
  readonly hasUnsavedChanges = output<boolean>();

  constructor() {
    effect(() => {
      const task = this.task();

      this.taskFormStore.setInitialTaskFormValues(task);
      this.taskFormStore.setTaskFormValues(task);
    });

    this.form.events
      .pipe(
        takeUntilDestroyed(),
        filter(
          (event): event is ValueChangeEvent<unknown> =>
            event instanceof ValueChangeEvent,
        ),
        map(() => [
          this.taskFormStore.initialForm.getRawValue(),
          this.taskFormStore.form.getRawValue(),
        ]),
        map(
          ([initialFormValue, currentFormValue]) =>
            JSON.stringify(initialFormValue) !==
            JSON.stringify(currentFormValue),
        ),
      )
      .subscribe((hasUnsavedChanges) =>
        this.hasUnsavedChanges.emit(hasUnsavedChanges),
      );
  }

  get stepperTitles() {
    return [
      $localize`:taskTitle is a placeholder and should stay in every language:Aufgaben von '${this.formTitle}' bearbeiten`,
      $localize`:taskTitle is a placeholder and should stay in every language:Parameter von '${this.formTitle}' bearbeiten`,
      $localize`:taskTitle is a placeholder and should stay in every language:Antworten von '${this.formTitle}' bearbeiten`,
    ];
  }

  get form() {
    return this.taskFormStore.form;
  }

  get formTitle() {
    return this.form.controls.title.getRawValue();
  }

  get isFirstStepValid() {
    if (this.form.controls.title.invalid) {
      return false;
    }
    if (this.form.controls.contact.invalid) {
      return false;
    }
    if (this.form.controls.isOpenApiVerbose.invalid) {
      return false;
    }
    if (this.form.controls.description.invalid) {
      return false;
    }
    return true;
  }

  submitForm() {
    const task = this.form.getRawValue();

    if (this.form.invalid) {
      return;
    }
    if (task) {
      this.taskSubmit.emit({
        ...task,
        parameters: task.parameters.map((parameter) => ({
          ...parameter,
          example: parameter.example.toString(),
        })),
        updatedAt: new Date(),
      });
    }
  }
}
