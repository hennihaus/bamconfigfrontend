import { Component, computed, input } from '@angular/core';
import { Task } from '../../shared/models/task';
import { HasNoAsyncBanksActivatedPipe } from '../pipes/has-no-async-banks-activated.pipe';
import { TaskListPrintItemComponent } from '../task-list-print-item/task-list-print-item.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { Message } from '../../shared/models/message';

@Component({
  selector: 'bam-task-list-print',
  imports: [
    MessageComponent,
    TaskListPrintItemComponent,
    HasNoAsyncBanksActivatedPipe,
  ],
  templateUrl: './task-list-print.component.html',
  styleUrl: './task-list-print.component.css',
})
export class TaskListPrintComponent {
  readonly printData = input.required<Task[] | Message>();

  readonly tasks = computed(() => {
    const printData = this.printData();

    if (Array.isArray(printData)) {
      return printData;
    }
    return null;
  });
  readonly message = computed(() => {
    const printData = this.printData();

    if (Array.isArray(printData)) {
      return null;
    }
    return printData;
  });

  get notFoundMessage(): Message {
    return {
      text: $localize`Keine Aufgaben gefunden`,
      type: 'negative',
      size: 'massive',
    };
  }
}
