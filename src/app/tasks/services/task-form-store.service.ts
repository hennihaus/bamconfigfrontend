import { inject, Injectable } from '@angular/core';
import {
  FormArray,
  FormGroup,
  NonNullableFormBuilder,
  Validators,
} from '@angular/forms';
import {
  BankForm,
  ContactForm,
  CreditConfigurationForm,
  EndpointForm,
  ParameterForm,
  ResponseForm,
  TaskForm,
} from '../../shared/models/task-form';
import { Task } from '../../shared/models/task';
import { Contact } from '../../shared/models/contact';
import { Endpoint } from '../../shared/models/endpoint';
import { Parameter } from '../../shared/models/parameter';
import { integer } from '../../shared/validators/integer.validator';
import { Response } from '../../shared/models/response';
import { json } from '../../shared/validators/json.validator';
import { notEmptyJson } from '../../shared/validators/not-empty-json.validator';
import { Bank } from '../../shared/models/bank';
import { CreditConfiguration } from '../../shared/models/credit-configuration';
import { v4 as uuidv4 } from 'uuid';
import { uniqueTitle } from '../validators/unique-title.validator';

@Injectable()
export class TaskFormStoreService {
  private readonly formBuilder = inject(NonNullableFormBuilder);

  private readonly initialTaskId = uuidv4();
  private readonly initialContactId = uuidv4();

  readonly form = this.createEmptyTaskFormValues();
  readonly initialForm = this.createEmptyTaskFormValues();

  setInitialTaskFormValues(task: Task) {
    const endpoints = this.buildEndpointsFormValues(task.endpoints);
    const parameters = this.buildParametersFormValues(task.parameters);
    const responses = this.buildResponsesFormValues(task.responses);
    const banks = this.buildBanksFormValues(task.banks);

    this.initialForm.patchValue(task);
    this.initialForm.setControl('endpoints', endpoints);
    this.initialForm.setControl('parameters', parameters);
    this.initialForm.setControl('responses', responses);
    this.initialForm.setControl('banks', banks);
  }

  setTaskFormValues(task: Task) {
    const endpoints = this.buildEndpointsFormValues(task.endpoints);
    const parameters = this.buildParametersFormValues(task.parameters);
    const responses = this.buildResponsesFormValues(task.responses);
    const banks = this.buildBanksFormValues(task.banks);

    this.form.patchValue(task);
    this.form.setControl('endpoints', endpoints);
    this.form.setControl('parameters', parameters);
    this.form.setControl('responses', responses);
    this.form.setControl('banks', banks);
  }

  private createEmptyTaskFormValues(): FormGroup<TaskForm> {
    const contact = this.getBaseContact();

    return this.formBuilder.group({
      uuid: this.initialTaskId,
      title: this.formBuilder.control('', {
        validators: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
        asyncValidators: [uniqueTitle()],
      }),
      description: ['', [Validators.required, Validators.maxLength(2_000)]],
      integrationStep: 1,
      isOpenApiVerbose: this.formBuilder.control(false),
      contact: this.buildContactFormValues({
        ...contact,
        uuid: this.initialContactId,
      }),
      endpoints: this.buildEndpointsFormValues([]),
      parameters: this.buildParametersFormValues([]),
      responses: this.buildResponsesFormValues([]),
      banks: this.buildBanksFormValues([]),
      updatedAt: new Date(),
    });
  }

  private buildContactFormValues(contact: Contact): FormGroup<ContactForm> {
    return this.formBuilder.group({
      uuid: contact.uuid,
      firstname: [
        contact.firstname,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ],
      ],
      lastname: [
        contact.lastname,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50),
        ],
      ],
      email: [contact.email, [Validators.required, Validators.email]],
    });
  }

  private buildEndpointsFormValues(
    endpoints: Endpoint[],
  ): FormArray<FormGroup<EndpointForm>> {
    return this.formBuilder.array(
      endpoints.map((endpoint) => this.buildEndpointFormValues(endpoint)),
    );
  }

  private buildEndpointFormValues(endpoint: Endpoint): FormGroup<EndpointForm> {
    return this.formBuilder.group({
      uuid: endpoint.uuid,
      type: endpoint.type,
      url: endpoint.url,
      docsUrl: endpoint.docsUrl,
    });
  }

  private buildParametersFormValues(
    parameters: Parameter[],
  ): FormArray<FormGroup<ParameterForm>> {
    return this.formBuilder.array(
      parameters.map((parameter) => this.buildParameterFormValues(parameter)),
    );
  }

  private buildParameterFormValues(
    parameter: Parameter,
  ): FormGroup<ParameterForm> {
    return this.formBuilder.group({
      uuid: parameter.uuid,
      name: parameter.name,
      type: parameter.type,
      description: [parameter.description, [Validators.maxLength(100)]],
      example: this.formBuilder.control(
        {
          value: parameter.example,
          disabled:
            parameter.name === 'username' ||
            parameter.name === 'password' ||
            parameter.name === 'amountInEuros' ||
            parameter.name === 'termInMonths',
        },
        {
          validators:
            parameter.name === 'delayInMilliseconds'
              ? [
                  Validators.required,
                  integer,
                  Validators.min(0),
                  Validators.max(Number.MAX_SAFE_INTEGER),
                ]
              : [
                  Validators.required,
                  Validators.minLength(1),
                  Validators.maxLength(50),
                ],
        },
      ),
    });
  }

  private buildResponsesFormValues(
    responses: Response[],
  ): FormArray<FormGroup<ResponseForm>> {
    return this.formBuilder.array(
      responses.map((response) => this.buildResponseFormValues(response)),
    );
  }

  private buildResponseFormValues(response: Response): FormGroup<ResponseForm> {
    return this.formBuilder.group({
      uuid: response.uuid,
      httpStatusCode: response.httpStatusCode,
      contentType: response.contentType,
      description: [
        response.description,
        [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(100),
        ],
      ],
      example: [response.example, [Validators.required, json, notEmptyJson]],
    });
  }

  private buildBanksFormValues(banks: Bank[]): FormArray<FormGroup<BankForm>> {
    return this.formBuilder.array(
      banks.map((bank) => this.buildBankFormValues(bank)),
    );
  }

  private buildBankFormValues(bank: Bank): FormGroup<BankForm> {
    return this.formBuilder.group({
      uuid: bank.uuid,
      name: bank.name,
      jmsQueue: bank.jmsQueue,
      thumbnailUrl: bank.thumbnailUrl,
      isAsync: bank.isAsync,
      isActive: bank.isActive,
      ...(bank.creditConfiguration && {
        creditConfiguration: this.buildCreditConfigurationFormValues(
          bank.creditConfiguration,
        ),
      }),
      updatedAt: bank.updatedAt,
    });
  }

  private buildCreditConfigurationFormValues(
    creditConfiguration: CreditConfiguration,
  ): FormGroup<CreditConfigurationForm> {
    return this.formBuilder.group({
      minAmountInEuros: creditConfiguration.minAmountInEuros,
      maxAmountInEuros: creditConfiguration.maxAmountInEuros,
      minTermInMonths: creditConfiguration.minTermInMonths,
      maxTermInMonths: creditConfiguration.maxTermInMonths,
      minSchufaRating: creditConfiguration.minSchufaRating,
      maxSchufaRating: creditConfiguration.maxSchufaRating,
    });
  }

  private getBaseContact(): Contact {
    return {
      uuid: uuidv4(),
      firstname: '',
      lastname: '',
      email: '',
    };
  }
}
