import { Routes } from '@angular/router';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { leaveFormGuard } from '../shared/guards/leave-form.guard';
import { TaskEditComponent } from './task-edit/task-edit.component';

export const TASKS_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: TaskListComponent,
    title: $localize`Aufgaben`,
  },
  {
    path: 'list/:pageNumber',
    component: TaskListComponent,
    title: $localize`Aufgaben`,
  },
  {
    path: 'details',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'details/:uuid',
    component: TaskDetailsComponent,
    title: $localize`Aufgabe`,
  },
  {
    path: 'edit',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'edit/:uuid',
    component: TaskEditComponent,
    title: $localize`Aufgabe bearbeiten`,
    canDeactivate: [leaveFormGuard],
  },
];
