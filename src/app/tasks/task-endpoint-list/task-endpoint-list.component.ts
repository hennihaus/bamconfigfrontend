import { Component, input } from '@angular/core';
import { Endpoint } from '../../shared/models/endpoint';

@Component({
  selector: 'bam-task-endpoint-list',
  templateUrl: './task-endpoint-list.component.html',
  styleUrl: './task-endpoint-list.component.css',
})
export class TaskEndpointListComponent {
  readonly endpoints = input.required<Endpoint[]>();
}
