import { TaskEndpointListComponent } from './task-endpoint-list.component';

describe('TaskEndpointListComponent', () => {
  it('shows right amount of endpoints with values and fallbacks', () => {
    // given
    const endpoints = [
      {
        uuid: 'a2058a25-5934-414a-a67f-afeb4c0e1bec',
        type: 'REST',
        url: '',
        docsUrl: 'https://hennihaus.github.io/bamschufarest',
      },
      {
        uuid: '35540453-ea3d-4f87-a19f-486c29e68b4f',
        type: 'JMS',
        url: 'tcp://bambusinessintegration.wi.hs-furtwangen.de:61616',
        docsUrl: '',
      },
    ];
    cy.mount(TaskEndpointListComponent, {
      componentProperties: {
        endpoints,
      },
    });

    // then
    cy.get('[data-cy=endpoint-url]').should('have.length', 2);
    cy.get('[data-cy=endpoint-docs-url]').should('have.length', 2);
    cy.get('[data-cy=endpoint-url]')
      .eq(0)
      .find('[data-cy=endpoint-url-link]')
      .should('not.exist');
    cy.get('[data-cy=endpoint-url]')
      .eq(0)
      .find('[data-cy=endpoint-url-hint]')
      .should('be.visible');
    cy.get('[data-cy=endpoint-url]')
      .eq(1)
      .find('[data-cy=endpoint-url-link]')
      .should('have.attr', 'href', endpoints[1].url);
    cy.get('[data-cy=endpoint-url]')
      .eq(1)
      .find('[data-cy=endpoint-url-link]')
      .contains(endpoints[1].url);
    cy.get('[data-cy=endpoint-url]')
      .eq(1)
      .find('[data-cy=endpoint-url-hint]')
      .should('not.exist');
    cy.get('[data-cy=endpoint-docs-url]')
      .eq(0)
      .find('[data-cy=endpoint-docs-url-link]')
      .should('have.attr', 'href', endpoints[0].docsUrl);
    cy.get('[data-cy=endpoint-docs-url]')
      .eq(0)
      .find('[data-cy=endpoint-docs-url-link]')
      .contains(endpoints[0].docsUrl);
    cy.get('[data-cy=endpoint-docs-url]')
      .eq(0)
      .find('[data-cy=endpoint-docs-url-hint]')
      .should('not.exist');
    cy.get('[data-cy=endpoint-docs-url]')
      .eq(1)
      .find('[data-cy=endpoint-docs-url-link]')
      .should('not.exist');
    cy.get('[data-cy=endpoint-docs-url]')
      .eq(1)
      .find('[data-cy=endpoint-docs-url-hint]')
      .should('be.visible');
  });
});
