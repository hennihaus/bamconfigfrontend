import { Component, inject, LOCALE_ID, ViewEncapsulation } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {
  AccessibilityHelp,
  AutoLink,
  Bold,
  ClassicEditor,
  EditorConfig,
  Essentials,
  Indent,
  IndentBlock,
  Italic,
  Link,
  List,
  Paragraph,
  Undo,
} from 'ckeditor5';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import coreTranslationsGerman from 'ckeditor5/translations/de.js';
import coreTranslationsEnglish from 'ckeditor5/translations/en.js';

@Component({
  selector: 'bam-task-form-description',
  imports: [CKEditorModule, FormErrorMessageComponent, ReactiveFormsModule],
  templateUrl: './task-form-description.component.html',
  styleUrl: './task-form-description.component.css',
  /* eslint-disable-next-line @angular-eslint/use-component-view-encapsulation --
   * This disable is necessary to load ck editor styles.
   */
  encapsulation: ViewEncapsulation.None,
})
export class TaskFormDescriptionComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);
  private readonly locale = inject(LOCALE_ID);

  get form() {
    return this.taskFormStore.form;
  }

  get config(): EditorConfig {
    return {
      language: this.locale,
      placeholder: $localize`Beschreibung`,
      toolbar: [
        'bold',
        'italic',
        '|',
        'link',
        '|',
        'bulletedList',
        'numberedList',
        '|',
        'outdent',
        'indent',
        '|',
        'undo',
        'redo',
      ],
      plugins: [
        // base features
        Essentials,
        Paragraph,
        AutoLink,
        AccessibilityHelp,
        Indent,
        // visible features
        Bold,
        Italic,
        Link,
        List,
        IndentBlock,
        Undo,
      ],
      licenseKey: 'GPL',
      link: {
        addTargetToExternalLinks: true,
      },
      translations: [coreTranslationsGerman, coreTranslationsEnglish],
    };
  }

  protected readonly Editor = ClassicEditor;
}
