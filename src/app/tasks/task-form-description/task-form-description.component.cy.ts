import { TaskFormDescriptionComponent } from './task-form-description.component';
import { TaskFormStoreService } from '../services/task-form-store.service';

describe('TaskFormDescriptionComponent', () => {
  it('shows prefilled and valid form', () => {
    cy.fixture('task').then((task) => {
      // given
      // TODO: <form class='ui equal width form'><bam-task-form-description /><form>
      cy.mount(TaskFormDescriptionComponent, {
        providers: [
          {
            provide: TaskFormStoreService,
            useFactory: () => {
              const taskFormStore = new TaskFormStoreService();

              taskFormStore.setInitialTaskFormValues(task);
              taskFormStore.setTaskFormValues(task);

              return taskFormStore;
            },
          },
        ],
      });

      // then
      cy.get('[data-cy=task-form-description-textarea]')
        .find('[data-placeholder=Beschreibung]')
        .should(
          'have.text',
          task.description.replace('<p>', '').replace('</p>', ''),
        );
      cy.get('[data-cy=form-error-message]').should('not.exist');
      cy.get('[data-cy=task-form-description-field]')
        .find('button')
        .should('have.length', 9);
    });
  });

  it('shows errors for description', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      // TODO: <form class='ui equal width form'><bam-task-form-description /><form>
      cy.mount(TaskFormDescriptionComponent, {
        providers: [
          {
            provide: TaskFormStoreService,
            useFactory: () => {
              const taskFormStore = new TaskFormStoreService();

              taskFormStore.setInitialTaskFormValues(task);
              taskFormStore.setTaskFormValues(task);

              return taskFormStore;
            },
          },
        ],
      });
      cy.get('[data-cy=task-form-description-textarea]')
        .find('[data-placeholder=Beschreibung]')
        .should(
          'have.text',
          task.description.replace('<p>', '').replace('</p>', ''),
        );
      cy.wait('@isTitleUniqueRequest');
    });

    // when field is cleared
    cy.get('[data-cy=task-form-description-textarea]')
      .find('.ck-content')
      .clear();
    // then error is shown
    cy.get('[data-cy=task-form-description-field]').contains('Pflichtfeld');
    // when too long field is entered
    cy.get('[data-cy=task-form-description-textarea]')
      .find('.ck-content')
      .then((el) => {
        // https://github.com/cypress-io/cypress/issues/26155
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-expect-error
        const editor = el[0].ckeditorInstance;
        editor.setData('T'.repeat(2_001));
      });
    // then error is shown
    cy.get('[data-cy=task-form-description-field]').contains('nicht länger');
  });
});
