import { TaskFormCommonComponent } from './task-form-common.component';
import { TaskFormStoreService } from '../services/task-form-store.service';

describe('TaskFormCommonComponent', () => {
  it('shows prefilled and valid form', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept(
        'GET',
        `/v1/tasks/${task.uuid}/unique/title/Deutsche%20Bank`,
        {
          isUnique: true,
        },
      ).as('isTitleUniqueRequest');
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-title-input]').should(
        'have.value',
        task.title,
      );
      cy.get('[data-cy=task-form-contact-firstname-input]').should(
        'have.value',
        task.contact.firstname,
      );
      cy.get('[data-cy=task-form-contact-lastname-input]').should(
        'have.value',
        task.contact.lastname,
      );
      cy.get('[data-cy=task-form-contact-email-input]').should(
        'have.value',
        task.contact.email,
      );
      cy.get('[data-cy=task-form-is-open-api-verbose-select]').should(
        'have.value',
        `0: ${task.isOpenApiVerbose}`,
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows errors for title', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-title-input]').should(
        'have.value',
        task.title,
      );

      // when field is cleared
      cy.get('[data-cy=task-form-title-input]').clear();
      // then error is shown
      cy.get('[data-cy=task-form-title-field]').contains('Pflichtfeld');
      // when too small field is entered
      cy.get('[data-cy=task-form-title-input]').type('T'.repeat(5));
      // then error is shown
      cy.get('[data-cy=task-form-title-field]').contains('mindestens');
      // when too long field is entered
      cy.get('[data-cy=task-form-title-input]').type('T'.repeat(51));
      // then error is shown
      cy.get('[data-cy=task-form-title-field]').contains('nicht länger');
      cy.wait('@isTitleUniqueRequest');
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: false,
      }).as('isTitleUniqueRequest');
      cy.get('[data-cy=task-form-title-input]').clear();
      cy.get('[data-cy=task-form-title-input]').type(task.title);
      // then error is shown
      cy.wait('@isTitleUniqueRequest');
      cy.get('[data-cy=task-form-title-field]').contains('existiert bereits');
    });
  });

  it('shows no errors for valid title and unique title request fails', () => {
    // given
    cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
      statusCode: 500,
      body: {},
    }).as('isTitleUniqueRequest');
    cy.fixture('task').then((task) => {
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-title-input]').should(
        'have.value',
        task.title,
      );
    });

    // when
    cy.get('[data-cy=task-form-title-input]').clear();
    cy.get('[data-cy=task-form-title-input]').type('New task title');

    // then
    cy.wait('@isTitleUniqueRequest');
    cy.get('[data-cy=task-form-title-field]')
      .find('[data-cy=form-error-message]')
      .should('not.exist');
  });

  it('shows errors for contact firstname', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-contact-firstname-input]').should(
        'have.value',
        task.contact.firstname,
      );
      cy.wait('@isTitleUniqueRequest');
    });

    // when field is cleared
    cy.get('[data-cy=task-form-contact-firstname-input]').clear();
    // then error is shown
    cy.get('[data-cy=task-form-contact-firstname-field]').contains(
      'Pflichtfeld',
    );
    // when too small field is entered
    cy.get('[data-cy=task-form-contact-firstname-input]').type('T'.repeat(1));
    // then error is shown
    cy.get('[data-cy=task-form-contact-firstname-field]').contains(
      'mindestens',
    );
    // when too long field is entered
    cy.get('[data-cy=task-form-contact-firstname-input]').type('T'.repeat(51));
    // then error is shown
    cy.get('[data-cy=task-form-contact-firstname-field]').contains(
      'nicht länger',
    );
  });

  it('shows errors for contact lastname', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-contact-lastname-input]').should(
        'have.value',
        task.contact.lastname,
      );
      cy.wait('@isTitleUniqueRequest');
    });

    // when field is cleared
    cy.get('[data-cy=task-form-contact-lastname-input]').clear();
    // then error is shown
    cy.get('[data-cy=task-form-contact-lastname-field]').contains(
      'Pflichtfeld',
    );
    // when too small field is entered
    cy.get('[data-cy=task-form-contact-lastname-input]').type('T'.repeat(1));
    // then error is shown
    cy.get('[data-cy=task-form-contact-lastname-field]').contains('mindestens');
    // when too long field is entered
    cy.get('[data-cy=task-form-contact-lastname-input]').type('T'.repeat(51));
    // then error is shown
    cy.get('[data-cy=task-form-contact-lastname-field]').contains(
      'nicht länger',
    );
  });

  it('shows errors for contact email', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', {
        isUnique: true,
      }).as('isTitleUniqueRequest');
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-common /><form>',
        {
          imports: [TaskFormCommonComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-contact-email-input]').should(
        'have.value',
        task.contact.email,
      );
      cy.wait('@isTitleUniqueRequest');
    });

    // when field is cleared
    cy.get('[data-cy=task-form-contact-email-input]').clear();
    // then error is shown
    cy.get('[data-cy=task-form-contact-email-field]').contains('Pflichtfeld');
    // when invalid email is entered
    cy.get('[data-cy=task-form-contact-email-input]').type('invalidEmail');
    // then error is shown
    cy.get('[data-cy=task-form-contact-email-field]').contains(
      'gültige E-Mail-Adresse',
    );
  });
});
