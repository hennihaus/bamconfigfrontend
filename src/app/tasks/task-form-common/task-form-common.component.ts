import { Component, inject } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskFormStoreService } from '../services/task-form-store.service';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';

@Component({
  selector: 'bam-task-form-common',
  imports: [ReactiveFormsModule, FormErrorMessageComponent],
  templateUrl: './task-form-common.component.html',
  styleUrl: './task-form-common.component.css',
})
export class TaskFormCommonComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);

  get form() {
    return this.taskFormStore.form;
  }

  get contact() {
    return this.taskFormStore.form.controls.contact;
  }
}
