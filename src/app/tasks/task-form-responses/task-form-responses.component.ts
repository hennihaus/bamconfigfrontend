import { Component, inject } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import { TaskFormStoreService } from '../services/task-form-store.service';

@Component({
  selector: 'bam-task-form-responses',
  imports: [ReactiveFormsModule, FormErrorMessageComponent],
  templateUrl: './task-form-responses.component.html',
  styleUrl: './task-form-responses.component.css',
})
export class TaskFormResponsesComponent {
  private readonly taskFormStore = inject(TaskFormStoreService);

  get form() {
    return this.taskFormStore.form;
  }

  get responses() {
    return this.taskFormStore.form.controls.responses;
  }
}
