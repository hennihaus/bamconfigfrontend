import { TaskFormResponsesComponent } from './task-form-responses.component';
import { TaskFormStoreService } from '../services/task-form-store.service';

describe('TaskFormResponsesComponent', () => {
  it('shows prefilled and valid form for synchronous task', () => {
    cy.fixture('tasks').then(([task]) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-responses /></form>',
        {
          imports: [TaskFormResponsesComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-responses-description-label]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-description-input]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-example-input]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-description-label]').each(
        (el, index) => {
          cy.wrap(el).contains(
            `Beschreibung (Statuscode ${task.responses[index].httpStatusCode})`,
          );
        },
      );
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.responses[index].description);
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.responses[index].example);
        },
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows prefilled and valid form for asynchronous task', () => {
    cy.fixture('tasks').then(([_1, _2, task]) => {
      // given
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-responses /></form>',
        {
          imports: [TaskFormResponsesComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );

      // when
      cy.wait('@isTitleUniqueRequest');

      // then
      cy.get('[data-cy=task-form-responses-description-label]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-description-input]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-example-input]').should(
        'have.length',
        task.responses.length,
      );
      cy.get('[data-cy=task-form-responses-description-label]').each((el) => {
        cy.wrap(el).should('contain.text', 'Beschreibung');
        cy.wrap(el).should('not.contain.text', 'Statuscode');
      });
      cy.get('[data-cy=task-form-responses-description-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.responses[index].description);
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').each(
        (el, index) => {
          cy.wrap(el).should('have.value', task.responses[index].example);
        },
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
    });
  });

  it('shows errors for description', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-responses /></form>',
        {
          imports: [TaskFormResponsesComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-responses-description-input]').should(
        'have.length',
        task.responses.length,
      );
    });
    cy.wait('@isTitleUniqueRequest');

    cy.get('[data-cy=task-form-responses-description-input]').each(
      (el, index) => {
        // when field is cleared
        cy.wrap(el).clear();
        // then error message is shown
        cy.get('[data-cy=task-form-responses-description-field]')
          .eq(index)
          .contains('Pflichtfeld');
        // when too long field is entered
        cy.wrap(el).type('T'.repeat(101));
        // then error message is shown
        cy.get('[data-cy=task-form-responses-description-field]')
          .eq(index)
          .contains('nicht länger');
      },
    );
  });

  it('shows errors for example', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.intercept('GET', '/v1/tasks/*/unique/title/*', { isUnique: true }).as(
        'isTitleUniqueRequest',
      );
      cy.mount(
        '<form class="ui equal width form"><bam-task-form-responses /></form>',
        {
          imports: [TaskFormResponsesComponent],
          providers: [
            {
              provide: TaskFormStoreService,
              useFactory: () => {
                const taskFormStore = new TaskFormStoreService();

                taskFormStore.setInitialTaskFormValues(task);
                taskFormStore.setTaskFormValues(task);

                return taskFormStore;
              },
            },
          ],
        },
      );
      cy.get('[data-cy=task-form-responses-example-input]').should(
        'have.length',
        task.responses.length,
      );
    });
    cy.wait('@isTitleUniqueRequest');

    cy.get('[data-cy=task-form-responses-example-input]').each((el, index) => {
      // when field is cleared
      cy.wrap(el).clear();
      // then error message is shown
      cy.get('[data-cy=task-form-responses-example-field]')
        .eq(index)
        .contains('Pflichtfeld');
      // when field is entered with no json
      cy.wrap(el).type('no json');
      // then error message is shown
      cy.get('[data-cy=task-form-responses-example-field]')
        .eq(index)
        .contains('gültiges JSON');
      // when field is entered with empty json object
      cy.wrap(el).clear();
      cy.wrap(el).type('{}');
      // then error message is shown
      cy.get('[data-cy=task-form-responses-example-field]')
        .eq(index)
        .contains('nicht leeres JSON');
      // when field is entered with empty json array
      cy.wrap(el).clear();
      cy.wrap(el).type('[]');
      // then error message is shown
      cy.get('[data-cy=task-form-responses-example-field]')
        .eq(index)
        .contains('nicht leeres JSON');
    });
  });
});
