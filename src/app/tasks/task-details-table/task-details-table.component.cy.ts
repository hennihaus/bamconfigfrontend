import { TaskDetailsTableComponent } from './task-details-table.component';
import { Parameter } from '../../shared/models/parameter';
import { Response } from '../../shared/models/response';

describe('TaskDetailsTableComponent', () => {
  it('shows placeholders and no table when parameters and responses are not available', () => {
    // given
    cy.mount(TaskDetailsTableComponent, {
      componentProperties: {
        parameters: [],
        responses: [],
      },
    });

    // then
    cy.get('[data-cy=task-details-parameters-table]').should('not.exist');
    cy.get('[data-cy=task-details-responses-table]').should('not.exist');
    cy.get('[data-cy=task-details-no-parameters-message]').should('be.visible');
    cy.get('[data-cy=task-details-no-responses-message]').should('be.visible');
  });

  it('shows table parameters and table responses with same column width in each table', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.viewport(1_000, 1_000);
      cy.mount(TaskDetailsTableComponent, {
        componentProperties: {
          parameters: task.parameters,
          responses: task.responses,
        },
      });

      // then
      cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
      cy.get('[data-cy=task-details-responses-table]').should('be.visible');
      cy.get('[data-cy=task-details-no-parameters-message]').should(
        'not.exist',
      );
      cy.get('[data-cy=task-details-no-responses-message]').should('not.exist');
      cy.get('[data-cy=task-details-table-row-parameter]').should(
        'have.length',
        task.parameters.length + 1,
      );
      cy.get('[data-cy=task-details-table-row-response]').should(
        'have.length',
        task.responses.length + 1,
      );
      cy.get('[data-cy=task-details-invisible-table-row-parameter]').should(
        'not.be.visible',
      );
      cy.get('[data-cy=task-details-invisible-table-row-response]').should(
        'not.be.visible',
      );
      cy.get('[data-cy="task-details-table-column-parameter"]').should(
        'have.length',
        4,
      );
      cy.get('[data-cy="task-details-table-column-response"]').should(
        'have.length',
        4,
      );
      cy.get('[data-cy="task-details-table-column-parameter"]').each(
        (parameterColumn, index) => {
          cy.wrap(parameterColumn)
            .invoke('css', 'width')
            .then((parameterColumnWidth) => {
              cy.get('[data-cy="task-details-table-column-response"]')
                .eq(index)
                .invoke('css', 'width')
                .should('eq', parameterColumnWidth);
            });
        },
      );
      cy.get('[data-cy=task-details-table-description-cell]').should(
        'not.contain',
        'Beschreibung nicht vorhanden',
      );
      cy.get('[data-cy=task-details-table-http-status-code-cell]').should(
        'not.contain',
        'Status nicht vorhanden',
      );
    });
  });

  it('shows table parameters and no responses when responses are not available', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskDetailsTableComponent, {
        componentProperties: {
          parameters: task.parameters,
          responses: [],
        },
      });
    });

    // then
    cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
    cy.get('[data-cy=task-details-responses-table]').should('not.exist');
    cy.get('[data-cy=task-details-no-parameters-message]').should('not.exist');
    cy.get('[data-cy=task-details-no-responses-message]').should('be.visible');
    cy.get('[data-cy=task-details-invisible-table-row-response]').should(
      'not.exist',
    );
  });

  it('shows table responses and no parameters when parameters are not available', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskDetailsTableComponent, {
        componentProperties: {
          parameters: [],
          responses: task.responses,
        },
      });
    });

    // then
    cy.get('[data-cy=task-details-parameters-table]').should('not.exist');
    cy.get('[data-cy=task-details-responses-table]').should('be.visible');
    cy.get('[data-cy=task-details-no-parameters-message]').should('be.visible');
    cy.get('[data-cy=task-details-no-responses-message]').should('not.exist');
    cy.get('[data-cy=task-details-invisible-table-row-response]').should(
      'not.exist',
    );
  });

  it('shows fallbacks for descriptions and http status code', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.mount(TaskDetailsTableComponent, {
        componentProperties: {
          parameters: task.parameters.map((parameter: Parameter) => ({
            ...parameter,
            description: '',
          })),
          responses: task.responses.map((response: Response) => ({
            ...response,
            httpStatusCode: 0,
          })),
        },
      });

      // then
      cy.get('[data-cy=task-details-table-description-cell]').should(
        'have.length',
        6,
      );
      cy.get('[data-cy=task-details-table-description-cell]').contains(
        'Beschreibung nicht vorhanden',
      );
      cy.get('[data-cy=task-details-table-http-status-code-cell]').should(
        'have.length',
        4,
      );
      cy.get('[data-cy=task-details-table-http-status-code-cell]').contains(
        'Status nicht vorhanden',
      );
    });
  });
});
