import { Component, input } from '@angular/core';
import { Parameter } from '../../shared/models/parameter';
import { Response } from '../../shared/models/response';
import { TableResponsesPipe } from '../pipes/table-responses.pipe';
import { TableParametersPipe } from '../pipes/table-parameters.pipe';

@Component({
  selector: 'bam-task-details-table',
  imports: [TableParametersPipe, TableResponsesPipe],
  templateUrl: './task-details-table.component.html',
  styleUrl: './task-details-table.component.css',
})
export class TaskDetailsTableComponent {
  readonly parameters = input.required<Parameter[]>();
  readonly responses = input.required<Response[]>();
}
