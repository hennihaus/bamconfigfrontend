import { Pipe, PipeTransform } from '@angular/core';
import { Parameter } from '../../shared/models/parameter';
import { TableParameter } from '../../shared/models/table-parameter';

@Pipe({
  name: 'tableParameters',
  standalone: true,
  pure: false,
})
export class TableParametersPipe implements PipeTransform {
  transform(parameters: Parameter[]): TableParameter[] {
    return parameters.map((parameter) => ({
      ...parameter,
      type: parameter.type.toLowerCase(),
      description: parameter.description.length
        ? parameter.description
        : $localize`Beschreibung nicht vorhanden`,
    }));
  }
}
