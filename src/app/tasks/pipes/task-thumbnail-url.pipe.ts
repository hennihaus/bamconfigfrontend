import { inject, Pipe, PipeTransform } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { ASYNC_TASK_THUMBNAIL_URL } from '../../../tokens';

@Pipe({
  name: 'taskThumbnailUrl',
  pure: false,
})
export class TaskThumbnailUrlPipe implements PipeTransform {
  private readonly asyncTaskThumbnailUrl = inject(ASYNC_TASK_THUMBNAIL_URL);

  transform(banks: Bank[]): string {
    if (!banks.length) {
      return '';
    }
    if (banks[0].isAsync) {
      return this.asyncTaskThumbnailUrl;
    }
    return banks[0].thumbnailUrl;
  }
}
