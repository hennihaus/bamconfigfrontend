import { Pipe, PipeTransform } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { Task } from '../../shared/models/task';

@Pipe({
  name: 'asyncBanks',
  pure: false,
})
export class AsyncBanksPipe implements PipeTransform {
  transform(tasks: Task[]): Bank[] {
    const asyncTask = tasks.find((task) =>
      task.banks.every((bank) => bank.isAsync),
    );

    if (asyncTask) {
      return asyncTask.banks;
    }
    return [];
  }
}
