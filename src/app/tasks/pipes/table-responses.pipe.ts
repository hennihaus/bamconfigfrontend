import { Pipe, PipeTransform } from '@angular/core';
import { Response } from '../../shared/models/response';
import { TableResponse } from '../../shared/models/table-response';

@Pipe({
  name: 'tableResponses',
  pure: false,
})
export class TableResponsesPipe implements PipeTransform {
  transform(responses: Response[]): TableResponse[] {
    return responses
      .sort((first, second) => first.httpStatusCode - second.httpStatusCode)
      .map((response) => ({
        ...response,
        httpStatusCode:
          response.httpStatusCode > 0
            ? response.httpStatusCode.toString()
            : $localize`Status nicht vorhanden`,
      }));
  }
}
