import { Pipe, PipeTransform } from '@angular/core';
import { Bank } from '../../shared/models/bank';

@Pipe({
  name: 'hasNoAsyncBanksActivated',
  pure: false,
})
export class HasNoAsyncBanksActivatedPipe implements PipeTransform {
  transform(banks: Bank[]): boolean {
    if (!banks.some((bank) => bank.isAsync)) {
      return false;
    }
    return !banks.filter((bank) => bank.isAsync).some((bank) => bank.isActive);
  }
}
