import { Component, inject, input } from '@angular/core';
import { Task } from '../../shared/models/task';
import { HasNoAsyncBanksActivatedPipe } from '../pipes/has-no-async-banks-activated.pipe';
import { TaskThumbnailUrlPipe } from '../pipes/task-thumbnail-url.pipe';
import { RouterLink } from '@angular/router';
import { TaskEndpointListComponent } from '../task-endpoint-list/task-endpoint-list.component';
import { DatePipe, NgOptimizedImage } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-task-details-view',
  imports: [
    TaskEndpointListComponent,
    RouterLink,
    DatePipe,
    TaskThumbnailUrlPipe,
    HasNoAsyncBanksActivatedPipe,
    NgOptimizedImage,
    ImageErrorPipe,
  ],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './task-details-view.component.html',
  styleUrl: './task-details-view.component.css',
})
export class TaskDetailsViewComponent {
  private readonly observer = inject(BreakpointObserver);
  public readonly imageError = inject(ImageErrorDirective);

  readonly task = input.required<Task>();

  readonly isBigScreen = toSignal(
    this.observer
      .observe(['(min-width: 1300px)'])
      .pipe(map((state) => state.matches)),
  );
}
