import { TaskDetailsViewComponent } from './task-details-view.component';
import { environment } from '../../../environments/environment';

describe('TaskDetailsViewComponent', () => {
  it('shows picture when task has valid thumbnailUrl and banks are synchronous', () => {
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        // given
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [{ ...bank, isAsync: false }],
            },
          },
        });

        // then
        cy.get('[data-cy=task-details-image]').should(
          'have.attr',
          'src',
          bank.thumbnailUrl,
        );
      });
    });
  });

  it('shows configured picture when task has valid thumbnail and banks are asynchronous', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [{ ...bank, isAsync: true }],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-details-image]').should(
      'have.attr',
      'src',
      environment.asyncTaskThumbnailUrl,
    );
  });

  it('shows an error picture when task has no banks', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskDetailsViewComponent, {
        componentProperties: {
          task: {
            ...task,
            banks: [],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-details-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows an error picture when task has invalid thumbnail', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  thumbnailUrl: 'http://localhost:4200/unknown.png',
                  isAsync: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-details-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows an error picture when task has no thumbnail', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  thumbnailUrl: '',
                  isAsync: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-details-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows no endpoints and no active banks', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              endpoints: [],
              banks: [
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=endpoint-url]').should('not.exist');
    cy.get('[data-cy=endpoint-docs-url]').should('not.exist');
    cy.get('[data-cy=task-details-active-bank-name]').should('not.exist');
    cy.get('[data-cy=task-details-no-active-banks-message]').should(
      'be.visible',
    );
  });

  it('shows endpoints and active banks', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskDetailsViewComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  isAsync: true,
                  isActive: true,
                },
                {
                  ...bank,
                  isAsync: true,
                  isActive: true,
                },
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=endpoint-url]').should('be.visible');
    cy.get('[data-cy=endpoint-docs-url]').should('be.visible');
    cy.get('[data-cy=task-details-active-bank-name]').should('have.length', 2);
    cy.get('[data-cy=task-details-no-active-banks-message]').should(
      'not.exist',
    );
  });
});
