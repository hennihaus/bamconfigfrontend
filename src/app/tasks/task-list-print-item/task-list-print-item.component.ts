import { Component, computed, input } from '@angular/core';
import { Task } from '../../shared/models/task';
import { TaskDetailsTableComponent } from '../task-details-table/task-details-table.component';
import { TaskEndpointListComponent } from '../task-endpoint-list/task-endpoint-list.component';
import { BankCreditConfigurationListComponent } from '../../banks/bank-credit-configuration-list/bank-credit-configuration-list.component';
import { TaskDetailsViewComponent } from '../task-details-view/task-details-view.component';

@Component({
  selector: 'bam-task-list-print-item',
  imports: [
    TaskDetailsViewComponent,
    BankCreditConfigurationListComponent,
    TaskEndpointListComponent,
    TaskDetailsTableComponent,
  ],
  templateUrl: './task-list-print-item.component.html',
  styleUrl: './task-list-print-item.component.css',
})
export class TaskListPrintItemComponent {
  readonly task = input.required<Task>();

  readonly banks = computed(() =>
    this.task()
      .banks.filter((bank) => bank.isActive)
      .filter((bank) => bank.creditConfiguration),
  );
}
