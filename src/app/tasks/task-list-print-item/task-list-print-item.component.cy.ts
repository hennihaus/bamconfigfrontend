import { TaskListPrintItemComponent } from './task-list-print-item.component';

describe('TaskListPrintItemComponent', () => {
  it('shows details, no credit configurations, endpoints and table for first integration step', () => {
    // given
    cy.fixture('tasks').then(([task]) => {
      cy.mount(TaskListPrintItemComponent, {
        componentProperties: {
          task,
        },
      });
    });

    // then
    cy.get('[data-cy=task-details-headline]').should('be.visible');
    cy.get('[data-cy=task-details-grid]').should('not.exist');
    cy.get('[data-cy=credit-configuration-list-item]').should('not.exist');
    cy.get('[data-cy=endpoint-url]').should('be.visible');
    cy.get('[data-cy=endpoint-docs-url]').should('be.visible');
    cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
    cy.get('[data-cy=task-details-responses-table]').should('be.visible');
  });

  it('shows details, credit configurations, endpoints and table for second integration step', () => {
    cy.fixture('tasks').then(([_, task]) => {
      // given
      cy.mount(TaskListPrintItemComponent, {
        componentProperties: {
          task,
        },
      });

      // then
      cy.get('[data-cy=task-details-headline]').should('be.visible');
      cy.get('[data-cy=task-details-grid]').should('not.exist');
      cy.get('[data-cy=credit-configuration-list-item]').should('be.visible');
      cy.get('[data-cy=credit-configuration-list-item]').should(
        'have.length',
        task.banks.length,
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should(
        'be.visible',
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should(
        'have.length',
        task.banks.length,
      );
      cy.get('[data-cy=custom-credit-configuration-title]').each(
        (el, index) => {
          cy.wrap(el).contains(task.banks[index].name);
        },
      );
      cy.get('[data-cy=credit-configuration-jms-queue]').should('be.visible');
      cy.get('[data-cy=credit-configuration-jms-queue]').should(
        'have.length',
        task.banks.length,
      );
      cy.get('[data-cy=credit-configuration-jms-queue]').each((el, index) => {
        cy.wrap(el).contains(task.banks[index].jmsQueue);
      });
      cy.get('[data-cy=endpoint-url]').should('be.visible');
      cy.get('[data-cy=endpoint-docs-url]').should('be.visible');
      cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
      cy.get('[data-cy=task-details-responses-table]').should('be.visible');
    });
  });

  it('shows details, some credit configurations, endpoints and table for third integration step', () => {
    cy.fixture('tasks').then(([_1, _2, task]) => {
      cy.fixture('bank').then((bank) => {
        // given
        cy.mount(TaskListPrintItemComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  isAsync: true,
                  isActive: true,
                },
                {
                  ...bank,
                  isAsync: true,
                  isActive: true,
                },
                {
                  ...bank,
                  isAsync: true,
                  isActive: false,
                },
              ],
            },
          },
        });

        // then
        cy.get('[data-cy=task-details-headline]').should('be.visible');
        cy.get('[data-cy=task-details-grid]').should('not.exist');
        cy.get('[data-cy=credit-configuration-list-item]').should('be.visible');
        cy.get('[data-cy=credit-configuration-list-item]').should(
          'have.length',
          2,
        );
        cy.get('[data-cy=custom-credit-configuration-title]').should(
          'be.visible',
        );
        cy.get('[data-cy=custom-credit-configuration-title]').should(
          'have.length',
          2,
        );
        cy.get('[data-cy=custom-credit-configuration-title]').each((el) => {
          cy.wrap(el).contains(bank.name);
        });
        cy.get('[data-cy=credit-configuration-jms-queue]').should('be.visible');
        cy.get('[data-cy=credit-configuration-jms-queue]').should(
          'have.length',
          2,
        );
        cy.get('[data-cy=credit-configuration-jms-queue]').each((el) => {
          cy.wrap(el).contains(bank.jmsQueue);
        });
        cy.get('[data-cy=endpoint-url]').should('be.visible');
        cy.get('[data-cy=endpoint-docs-url]').should('be.visible');
        cy.get('[data-cy=task-details-parameters-table]').should('be.visible');
        cy.get('[data-cy=task-details-responses-table]').should('be.visible');
      });
    });
  });

  it('shows details, no credit configurations, no endpoints and no table when properties are empty', () => {
    cy.fixture('task').then((task) => {
      // given
      cy.mount(TaskListPrintItemComponent, {
        componentProperties: {
          task: {
            ...task,
            banks: [],
            endpoints: [],
            parameters: [],
            responses: [],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-details-headline]').should('be.visible');
    cy.get('[data-cy=task-details-grid]').should('not.exist');
    cy.get('[data-cy=credit-configuration-list-item]').should('not.exist');
    cy.get('[data-cy=endpoint-url]').should('not.exist');
    cy.get('[data-cy=endpoint-docs-url]').should('not.exist');
    cy.get('[data-cy=task-details-parameters-table]').should('not.exist');
    cy.get('[data-cy=task-details-responses-table]').should('not.exist');
  });
});
