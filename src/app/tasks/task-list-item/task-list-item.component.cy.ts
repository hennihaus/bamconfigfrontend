import { TaskListItemComponent } from './task-list-item.component';
import { environment } from '../../../environments/environment';

describe('TaskListItemComponent', () => {
  it('shows picture when task has valid thumbnail and banks are synchronous', () => {
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        // given
        cy.mount(TaskListItemComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [{ ...bank, isAsync: false }],
            },
          },
        });

        // then
        cy.get('[data-cy=task-list-item-image]').should(
          'have.attr',
          'src',
          bank.thumbnailUrl,
        );
      });
    });
  });

  it('shows configured picture when task has valid thumbnail and banks are asynchronous', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskListItemComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [{ ...bank, isAsync: true }],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-list-item-image]').should(
      'have.attr',
      'src',
      environment.asyncTaskThumbnailUrl,
    );
  });

  it('shows an error picture when task has no banks', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskListItemComponent, {
        componentProperties: {
          task: {
            ...task,
            banks: [],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-list-item-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows an error picture when task has invalid thumbnail', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskListItemComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  thumbnailUrl: 'http://localhost:4200/unknown.png',
                  isAsync: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-list-item-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows an error picture when task has no thumbnail', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.fixture('bank').then((bank) => {
        cy.mount(TaskListItemComponent, {
          componentProperties: {
            task: {
              ...task,
              banks: [
                {
                  ...bank,
                  thumbnailUrl: '',
                  isAsync: false,
                },
              ],
            },
          },
        });
      });
    });

    // then
    cy.get('[data-cy=task-list-item-image]').should(
      'have.attr',
      'src',
      '/placeholder.png',
    );
  });

  it('shows correct text when task has one endpoint', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskListItemComponent, {
        componentProperties: {
          task: {
            ...task,
            endpoints: [
              {
                uuid: '08191f25-c0c0-43ac-8244-217965006118',
                type: 'REST',
                url: '',
                docsUrl: 'https://hennihaus.github.io/bambankrest',
              },
            ],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-list-item-endpoints]').contains('REST');
    cy.get('[data-cy=task-list-item-endpoints]').should('not.contain', 'REST,');
  });

  it('shows correct text when task has two endpoints', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskListItemComponent, {
        componentProperties: {
          task: {
            ...task,
            endpoints: [
              {
                uuid: '08191f25-c0c0-43ac-8244-217965006118',
                type: 'REST',
                url: '',
                docsUrl: 'https://hennihaus.github.io/bambankrest',
              },
              {
                uuid: 'ea0f187c-044d-40ba-8382-65e09b4bdb00',
                type: 'GRPC',
                url: '',
                docsUrl: 'https://hennihaus.github.io/bambankrest',
              },
            ],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-list-item-endpoints]').contains('REST, GRPC');
    cy.get('[data-cy=task-list-item-endpoints]').should('not.contain', 'GRPC,');
  });

  it('shows correct text when task has no endpoints', () => {
    // given
    cy.fixture('task').then((task) => {
      cy.mount(TaskListItemComponent, {
        componentProperties: {
          task: {
            ...task,
            endpoints: [],
          },
        },
      });
    });

    // then
    cy.get('[data-cy=task-list-item-endpoints]').contains('Keine Endpunkte');
  });
});
