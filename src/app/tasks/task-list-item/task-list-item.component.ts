import { Component, inject, input } from '@angular/core';
import { Task } from '../../shared/models/task';
import { TaskThumbnailUrlPipe } from '../pipes/task-thumbnail-url.pipe';
import { NgOptimizedImage } from '@angular/common';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';

@Component({
  selector: 'bam-task-list-item',
  imports: [TaskThumbnailUrlPipe, NgOptimizedImage, ImageErrorPipe],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './task-list-item.component.html',
  styleUrl: './task-list-item.component.css',
})
export class TaskListItemComponent {
  readonly imageError = inject(ImageErrorDirective);

  readonly task = input.required<Task>();
}
