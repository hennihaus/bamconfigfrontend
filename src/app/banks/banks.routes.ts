import { Routes } from '@angular/router';
import { BankListComponent } from './bank-list/bank-list.component';
import { BankDetailsComponent } from './bank-details/bank-details.component';
import { BankEditComponent } from './bank-edit/bank-edit.component';
import { leaveFormGuard } from '../shared/guards/leave-form.guard';

export const BANKS_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: BankListComponent,
    title: $localize`Banken`,
  },
  {
    path: 'list/:pageNumber',
    component: BankListComponent,
    title: $localize`Banken`,
  },
  {
    path: 'details',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'details/:uuid',
    component: BankDetailsComponent,
    title: $localize`Bank`,
  },
  {
    path: 'edit',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'edit/:uuid',
    component: BankEditComponent,
    title: $localize`Bank bearbeiten`,
    canDeactivate: [leaveFormGuard],
  },
];
