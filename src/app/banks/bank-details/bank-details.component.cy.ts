import { BankDetailsComponent } from './bank-details.component';

describe('BankDetailsComponent', () => {
  it('shows bank details with options on init', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: bank,
        delay: 250,
      }).as('getBankRequest');
      cy.mount(BankDetailsComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });

      // when
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait(['@getBankRequest']);

      // then
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=bank-details-headline]').should('be.visible');
      cy.get('[data-cy=bank-details-edit-link]').should('be.visible');
    });
  });

  it('shows error message with internal server error on init', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {},
        statusCode: 500,
      }).as('getBankRequest');
      cy.mount(BankDetailsComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });

      // when
      cy.wait(['@getBankRequest']);

      // then
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
      cy.get('[data-cy=bank-details-headline]').should('not.exist');
      cy.get('[data-cy=bank-details-edit-link]').should('not.exist');
    });
  });

  it('updates bank status in view when activate and deactivate button is clicked', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: { ...bank, isAsync: true, isActive: true },
      }).as('getBankRequest');
      cy.mount(BankDetailsComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });
      cy.wait(['@getBankRequest']);

      // when bank is deactivated in bank details options
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: { ...bank, isAsync: true, isActive: false },
      }).as('updateBankRequest');
      cy.get('[data-cy=bank-details-deactivate-button]').click();
      // then it should update bank active status in bank details view and bank details options
      cy.wait('@updateBankRequest');
      cy.get('[data-cy=bank-details-active-status-text]').contains(
        'Deaktiviert',
      );
      cy.get('[data-cy=bank-details-activate-button]').should('be.visible');
      cy.get('[data-cy=bank-details-deactivate-button]').should('not.exist');

      // when bank is activated in bank details options
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: { ...bank, isAsync: true, isActive: true },
      }).as('updateBankRequest');
      cy.get('[data-cy=bank-details-activate-button]').click();
      // then it should update bank active status in bank details view and bank details options
      cy.wait('@updateBankRequest');
      cy.get('[data-cy=bank-details-active-status-text]').contains('Aktiviert');
      cy.get('[data-cy=bank-details-activate-button]').should('not.exist');
      cy.get('[data-cy=bank-details-deactivate-button]').should('be.visible');
    });
  });
});
