import { Component, inject, input } from '@angular/core';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { Bank } from '../../shared/models/bank';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { BankDetailsOptionsComponent } from '../bank-details-options/bank-details-options.component';
import { BankDetailsViewComponent } from '../bank-details-view/bank-details-view.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-bank-details',
  imports: [
    BankDetailsViewComponent,
    BankDetailsOptionsComponent,
    MessageComponent,
    LoadingComponent,
  ],
  templateUrl: './bank-details.component.html',
  styleUrl: './bank-details.component.css',
})
export class BankDetailsComponent {
  private readonly bankStore = inject(BankStoreService);

  readonly uuid = input.required<string>();

  readonly bankResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.bankStore.getSingle(uuid),
  });

  updateBank(bank: Bank) {
    this.bankResource.set(bank);
  }
}
