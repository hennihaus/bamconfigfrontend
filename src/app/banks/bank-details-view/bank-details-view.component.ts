import { Component, inject, input } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { AsyncStatusPipe } from '../pipes/async-status.pipe';
import { ActiveStatusPipe } from '../pipes/active-status.pipe';
import { BankCreditConfigurationListItemComponent } from '../bank-credit-configuration-list-item/bank-credit-configuration-list-item.component';
import { DatePipe, NgOptimizedImage, TitleCasePipe } from '@angular/common';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';

@Component({
  selector: 'bam-bank-details-view',
  imports: [
    BankCreditConfigurationListItemComponent,
    TitleCasePipe,
    DatePipe,
    ActiveStatusPipe,
    AsyncStatusPipe,
    NgOptimizedImage,
    ImageErrorPipe,
  ],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './bank-details-view.component.html',
  styleUrl: './bank-details-view.component.css',
})
export class BankDetailsViewComponent {
  readonly imageError = inject(ImageErrorDirective);

  readonly bank = input.required<Bank>();
}
