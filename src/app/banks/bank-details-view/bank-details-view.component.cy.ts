import { BankDetailsViewComponent } from './bank-details-view.component';

describe('BankDetailsViewComponent', () => {
  it('shows correct active status when bank is active', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isActive: true,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-active-status-text]').contains('Aktiviert');
    });
  });

  it('shows correct active status when bank is inactive', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isActive: false,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-active-status-text]').contains(
        'Deaktiviert',
      );
    });
  });

  it('shows picture when bank has valid thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank,
        },
      });

      // then
      cy.get('[data-cy=bank-details-image]').should(
        'have.attr',
        'src',
        bank.thumbnailUrl,
      );
    });
  });

  it('shows an error picture when bank has invalid thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            thumbnailUrl: 'http://localhost:4200/unknown.png',
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-image]').should(
        'have.attr',
        'src',
        '/placeholder.png',
      );
    });
  });

  it('shows an error picture when bank has no thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            thumbnailUrl: undefined,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-image]').should(
        'have.attr',
        'src',
        '/placeholder.png',
      );
    });
  });

  it('shows correct async status when bank is synchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: false,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-jms-queue]').should('not.exist');
      cy.get('[data-cy=bank-details-interface]').contains('REST/GRPC');
      cy.get('[data-cy=bank-details-teams]').contains('alle');
    });
  });

  it('shows correct async status when bank is asynchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-jms-queue]').should('be.visible');
      cy.get('[data-cy=bank-details-interface]').contains('JMS');
      cy.get('[data-cy=bank-details-teams]').contains('verschiedene');
    });
  });

  it('shows credit configuration details when bank has configs', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank,
        },
      });

      // then
      cy.get('[data-cy=default-credit-configuration-title]').should(
        'be.visible',
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should('not.exist');
      cy.get('[data-cy=credit-configuration-jms-queue]').should('not.exist');
      cy.get('[data-cy=credit-configuration-min-term-text]').should(
        'be.visible',
      );
      cy.get('[data-cy=credit-configuration-max-term-text]').should(
        'be.visible',
      );
    });
  });

  it('shows no credit configuration details when bank has no configs', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsViewComponent, {
        componentProperties: {
          bank: {
            ...bank,
            creditConfiguration: undefined,
          },
        },
      });

      // then
      cy.get('[data-cy=default-credit-configuration-title]').should(
        'not.exist',
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should('not.exist');
      cy.get('[data-cy=credit-configuration-jms-queue]').should('not.exist');
      cy.get('[data-cy=credit-configuration-min-term-text]').should(
        'not.exist',
      );
      cy.get('[data-cy=credit-configuration-max-term-text]').should(
        'not.exist',
      );
    });
  });
});
