import { BankListItemComponent } from './bank-list-item.component';

describe('BankListItemComponent', () => {
  it('shows picture when bank has valid thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank,
        },
      });

      // then
      cy.get('[data-cy=bank-list-item-image]').should(
        'have.attr',
        'src',
        bank.thumbnailUrl,
      );
    });
  });

  it('shows an error picture when bank has invalid thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, thumbnailUrl: 'http://localhost:4200/unknown.png' },
        },
      });

      // then
      cy.get('[data-cy=bank-list-item-image]').should(
        'have.attr',
        'src',
        '/placeholder.png',
      );
    });
  });

  it('shows an error picture when bank has no thumbnail', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, thumbnailUrl: '' },
        },
      });

      // then
      cy.get('[data-cy=bank-list-item-image]').should(
        'have.attr',
        'src',
        '/placeholder.png',
      );
    });
  });

  it('shows correct active status when bank is active', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, isActive: true },
        },
      });

      // then
      cy.get('[data-cy=bank-list-item-description]').contains('aktiviert');
    });
  });

  it('shows correct active status when bank is inactive', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, isActive: false },
        },
      });

      // then
      cy.get('[data-cy=bank-list-item-description]').contains('deaktiviert');
    });
  });

  it('shows correct async status when bank is synchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, isAsync: false },
        },
      });
    });

    // then
    cy.get('[data-cy=bank-list-item-metadata]').contains('REST/GRPC');
    cy.get('[data-cy=bank-list-item-metadata]').contains('Alle Teams');
  });

  it('shows correct async status when bank is asynchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankListItemComponent, {
        componentProperties: {
          bank: { ...bank, isAsync: true },
        },
      });
    });

    // then
    cy.get('[data-cy=bank-list-item-metadata]').contains('JMS');
    cy.get('[data-cy=bank-list-item-metadata]').contains('Verschiedene Teams');
  });
});
