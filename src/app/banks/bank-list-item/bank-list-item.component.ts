import { Component, inject, input } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { AsyncStatusPipe } from '../pipes/async-status.pipe';
import { ActiveStatusPipe } from '../pipes/active-status.pipe';
import { NgOptimizedImage } from '@angular/common';
import { ImageErrorPipe } from '../../shared/pipes/image-error.pipe';
import { ImageErrorDirective } from '../../shared/directives/image-error.directive';

@Component({
  selector: 'bam-bank-list-item',
  imports: [
    ActiveStatusPipe,
    AsyncStatusPipe,
    NgOptimizedImage,
    ImageErrorPipe,
  ],
  hostDirectives: [ImageErrorDirective],
  templateUrl: './bank-list-item.component.html',
  styleUrl: './bank-list-item.component.css',
})
export class BankListItemComponent {
  readonly imageError = inject(ImageErrorDirective);

  readonly bank = input.required<Bank>();
}
