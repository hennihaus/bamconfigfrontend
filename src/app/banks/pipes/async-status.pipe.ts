import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'asyncStatus',
  pure: false,
})
export class AsyncStatusPipe implements PipeTransform {
  transform(isAsync: boolean): string {
    if (isAsync) {
      return 'JMS';
    }
    return 'REST/GRPC';
  }
}
