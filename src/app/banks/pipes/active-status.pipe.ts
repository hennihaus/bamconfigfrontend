import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'activeStatus',
  pure: false,
})
export class ActiveStatusPipe implements PipeTransform {
  transform(isActive: boolean): string {
    if (isActive) {
      return $localize`aktiviert`;
    }
    return $localize`deaktiviert`;
  }
}
