import { isFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const amountInEurosValidator: ValidatorFn = (
  control,
): ValidationErrors | null => {
  if (!isFormGroup(control)) {
    return null;
  }
  const minAmountInEuros = control.get('minAmountInEuros')?.value;
  const maxAmountInEuros = control.get('maxAmountInEuros')?.value;

  if (
    !isRegularNumber(minAmountInEuros) ||
    !isRegularNumber(maxAmountInEuros) ||
    +minAmountInEuros <= +maxAmountInEuros
  ) {
    return null;
  }
  return {
    amountineuros: true,
  };
};

const isRegularNumber = (number: number): boolean => {
  if (!number) {
    return false;
  }
  if (!Number.isSafeInteger(+number)) {
    return false;
  }
  return +number >= 0;
};
