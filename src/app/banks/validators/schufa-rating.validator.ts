import { isFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const schufaRatingValidator: ValidatorFn = (
  control,
): ValidationErrors | null => {
  if (!isFormGroup(control)) {
    return null;
  }
  const minSchufaRating = control.get('minSchufaRating')?.value;
  const maxSchufaRating = control.get('maxSchufaRating')?.value;

  if (!minSchufaRating || !maxSchufaRating) {
    return null;
  }
  if (minSchufaRating.charCodeAt(0) <= maxSchufaRating.charCodeAt(0)) {
    return null;
  }
  return {
    schufarating: true,
  };
};
