import { isFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export const termInMonthsValidator: ValidatorFn = (
  control,
): ValidationErrors | null => {
  if (!isFormGroup(control)) {
    return null;
  }
  const minTermInMonths = control.get('minTermInMonths')?.value;
  const maxTermInMonths = control.get('maxTermInMonths')?.value;

  if (
    !isRegularNumber(minTermInMonths) ||
    !isRegularNumber(maxTermInMonths) ||
    +minTermInMonths <= +maxTermInMonths
  ) {
    return null;
  }
  return {
    terminmonths: true,
  };
};

const isRegularNumber = (number: number): boolean => {
  if (!number) {
    return false;
  }
  if (!Number.isSafeInteger(+number)) {
    return false;
  }
  return +number >= 0;
};
