import {
  Component,
  effect,
  inject,
  input,
  linkedSignal,
  ResourceStatus,
  signal,
} from '@angular/core';
import { catchError, EMPTY, finalize, tap } from 'rxjs';
import { Bank } from '../../shared/models/bank';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { Router } from '@angular/router';
import { LeaveFormGuard } from '../../shared/guards/leave-form.guard';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { BankFormComponent } from '../bank-form/bank-form.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-bank-edit',
  imports: [MessageComponent, BankFormComponent, LoadingComponent],
  templateUrl: './bank-edit.component.html',
  styleUrl: './bank-edit.component.css',
})
export class BankEditComponent implements LeaveFormGuard {
  private readonly bankStore = inject(BankStoreService);
  private readonly router = inject(Router);

  readonly uuid = input.required<string>();

  readonly bankResource = rxResource({
    request: () => this.uuid(),
    loader: ({ request: uuid }) => this.bankStore.getSingle(uuid),
  });
  readonly isLoading = linkedSignal(() => this.bankResource.isLoading());
  readonly error = linkedSignal(() => this.bankResource.error());

  constructor() {
    effect(() => {
      if (this.bankResource.status() === ResourceStatus.Resolved) {
        this.hasUnsavedChanges.set(false);
      }
    });
  }

  readonly hasUnsavedChanges = signal(false);

  setHasUnsavedChanges(hasUnsavedChanges: boolean) {
    this.hasUnsavedChanges.set(hasUnsavedChanges);
  }

  updateBank(bank: Bank) {
    this.isLoading.set(true);
    this.error.set(null);
    this.bankStore
      .update(bank)
      .pipe(
        tap(() => this.hasUnsavedChanges.set(false)),
        tap({
          next: () => this.error.set(null),
          error: (error) => this.error.set(error),
        }),
        catchError(() => EMPTY),
        finalize(() => this.isLoading.set(false)),
      )
      .subscribe((bank) => this.router.navigate(['/banks/details', bank.uuid]));
  }
}
