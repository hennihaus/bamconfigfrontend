import { BankEditComponent } from './bank-edit.component';

describe('BankEditComponent', () => {
  it('updates bank when bank form is submitted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      cy.clock(now);
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {
          ...bank,
          isActive: true,
        },
        delay: 250, // milliseconds
      }).as('getBankRequest');
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: bank,
        delay: 250, // milliseconds
      }).as('updateBankRequest');
      cy.mount(BankEditComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });

      // when component is loaded
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait(['@getBankRequest']);
      // then it shows bank edit form
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=bank-edit-headline]').should('be.visible');
      cy.get('[data-cy=bank-form]').should('be.visible');

      // when bank form is submitted
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type(
        'https://example.com/thubmnail.jpg',
      );
      cy.get('[data-cy=bank-form-submit-button]').click();
      // then it updates bank
      cy.get('[data-cy=bank-form-submit-button]').should('be.disabled');
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@updateBankRequest')
        .its('request.body')
        .should('deep.equal', {
          ...bank,
          thumbnailUrl: 'https://example.com/thubmnail.jpg',
          updatedAt: '2024-05-24T12:00:00',
        });
      cy.get('[data-cy=loading-overlay]').should('not.exist');
    });
  });

  it('shows error message and bank form when submit responds with internal server error', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: bank,
      }).as('getBankRequest');
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('updateBankRequest');
      cy.mount(BankEditComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });
      cy.wait(['@getBankRequest']);

      // when
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('1');
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.wait('@updateBankRequest');
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').should(
        'have.value',
        '1',
      );
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows error message with internal server error on page load', () => {
    // given
    cy.fixture('bank').then((bank) => {
      cy.intercept('GET', `/v1/banks/${bank.uuid}`, {
        body: {},
        statusCode: 500,
      }).as('getBankRequest');

      cy.mount(BankEditComponent, {
        componentProperties: {
          uuid: bank.uuid,
        },
      });
    });

    // when
    cy.wait(['@getBankRequest']);

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=bank-edit-headline]').should('not.exist');
    cy.get('[data-cy=bank-form]').should('not.exist');
  });
});
