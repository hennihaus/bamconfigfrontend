import { BankDetailsOptionsComponent } from './bank-details-options.component';
import { createOutputSpy } from 'cypress/angular';
import { Bank } from '../../shared/models/bank';

describe('BankDetailsOptionsComponent', () => {
  it('shows one button when bank is synchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: false,
            isActive: false,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-deactivate-button]').should('not.exist');
      cy.get('[data-cy=bank-details-activate-button]').should('not.exist');
      cy.get('[data-cy=bank-details-edit-link]').should('be.visible');
      cy.get('[data-cy=bank-details-add-teams-button]').should('not.exist');
      cy.get('[data-cy=bank-details-remove-teams-button]').should('not.exist');
    });
  });

  it('shows all buttons when bank is asynchronous', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
            isActive: false,
          },
        },
      });

      // then
      cy.get('[data-cy=bank-details-activate-button]').should('be.visible');
      cy.get('[data-cy=bank-details-edit-link]').should('be.visible');
      cy.get('[data-cy=bank-details-add-teams-button]').should('be.visible');
      cy.get('[data-cy=bank-details-remove-teams-button]').should('be.visible');
    });
  });

  it('should be possible to activate bank', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      cy.clock(now);
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
            isActive: false,
          },
          bankChange: createOutputSpy<Bank>('bankChangeSpy'),
        },
      });
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: { ...bank, isActive: true },
        delay: 45, // milliseconds
      }).as('updateBankRequest');

      // when
      cy.get('[data-cy=bank-details-activate-button]').click();

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@updateBankRequest')
        .its('request.body')
        .should('deep.equal', {
          ...bank,
          isActive: true,
          isAsync: true,
          updatedAt: '2024-05-24T12:00:00',
        });
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('@bankChangeSpy').should('have.been.calledOnceWithExactly', {
        ...bank,
        isActive: true,
      });
    });
  });

  it('should be possible to deactivate bank', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      cy.clock(now);
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
            isActive: true,
          },
          bankChange: createOutputSpy<Bank>('bankChangeSpy'),
        },
      });
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: { ...bank, isActive: false },
      }).as('updateBankRequest');

      // when
      cy.get('[data-cy=bank-details-deactivate-button]').click();

      // then
      cy.wait('@updateBankRequest')
        .its('request.body')
        .should('deep.equal', {
          ...bank,
          isActive: false,
          isAsync: true,
          updatedAt: '2024-05-24T12:00:00',
        });
      cy.get('@bankChangeSpy').should('have.been.calledOnceWithExactly', {
        ...bank,
        isActive: false,
      });
    });
  });

  it('shows error message when activate bank request responds with internal server error', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
            isActive: false,
          },
          bankChange: createOutputSpy<Bank>('bankChangeSpy'),
        },
      });
      cy.intercept('PATCH', `/v1/banks/${bank.uuid}`, {
        body: {},
        statusCode: 500,
      }).as('updateBankRequest');

      // when
      cy.get('[data-cy=bank-details-activate-button]').click();

      // then
      cy.wait('@updateBankRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('@bankChangeSpy').should('not.have.been.called');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows success message when statistics are added', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
          },
        },
      });
      cy.intercept(
        'PUT',
        `/v1/statistics/${bank.uuid}`,
        (req) => req.reply({ statusCode: 204, body: '', delay: 45 }), // milliseconds
      ).as('saveStatisticsRequest');

      // when
      cy.get('[data-cy=bank-details-add-teams-button]').click();

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@saveStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(44, 102, 45)');
    });
  });

  it('shows error message when statistics add request responds with internal server error', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
          },
        },
      });
      cy.intercept('PUT', `/v1/statistics/${bank.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('saveStatisticsRequest');

      // when
      cy.get('[data-cy=bank-details-add-teams-button]').click();

      // then
      cy.wait('@saveStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });

  it('shows success message when statistics are removed', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
          },
        },
      });
      cy.intercept(
        'DELETE',
        `/v1/statistics/${bank.uuid}`,
        (req) => req.reply({ statusCode: 204, body: '', delay: 45 }), // milliseconds
      ).as('deleteStatisticsRequest');

      // when
      cy.get('[data-cy=bank-details-remove-teams-button]').click();

      // then
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@deleteStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(44, 102, 45)');
    });
  });

  it('shows error message when statistics remove request responds with internal server error', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankDetailsOptionsComponent, {
        componentProperties: {
          bank: {
            ...bank,
            isAsync: true,
          },
        },
      });
      cy.intercept('DELETE', `/v1/statistics/${bank.uuid}`, {
        statusCode: 500,
        body: {},
      }).as('deleteStatisticsRequest');

      // when
      cy.get('[data-cy=bank-details-remove-teams-button]').click();

      // then
      cy.wait('@deleteStatisticsRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');
      cy.get('[data-cy=message]')
        .should('have.css', 'color')
        .and('eq', 'rgb(159, 58, 56)');
    });
  });
});
