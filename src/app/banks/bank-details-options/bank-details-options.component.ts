import { Component, inject, input, output, signal } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { catchError, map, tap } from 'rxjs';
import { Message } from '../../shared/models/message';
import { StatisticStoreService } from '../../shared/services/statistic-store.service';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { RouterLink } from '@angular/router';
import { ErrorHandlerService } from '../../shared/services/error-handler.service';

@Component({
  selector: 'bam-bank-details-options',
  imports: [RouterLink, MessageComponent, LoadingComponent],
  templateUrl: './bank-details-options.component.html',
  styleUrl: './bank-details-options.component.css',
})
export class BankDetailsOptionsComponent {
  private readonly bankStore = inject(BankStoreService);
  private readonly statisticStore = inject(StatisticStoreService);
  private readonly errorHandler = inject(ErrorHandlerService);

  // TODO: migrate to model when cypress component testing is able to deal with it
  readonly bank = input.required<Bank>();
  readonly bankChange = output<Bank>();

  readonly isLoading = signal(false);
  readonly message = signal<Message | null>(null);

  onActivateClick(isActive: boolean) {
    this.isLoading.set(true);
    this.message.set(null);
    this.bankStore
      .update({ ...this.bank(), isActive, updatedAt: new Date() })
      .pipe(
        catchError(() => this.errorHandler.getServerErrorMessageAsync()),
        tap(() => this.isLoading.set(false)),
      )
      .subscribe((response) => {
        if ('uuid' in response) {
          this.bankChange.emit(response);
        } else {
          this.message.set(response);
        }
      });
  }

  onSaveStatistics() {
    this.isLoading.set(true);
    this.message.set(null);
    this.statisticStore
      .saveStatistics(this.bank().uuid)
      .pipe(
        map(() => ({
          text: $localize`Alle Teams erfolgreich hinzugefügt`,
          type: 'positive' as const,
        })),
        catchError(() => this.errorHandler.getServerErrorMessageAsync()),
        tap(() => this.isLoading.set(false)),
      )
      .subscribe((message: Message) => this.message.set(message));
  }

  onDeleteStatistics() {
    this.isLoading.set(true);
    this.message.set(null);
    this.statisticStore
      .deleteStatistics(this.bank().uuid)
      .pipe(
        map(() => ({
          text: $localize`Alle Teams erfolgreich entfernt`,
          type: 'positive' as const,
        })),
        catchError(() => this.errorHandler.getServerErrorMessageAsync()),
        tap(() => this.isLoading.set(false)),
      )
      .subscribe((message: Message) => this.message.set(message));
  }
}
