import { BankListComponent } from './bank-list.component';

describe('BankListComponent', () => {
  it('shows six banks and pagination with two numbers when banks are provided', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.intercept('GET', '/v1/banks', (req) =>
        req.reply({
          body: [...banks, ...banks],
          delay: 45, // milliseconds
        }),
      ).as('getBanksRequest');
      cy.mount(BankListComponent);

      // when
      cy.get('[data-cy=loading-overlay]').should('be.visible');
      cy.wait('@getBanksRequest');
      cy.get('[data-cy=loading-overlay]').should('not.exist');

      // then
      cy.get('[data-cy=bank-list-item]').should('have.length', 3);
      cy.get('[data-cy=frontend-pagination-item]').should('have.length', 2);
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(0)
        .should('have.class', 'active');
      cy.get('[data-cy=frontend-pagination-item]')
        .eq(1)
        .should('not.have.class', 'active');
    });
  });

  it('shows an error message and no pagination when no banks are provided', () => {
    // given
    cy.intercept('GET', '/v1/banks', { body: [] }).as('getBanksRequest');
    cy.mount(BankListComponent);

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=frontend-pagination-menu]').should('not.exist');
  });

  it('shows an error message and no pagination when an internal server error occurs', () => {
    // given
    cy.intercept('GET', '/v1/banks', { body: {}, statusCode: 500 }).as(
      'getBanksRequest',
    );
    cy.mount(BankListComponent);

    // when
    cy.wait('@getBanksRequest');

    // then
    cy.get('[data-cy=message]')
      .should('have.css', 'color')
      .and('eq', 'rgb(159, 58, 56)');
    cy.get('[data-cy=frontend-pagination-menu]').should('not.exist');
  });
});
