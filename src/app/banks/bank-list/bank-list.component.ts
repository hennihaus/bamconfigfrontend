import { Component, inject, input } from '@angular/core';
import { BankStoreService } from '../../shared/services/bank-store.service';
import { Message } from '../../shared/models/message';
import { Router, RouterLink } from '@angular/router';
import { LoadingComponent } from '../../shared/components/loading/loading.component';
import { MessageComponent } from '../../shared/components/message/message.component';
import { BankListItemComponent } from '../bank-list-item/bank-list-item.component';
import { FrontendPaginationComponent } from '../../shared/components/frontend-pagination/frontend-pagination.component';
import { rxResource } from '@angular/core/rxjs-interop';

@Component({
  selector: 'bam-bank-list',
  imports: [
    FrontendPaginationComponent,
    BankListItemComponent,
    RouterLink,
    MessageComponent,
    LoadingComponent,
  ],
  templateUrl: './bank-list.component.html',
  styleUrl: './bank-list.component.css',
})
export class BankListComponent {
  private readonly bankStore = inject(BankStoreService);
  private readonly router = inject(Router);

  readonly pageNumber = input(1, {
    transform: (value?: string) => (value ? +value : 1),
  });

  readonly banksResource = rxResource({
    loader: () => this.bankStore.getAll(),
  });

  navigateToPageNumber(pageNumber: number) {
    this.router.navigate(['/banks/list', pageNumber]);
  }

  get notFoundMessage(): Message {
    return { text: $localize`Keine Banken gefunden`, type: 'negative' };
  }
}
