import { BankCreditConfigurationListComponent } from './bank-credit-configuration-list.component';

describe('BankCreditConfigurationListComponent', () => {
  it('shows two credit configurations with custom title and jmsQueue', () => {
    cy.fixture('banks').then((banks) => {
      // given
      cy.mount(BankCreditConfigurationListComponent, {
        componentProperties: {
          banks,
        },
      });
      cy.wrap(banks).should('have.length', 3);

      // then
      cy.get('[data-cy=default-credit-configuration-title]').should(
        'not.exist',
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should(
        'have.length',
        2,
      );
      cy.get('[data-cy=custom-credit-configuration-title]').each(
        (title, index) => {
          cy.wrap(title).should('contain.text', banks[index + 1].name);
        },
      );
      cy.get('[data-cy=credit-configuration-jms-queue]').should(
        'have.length',
        2,
      );
      cy.get('[data-cy=credit-configuration-jms-queue]').each(
        (jmsQueue, index) => {
          cy.wrap(jmsQueue).should('contain.text', banks[index + 1].jmsQueue);
        },
      );
    });
  });
});
