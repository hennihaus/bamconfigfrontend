import { Component, input } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import { BankCreditConfigurationListItemComponent } from '../bank-credit-configuration-list-item/bank-credit-configuration-list-item.component';

@Component({
  selector: 'bam-bank-credit-configuration-list',
  imports: [BankCreditConfigurationListItemComponent],
  templateUrl: './bank-credit-configuration-list.component.html',
  styleUrl: './bank-credit-configuration-list.component.css',
})
export class BankCreditConfigurationListComponent {
  readonly banks = input.required<Bank[]>();
}
