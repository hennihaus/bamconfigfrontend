import { BankFormComponent } from './bank-form.component';
import { createOutputSpy } from 'cypress/angular';
import { Bank } from '../../shared/models/bank';
import { RatingLevel } from '../../shared/models/rating-level';

describe('BankFormComponent', () => {
  it('shows min and invalid form when team is not set', () => {
    // given
    cy.mount(BankFormComponent, {
      componentProperties: {
        isSubmitting: false,
        bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
      },
    });

    // then all fields are prefilled right and errors are shown
    cy.get('[data-cy=bank-form-is-active-select]').should(
      'have.value',
      `0: ${true}`,
    );
    cy.get('[data-cy=bank-form-thumbnail-url-input]').should('have.value', '');
    cy.get('[data-cy=bank-form-thumbnail-url-field]').contains('Pflichtfeld');
    cy.get('[data-cy=bank-form-min-amount-in-euros-input]').should(
      'have.value',
      0,
    );
    cy.get('[data-cy=bank-form-max-amount-in-euros-input]').should(
      'have.value',
      0,
    );
    cy.get('[data-cy=bank-form-min-term-in-months-input]').should(
      'have.value',
      0,
    );
    cy.get('[data-cy=bank-form-max-term-in-months-input]').should(
      'have.value',
      0,
    );
    cy.get('[data-cy=bank-form-min-schufa-rating-select]').should(
      'have.value',
      RatingLevel.A,
    );
    cy.get('[data-cy=bank-form-max-schufa-rating-select]').should(
      'have.value',
      RatingLevel.P,
    );

    // when submit button is clicked
    cy.get('[data-cy=bank-form-submit-button]').click();
    // then bankSubmitSpy is not called
    cy.get('@bankSubmitSpy').should('not.have.been.called');
    cy.get('@hasUnsavedChangesSpy').should('not.have.been.called');
  });

  it('shows prefilled and valid form for asynchronous banks', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      const prefilledBank = {
        ...bank,
        isAsync: true,
        creditConfiguration: {
          ...bank.creditConfiguration,
        },
        updatedAt: now,
      };
      cy.clock(now);
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: prefilledBank,
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
          hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
        },
      });

      // then all fields are prefilled right and no errors are shown
      cy.get('[data-cy=bank-form-is-active-select]').should(
        'have.value',
        `0: ${prefilledBank.isActive}`,
      );
      cy.get('[data-cy=bank-form-thumbnail-url-input]').should(
        'have.value',
        prefilledBank.thumbnailUrl,
      );
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').should(
        'have.value',
        prefilledBank.creditConfiguration.minAmountInEuros,
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').should(
        'have.value',
        prefilledBank.creditConfiguration.maxAmountInEuros,
      );
      cy.get('[data-cy=bank-form-min-term-in-months-input]').should(
        'have.value',
        prefilledBank.creditConfiguration.minTermInMonths,
      );
      cy.get('[data-cy=bank-form-max-term-in-months-input]').should(
        'have.value',
        prefilledBank.creditConfiguration.maxTermInMonths,
      );
      cy.get('[data-cy=bank-form-min-schufa-rating-select]').should(
        'have.value',
        prefilledBank.creditConfiguration.minSchufaRating,
      );
      cy.get('[data-cy=bank-form-max-schufa-rating-select]').should(
        'have.value',
        prefilledBank.creditConfiguration.maxSchufaRating,
      );
      cy.get('[data-cy=form-error-message]').should('not.exist');
      cy.get('[data-cy=bank-form-submit-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');

      // when submit button is clicked
      cy.get('[data-cy=bank-form-submit-button]').click();
      // then bankSubmitSpy is called with bank
      cy.get('@bankSubmitSpy').should(
        'have.been.calledOnceWithExactly',
        prefilledBank,
      );
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', false);
    });
  });

  it('shows prefilled and valid form for synchronous banks', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      const prefilledBank = {
        ...bank,
        isActive: false,
        isAsync: false,
        updatedAt: now,
      };
      delete prefilledBank.creditConfiguration;
      cy.clock(now);
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: prefilledBank,
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
          hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
        },
      });

      // then all fields are prefilled right
      cy.get('[data-cy=bank-form-is-active-select]').should('not.exist');
      cy.get('[data-cy=bank-form-thumbnail-url-input]').should(
        'have.value',
        prefilledBank.thumbnailUrl,
      );
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-min-term-in-months-input]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-input]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-min-schufa-rating-select]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-max-schufa-rating-select]').should(
        'not.exist',
      );
      cy.get('[data-cy=bank-form-submit-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(33, 186, 69)');

      // when submit button is clicked
      cy.get('[data-cy=bank-form-submit-button]').click();
      // then bankSubmitSpy is called with bank
      cy.get('@bankSubmitSpy').should(
        'have.been.calledOnceWithExactly',
        prefilledBank,
      );
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', false);
    });
  });

  it('creates bank when all fields are filled and form is submitted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
          hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
        },
      });

      // when
      cy.get('[data-cy=bank-form-is-active-select]').select('Aktiviert');
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type(bank.thumbnailUrl);
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type(
        bank.creditConfiguration.minAmountInEuros,
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type(
        bank.creditConfiguration.maxAmountInEuros,
      );
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type(
        bank.creditConfiguration.minTermInMonths,
      );
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type(
        bank.creditConfiguration.maxTermInMonths,
      );
      cy.get('[data-cy=bank-form-min-schufa-rating-select]').select(
        bank.creditConfiguration.minSchufaRating,
      );
      cy.get('[data-cy=bank-form-max-schufa-rating-select]').select(
        bank.creditConfiguration.maxSchufaRating,
      );
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@bankSubmitSpy').should('have.been.calledOnce');
    });
  });

  it('updates bank when all fields are for asynchronous bank are changed and form is submitted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isActive: true,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
          hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
        },
      });

      // when
      cy.get('[data-cy=bank-form-is-active-select]').select('Deaktiviert');
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type(
        'https://example.com/thubmnail.jpg',
      );
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('0');
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type('1000');
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type('1');
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type('6');
      cy.get('[data-cy=bank-form-min-schufa-rating-select]').select('B');
      cy.get('[data-cy=bank-form-max-schufa-rating-select]').select('B');
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@bankSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...bank,
        isActive: false,
        isAsync: true,
        thumbnailUrl: 'https://example.com/thubmnail.jpg',
        creditConfiguration: {
          minAmountInEuros: 0,
          maxAmountInEuros: 1000,
          minTermInMonths: 1,
          maxTermInMonths: 6,
          minSchufaRating: 'B',
          maxSchufaRating: 'B',
        },
        updatedAt: now,
      });
    });
  });

  it('updates bank when all fields are for synchronous bank are changed and form is submitted', () => {
    cy.fixture('bank').then((bank) => {
      // given
      const now = new Date(2024, 4, 24, 14, 0, 0, 0);
      const prefilledBank = {
        ...bank,
        isActive: true,
        isAsync: false,
        updatedAt: now,
      };
      delete prefilledBank.creditConfiguration;
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: prefilledBank,
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
          hasUnsavedChanges: createOutputSpy<boolean>('hasUnsavedChangesSpy'),
        },
      });

      // when
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type(
        'https://example.com/thubmnail.jpg',
      );
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.get('@hasUnsavedChangesSpy').should('have.been.calledWith', true);
      cy.get('@bankSubmitSpy').should('have.been.calledOnceWithExactly', {
        ...prefilledBank,
        isActive: true,
        isAsync: false,
        thumbnailUrl: 'https://example.com/thubmnail.jpg',
        updatedAt: now,
      });
    });
  });

  it('shows errors when all required fields are empty and submit button is pressed', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        },
      });

      // when
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-submit-button]').click();

      // then
      cy.get('@bankSubmitSpy').should('not.have.been.called');
      cy.get('[data-cy=bank-form-thumbnail-url-field]').contains('Pflichtfeld');
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-min-term-in-months-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-submit-button]')
        .should('have.css', 'background-color')
        .and('eq', 'rgb(202, 16, 16)');
    });
  });

  it('shows errors for thumbnail url', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        },
      });

      // when field is cleared
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      // then error is shown
      cy.get('[data-cy=bank-form-thumbnail-url-field]').contains('Pflichtfeld');

      // when invalid url is entered
      cy.get('[data-cy=bank-form-thumbnail-url-input]').clear();
      cy.get('[data-cy=bank-form-thumbnail-url-input]').type('invalid-url');
      // then error is shown
      cy.get('[data-cy=bank-form-thumbnail-url-field]').contains(
        'keine gültige URL',
      );
    });
  });

  it('shows errors for min and max amount in euros', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        },
      });

      // when field is cleared
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      // then error is shown
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]').contains(
        'Pflichtfeld',
      );
      // when invalid number is entered
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('1.2');
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type('1.2');
      // then error is shown
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]')
        .contains('Format')
        .contains('ungültig');
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]')
        .contains('Format')
        .contains('ungültig');
      // when too small number is entered
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('-1');
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type('-1');
      // then error is shown
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]').contains(
        'mindestens',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]').contains(
        'mindestens',
      );
      // when too high number is entered
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type(
        `${Number.MAX_SAFE_INTEGER + 1}`,
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type(
        `${Number.MAX_SAFE_INTEGER + 1}`,
      );
      // then error is shown
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]').contains(
        'maximal',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]').contains(
        'maximal',
      );
      // when min amount is higher than max amount
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').clear();
      cy.get('[data-cy=bank-form-min-amount-in-euros-input]').type('100');
      cy.get('[data-cy=bank-form-max-amount-in-euros-input]').type('99');
      // then error is shown
      cy.get('[data-cy=bank-form-min-amount-in-euros-field]').contains(
        'Kreditminimum darf nicht größer als Kreditmaximum',
      );
      cy.get('[data-cy=bank-form-max-amount-in-euros-field]').contains(
        'Kreditminimum darf nicht größer als Kreditmaximum',
      );
    });
  });

  it('shows errors for min and max term in months', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        },
      });

      // when field is cleared
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      // then error is shown
      cy.get('[data-cy=bank-form-min-term-in-months-field]').contains(
        'Pflichtfeld',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-field]').contains(
        'Pflichtfeld',
      );
      // when invalid number is entered
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type('1.2');
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type('1.2');
      // then error is shown
      cy.get('[data-cy=bank-form-min-term-in-months-field]')
        .contains('Format')
        .contains('ungültig');
      cy.get('[data-cy=bank-form-max-term-in-months-field]')
        .contains('Format')
        .contains('ungültig');
      // when too small number is entered
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type('-1');
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type('-1');
      // then error is shown
      cy.get('[data-cy=bank-form-min-term-in-months-field]').contains(
        'mindestens',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-field]').contains(
        'mindestens',
      );
      // when too high number is entered
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type(
        `${Number.MAX_SAFE_INTEGER + 1}`,
      );
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type(
        `${Number.MAX_SAFE_INTEGER + 1}`,
      );
      // then error is shown
      cy.get('[data-cy=bank-form-min-term-in-months-field]').contains(
        'maximal',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-field]').contains(
        'maximal',
      );
      // when min term is higher than max term
      cy.get('[data-cy=bank-form-min-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-max-term-in-months-input]').clear();
      cy.get('[data-cy=bank-form-min-term-in-months-input]').type('6');
      cy.get('[data-cy=bank-form-max-term-in-months-input]').type('5');
      // then error is shown
      cy.get('[data-cy=bank-form-min-term-in-months-field]').contains(
        'Mindestlaufzeit darf nicht größer als Maximallaufzeit',
      );
      cy.get('[data-cy=bank-form-max-term-in-months-field]').contains(
        'Mindestlaufzeit darf nicht größer als Maximallaufzeit',
      );
    });
  });

  it('shows errors for min and max schufa rating', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankFormComponent, {
        componentProperties: {
          isSubmitting: false,
          bank: {
            ...bank,
            isAsync: true,
            creditConfiguration: {
              ...bank.creditConfiguration,
            },
          },
          bankSubmit: createOutputSpy<Bank>('bankSubmitSpy'),
        },
      });

      // when
      cy.get('[data-cy=bank-form-min-schufa-rating-select]').select('B');
      cy.get('[data-cy=bank-form-max-schufa-rating-select]').select('A');

      // then
      cy.get('[data-cy=bank-form-min-schufa-rating-field]').contains(
        'Min. Schufa Rating darf nicht größer als Max. Schufa Rating',
      );
      cy.get('[data-cy=bank-form-max-schufa-rating-field]').contains(
        'Min. Schufa Rating darf nicht größer als Max. Schufa Rating',
      );
    });
  });
});
