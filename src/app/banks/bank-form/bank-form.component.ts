import { Component, effect, inject, input, output } from '@angular/core';
import { Bank } from '../../shared/models/bank';
import {
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
  ValueChangeEvent,
} from '@angular/forms';
import {
  AmountInEurosForm,
  BankForm,
  SchufaRatingForm,
  TermInMonthsForm,
} from '../../shared/models/bank-form';
import { RatingLevel } from '../../shared/models/rating-level';
import { urlValidator } from '../../shared/validators/url.validator';
import { filter, map } from 'rxjs';
import { FormErrorMessageComponent } from '../../shared/components/form-error-message/form-error-message.component';
import { KeyValuePipe } from '@angular/common';
import { v4 as uuidv4 } from 'uuid';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { CreditConfiguration } from '../../shared/models/credit-configuration';
import { integer } from '../../shared/validators/integer.validator';
import { amountInEurosValidator } from '../validators/amount-in-euros.validator';
import { termInMonthsValidator } from '../validators/term-in-months.validator';
import { schufaRatingValidator } from '../validators/schufa-rating.validator';

@Component({
  selector: 'bam-bank-form',
  imports: [ReactiveFormsModule, FormErrorMessageComponent, KeyValuePipe],
  templateUrl: './bank-form.component.html',
  styleUrl: './bank-form.component.css',
})
export class BankFormComponent {
  private readonly formBuilder = inject(NonNullableFormBuilder);

  readonly isSubmitting = input.required<boolean>();
  readonly bank = input<Bank>();
  readonly bankSubmit = output<Bank>();
  readonly hasUnsavedChanges = output<boolean>();

  readonly form = this.createEmptyBankFormValues();

  private readonly initialForm = this.createEmptyBankFormValues();
  private readonly initialBankId = uuidv4();

  constructor() {
    effect(() => {
      const bank = this.bank();

      if (bank) {
        this.setInitialBankFormValues(bank);
        this.setBankFormValues(bank);
      }
    });

    this.form.events
      .pipe(
        takeUntilDestroyed(),
        filter(
          (event): event is ValueChangeEvent<unknown> =>
            event instanceof ValueChangeEvent,
        ),
        map(() => [this.initialForm.getRawValue(), this.form.getRawValue()]),
        map(
          ([initialFormValue, currentFormValue]) =>
            JSON.stringify(initialFormValue) !==
            JSON.stringify(currentFormValue),
        ),
      )
      .subscribe((hasUnsavedChanges) =>
        this.hasUnsavedChanges.emit(hasUnsavedChanges),
      );
  }

  submitForm() {
    const bank = this.form?.getRawValue();

    if (this.form?.invalid) {
      return;
    }
    if (bank) {
      this.bankSubmit.emit({
        uuid: bank.uuid,
        name: bank.name,
        jmsQueue: bank.jmsQueue,
        thumbnailUrl: bank.thumbnailUrl,
        isAsync: bank.isAsync,
        isActive: bank.isActive,
        updatedAt: new Date(),
        ...(bank.hasCreditConfiguration && {
          creditConfiguration: {
            minAmountInEuros: bank.amountInEuros.minAmountInEuros,
            maxAmountInEuros: bank.amountInEuros.maxAmountInEuros,
            minTermInMonths: bank.termInMonths.minTermInMonths,
            maxTermInMonths: bank.termInMonths.maxTermInMonths,
            minSchufaRating: bank.schufaRating.minSchufaRating,
            maxSchufaRating: bank.schufaRating.maxSchufaRating,
          },
        }),
      });
    }
  }

  private setInitialBankFormValues(bank: Bank) {
    this.initialForm.patchValue(bank);
    this.initialForm.setControl(
      'hasCreditConfiguration',
      this.buildHasCreditConfigurationFormValue(bank.creditConfiguration),
    );

    if (bank.creditConfiguration) {
      const amountInEuros = this.buildAmountInEurosFormValues(
        bank.creditConfiguration,
      );
      const termInMonths = this.buildTermInMonthsFormValues(
        bank.creditConfiguration,
      );
      const schufaRating = this.buildSchufaRatingFormValues(
        bank.creditConfiguration,
      );

      this.initialForm.setControl('amountInEuros', amountInEuros);
      this.initialForm.setControl('termInMonths', termInMonths);
      this.initialForm.setControl('schufaRating', schufaRating);
    }
  }

  private setBankFormValues(bank: Bank) {
    this.form.patchValue(bank);
    this.form.setControl(
      'hasCreditConfiguration',
      this.buildHasCreditConfigurationFormValue(bank.creditConfiguration),
    );

    if (bank.creditConfiguration) {
      const amountInEuros = this.buildAmountInEurosFormValues(
        bank.creditConfiguration,
      );
      const termInMonths = this.buildTermInMonthsFormValues(
        bank.creditConfiguration,
      );
      const schufaRating = this.buildSchufaRatingFormValues(
        bank.creditConfiguration,
      );

      this.form.setControl('amountInEuros', amountInEuros);
      this.form.setControl('termInMonths', termInMonths);
      this.form.setControl('schufaRating', schufaRating);
    }
  }

  private createEmptyBankFormValues(): FormGroup<BankForm> {
    const creditConfiguration = this.getBaseCreditConfiguration();

    return this.formBuilder.group({
      uuid: this.initialBankId,
      name: '',
      jmsQueue: '',
      thumbnailUrl: ['', [Validators.required, urlValidator('https://.*')]],
      isAsync: this.formBuilder.control(true),
      isActive: this.formBuilder.control(true),
      updatedAt: new Date(),
      hasCreditConfiguration:
        this.buildHasCreditConfigurationFormValue(creditConfiguration),
      amountInEuros: this.buildAmountInEurosFormValues(creditConfiguration),
      termInMonths: this.buildTermInMonthsFormValues(creditConfiguration),
      schufaRating: this.buildSchufaRatingFormValues(creditConfiguration),
    });
  }

  private buildHasCreditConfigurationFormValue(
    creditConfiguration?: CreditConfiguration,
  ): FormControl<boolean> {
    return this.formBuilder.control(!!creditConfiguration);
  }

  private buildAmountInEurosFormValues(
    creditConfiguration: CreditConfiguration,
  ): FormGroup<AmountInEurosForm> {
    return this.formBuilder.group(
      {
        minAmountInEuros: [
          creditConfiguration.minAmountInEuros,
          [
            Validators.required,
            integer,
            Validators.min(0),
            Validators.max(Number.MAX_SAFE_INTEGER),
          ],
        ],
        maxAmountInEuros: [
          creditConfiguration.maxAmountInEuros,
          [
            Validators.required,
            integer,
            Validators.min(0),
            Validators.max(Number.MAX_SAFE_INTEGER),
          ],
        ],
      },
      { validators: [amountInEurosValidator] },
    );
  }

  private buildTermInMonthsFormValues(
    creditConfiguration: CreditConfiguration,
  ): FormGroup<TermInMonthsForm> {
    return this.formBuilder.group(
      {
        minTermInMonths: [
          creditConfiguration.minTermInMonths,
          [
            Validators.required,
            integer,
            Validators.min(0),
            Validators.max(Number.MAX_SAFE_INTEGER),
          ],
        ],
        maxTermInMonths: [
          creditConfiguration.maxTermInMonths,
          [
            Validators.required,
            integer,
            Validators.min(0),
            Validators.max(Number.MAX_SAFE_INTEGER),
          ],
        ],
      },
      { validators: [termInMonthsValidator] },
    );
  }

  private buildSchufaRatingFormValues(
    creditConfiguration: CreditConfiguration,
  ): FormGroup<SchufaRatingForm> {
    return this.formBuilder.group(
      {
        minSchufaRating: creditConfiguration.minSchufaRating,
        maxSchufaRating: creditConfiguration.maxSchufaRating,
      },
      {
        validators: [schufaRatingValidator],
      },
    );
  }

  private getBaseCreditConfiguration(): CreditConfiguration {
    return {
      minAmountInEuros: 0,
      maxAmountInEuros: 0,
      minTermInMonths: 0,
      maxTermInMonths: 0,
      minSchufaRating: RatingLevel.A,
      maxSchufaRating: RatingLevel.P,
    };
  }

  get amountInEuros() {
    return this.form.controls.amountInEuros;
  }

  get termInMonths() {
    return this.form.controls.termInMonths;
  }

  get schufaRating() {
    return this.form.controls.schufaRating;
  }

  protected readonly RatingLevel = RatingLevel;
}
