import { BankCreditConfigurationListItemComponent } from './bank-credit-configuration-list-item.component';

describe('BankCreditConfigurationListItemComponent', () => {
  it('shows default title and no jms queue when they are not provided', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankCreditConfigurationListItemComponent, {
        componentProperties: {
          creditConfiguration: bank.creditConfiguration,
        },
      });

      // then
      cy.get('[data-cy=default-credit-configuration-title]').should(
        'be.visible',
      );
      cy.get('[data-cy=custom-credit-configuration-title]').should('not.exist');
      cy.get('[data-cy=credit-configuration-jms-queue]').should('not.exist');
    });
  });

  it('shows pluralized months suffix when min and max term is greater one', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankCreditConfigurationListItemComponent, {
        componentProperties: {
          creditConfiguration: {
            ...bank.creditConfiguration,
            minTermInMonths: 2,
            maxTermInMonths: 2,
          },
        },
      });

      // then
      cy.get('[data-cy=credit-configuration-min-term-text]').contains('Monate');
      cy.get('[data-cy=credit-configuration-max-term-text]').contains('Monate');
    });
  });

  it('shows singular months suffix when min and max term is one', () => {
    cy.fixture('bank').then((bank) => {
      // given
      cy.mount(BankCreditConfigurationListItemComponent, {
        componentProperties: {
          creditConfiguration: {
            ...bank.creditConfiguration,
            minTermInMonths: 1,
            maxTermInMonths: 1,
          },
        },
      });

      // then
      cy.get('[data-cy=credit-configuration-min-term-text]').contains('Monat');
      cy.get('[data-cy=credit-configuration-max-term-text]').contains('Monat');
      cy.get('[data-cy=credit-configuration-min-term-text]').should(
        'not.contain',
        'Monate',
      );
      cy.get('[data-cy=credit-configuration-max-term-text]').should(
        'not.contain',
        'Monate',
      );
    });
  });
});
