import { Component, input } from '@angular/core';
import { CreditConfiguration } from '../../shared/models/credit-configuration';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'bam-bank-credit-configuration-list-item',
  imports: [CurrencyPipe],
  templateUrl: './bank-credit-configuration-list-item.component.html',
  styleUrl: './bank-credit-configuration-list-item.component.css',
})
export class BankCreditConfigurationListItemComponent {
  readonly creditConfiguration = input.required<CreditConfiguration>();
  readonly title = input('');
  readonly jmsQueue = input('');
}
