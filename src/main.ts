/// <reference types="@angular/localize" />

import { AppComponent } from './app/app.component';
import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/core/core.config';

bootstrapApplication(AppComponent, appConfig).catch((err) =>
  console.error(err),
);
