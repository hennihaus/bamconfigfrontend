export interface AppEnvironment {
  baseApiUrl: string;
  baseApiVersion: string;
  baseTitle: string;
  baseTitlePrefix: string;
  activeMqUrl: string;
  googleCloudIamUrl: string;
  passwordLength: number;
  passwordSequence: string;
  asyncTaskThumbnailUrl: string;
}
