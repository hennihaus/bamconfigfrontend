import { AppEnvironment } from './app-environment';

export const environment: AppEnvironment = {
  baseApiUrl: 'http://localhost:8080',
  baseApiVersion: 'v1',
  baseTitle: 'Business Integration',
  baseTitlePrefix: 'HFU',
  activeMqUrl: 'http://localhost:8161/admin',
  googleCloudIamUrl: 'https://google.de',
  passwordLength: 10,
  passwordSequence: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz',
  asyncTaskThumbnailUrl:
    'https://activemq.apache.org/assets/img/activemq_logo_white_vertical.png',
};
