// @ts-check
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import angular from 'angular-eslint';
import eslintPluginPrettierRecommended from 'eslint-plugin-prettier/recommended';
import cypress from 'eslint-plugin-cypress/flat';

export default tseslint.config(
  {
    name: 'bam/cypress',
    files: ['cypress/**/*.ts', 'src/**/*.cy.ts'],
    ignores: ['cypress/devtools/**/*'],
    extends: [
      eslint.configs.recommended,
      ...tseslint.configs.strict,
      ...tseslint.configs.stylistic,
      cypress.configs.recommended,
      eslintPluginPrettierRecommended
    ],
    rules: {
      '@typescript-eslint/no-unused-vars': ['error', { destructuredArrayIgnorePattern: '^_' }],
      'cypress/assertion-before-screenshot': 'error',
      'cypress/no-async-before': 'error',
      'cypress/no-debug': 'error',
      'cypress/no-force': 'error',
      'cypress/no-pause': 'error',
      'cypress/require-data-selectors': 'error',
    }
  },
  {
    name: 'bam/components',
    files: ['src/**/*.ts'],
    ignores: ['**/*.cy.ts'],
    extends: [
      eslint.configs.recommended,
      ...tseslint.configs.strict,
      ...tseslint.configs.stylistic,
      ...angular.configs.tsAll,
      eslintPluginPrettierRecommended,
    ],
    processor: angular.processInlineTemplates,
    linterOptions: {
      reportUnusedDisableDirectives: 'error',
    },
    rules: {
      '@typescript-eslint/no-extraneous-class': ['error', { allowEmpty: true }],
      '@angular-eslint/directive-selector': [
        'error',
        {
          type: 'attribute',
          prefix: 'bam',
          style: 'camelCase',
        },
      ],
      '@angular-eslint/component-selector': [
        'error',
        {
          type: 'element',
          prefix: 'bam',
          style: 'kebab-case',
        },
      ],
      '@angular-eslint/prefer-on-push-component-change-detection': 'off',
      '@angular-eslint/no-pipe-impure': 'off',
      '@angular-eslint/runtime-localize': 'off',
      '@angular-eslint/use-injectable-provided-in': 'off'
    },
  },
  {
    name: 'bam/templates',
    files: ['src/**/*.html'],
    extends: [
      ...angular.configs.templateAll,
      eslintPluginPrettierRecommended
    ],
    linterOptions: {
      reportUnusedDisableDirectives: 'error',
    },
    rules: {
      '@angular-eslint/template/no-call-expression': 'off',
      '@angular-eslint/template/i18n': [
        'error',
        {
          checkAttributes: false,
          checkId: false,
        }
      ],
      '@angular-eslint/template/no-duplicate-attributes': [
        'error',
        {
          allowStylePrecedenceDuplicates: true
        }
      ],
      'prettier/prettier': [
        'error',
        {
          'parser': 'angular'
        },
      ]
    },
  }
);
