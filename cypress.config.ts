import { defineConfig } from 'cypress';
import * as path from 'node:path';
import CDP from 'chrome-remote-interface';

const REMOTE_DEBUGGING_MIN_PORT = 40_000;
const REMOTE_DEBUGGING_SEED_PORT = 25_000;

let port = 0;
let client: CDP.Client | null;

export default defineConfig({
  e2e: {
    baseUrl: 'http://localhost:4200',
    setupNodeEvents: (on) => {
      on('before:browser:launch', (browser, launchOptions) => {
        // activate angular devtools
        if (!browser.isHeadless) {
          const angularDevToolsPath = path.resolve(
            'cypress',
            'devtools',
            '1.0.15_0',
          );
          launchOptions.args.push(`--load-extension=${angularDevToolsPath}`);
        }

        // activate remote debugging
        const existingRemoteDebuggingPort = launchOptions.args.find(
          (arg) => arg.slice(0, 23) === '--remote-debugging-port',
        );
        if (existingRemoteDebuggingPort) {
          port = +existingRemoteDebuggingPort.split('=')[1];
        } else {
          port =
            REMOTE_DEBUGGING_MIN_PORT +
            Math.round(Math.random() * REMOTE_DEBUGGING_SEED_PORT);
          launchOptions.args.push(`--remote-debugging-port=${port}`);
        }

        // return new launch options
        return launchOptions;
      });
      on('task', {
        activatePrintMediaQuery: async () => {
          client = client ?? (await CDP({ port }));
          return client.send('Emulation.setEmulatedMedia', { media: 'print' });
        },
        resetChromeRemoteInterface: async () => {
          if (client) {
            await client.close();
            client = null;
          }
          return true;
        },
      });
    },
  },
  component: {
    devServer: {
      framework: 'angular',
      bundler: 'webpack',
    },
    specPattern: 'src/**/*.cy.ts',
    setupNodeEvents: (on, config) => {
      on('before:browser:launch', (browser, launchOptions) => {
        // activate angular devtools
        if (!browser.isHeadless) {
          const angularDevToolsPath = path.resolve(
            'cypress',
            'devtools',
            '1.0.15_0',
          );
          launchOptions.args.push(`--load-extension=${angularDevToolsPath}`);
        }

        // return new launch options
        return launchOptions;
      });
    },
  },
});
